# -*- coding: utf-8 -*-
import autocomplete_light.shortcuts as al

from statistics.models import AbsentHistoryChange


class ChangeTotalAbsents_Form(al.ModelForm):
    class Meta:
        model = AbsentHistoryChange
        fields = ['absent_count', 'comment', 'created_by']
