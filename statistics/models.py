# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.utils.translation import ugettext_lazy as _

from people.models import Student, User
# Create your models here.


class AbsentHistoryChange(models.Model):
    """Изменение нб"""
    truant = models.ForeignKey(Student, max_length=10, related_name='truants')
    absent_count = models.CharField(_('Count absent'), max_length=10)
    comment = models.TextField(_('Comment'), max_length=500)
    created_by = models.ForeignKey(User, max_length=100)
    entrance_date = models.DateTimeField(_('entrance date'), auto_now_add=True)
