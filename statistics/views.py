# -*- coding: utf-8 -*-
from django.shortcuts import render
from core.models import Setting
from people.models import StudentStatistics
from django.contrib import messages
from django.utils.translation import ugettext_lazy as _
from statistics.models import AbsentHistoryChange
from statistics.forms import ChangeTotalAbsents_Form


def truants_list(request):
    students = StudentStatistics.objects.filter(
        total_absents__gte=float(Setting.objects.get(
            variable='CRITICAL_ABSENT_COUNT').value)).order_by("-total_absents")

    return render(request, 'statistics/truants_list.html', {"truants": students})


def student_rating(request):
    top = None
    bottom = None

    if request.user.is_student():
        pass
    elif request.user.is_teacher():
        if request.user.has_perm(""):
            pass
        elif request.user.has_perm(""):
            pass

    return render(request,
                  'statistics/student_rating.html', {"top": top,
                                                     "bottom": bottom})


def change_absents(request, id=id):
    position = request.user.teacher.position.code
    if position == 'mrobktar' or position == 'spes_educational_work':
        if request.method == 'POST':
            student = StudentStatistics.objects.get(id=id)
            stud_absent = float(request.POST.get('absent', ''))
            if stud_absent < student.total_absents:
                history = AbsentHistoryChange()
                history.comment = request.POST.get('comment', '')
                history.absent_count = request.POST.get('absent', '')
                history.author = request.user
                history.truant = student.student
                history.save()
                student.total_absents -= stud_absent
                student.save()
                full_message = _(u'Changes made by the user') + u' ' + request.user.username
                messages.success(request, full_message)
            else:
                messages.error(request, "Error")
        else:
            form = ChangeTotalAbsents_Form()

            return render(request, 'statistics/absent_form.html', {"form": form})
    else:
        messages.error(request, _("you do not have access to this operation"))
        return render(request, 'root/index.html')


def history_list(request, id=id):
    history = AbsentHistoryChange.objects.filter(truants_id=id)
    student = StudentStatistics.objects.get(id=id)

    return render(request,
                  'statistics/dict_list.html',
                  {"history": history, "student": student.student.get_full_name()})
