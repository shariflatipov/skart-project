# -*- coding: utf-8 -*-
from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^truants/list$', views.truants_list, name='api_truants_list'),
    url(r'^notifications/list$', views.truants_list, name='api_notifications_list'),
]