# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from django.db.models import F
from django.utils.html import format_html
from educational_plan.models import MarkBook
from people.models import Student, Teacher, StudentStatistics
from core.enums import ACTIVE
from core.models import Setting, DefaultSettings


class SettingsToRequest(object):
    def process_request(self, request):
        request.settings = DefaultSettings.objects.filter()[0]


class JournalAppender(object):
    def process_request(self, request):
        try:
            result = {}

            mark_books = MarkBook.objects.filter(
                learning_group__current_semester=F('semester_year__semester'),
                state=ACTIVE)

            if hasattr(request.user, 'teacher'):
                mark_books = mark_books.filter(teacher=request.user)

                for mark_book in mark_books:
                    if mark_book.learning_group.get_full_name() in result:
                        result[mark_book.learning_group.get_full_name()].append(
                            [
                                mark_book.pk,
                                mark_book.subject.name
                            ])
                    else:
                        result[mark_book.learning_group.get_full_name()] = [[mark_book.pk, mark_book.subject.name]]
                request.user.classes = result
            elif hasattr(request.user, 'student'):
                student = Student.objects.get(user_ptr=request.user)
                mark_books = mark_books.filter(learning_group=student.learning_group)
                for mark_book in mark_books:
                    result[mark_book.pk] = mark_book.subject.name
                request.user.lessons = result
        except Exception as e:
            print(e)


class AbsentCounter(object):
    def process_request(self, request):
        if hasattr(request.user, 'teacher'):
            teacher = Teacher.objects.get(user_ptr=request.user.pk)

            count = 0
            kwargs = {}

            try:
                if teacher.position.code == 'dean':
                    kwargs['student__learning_group__speciality__faculty'] = teacher.faculty
                elif teacher.position.code == 'curator':
                    kwargs['student__in'] = Student.objects.filter(
                        learning_group__in=MarkBook.objects.filter(teacher=teacher))

                if teacher.position.code == 'dean' or teacher.position.code == 'curator':
                    kwargs = {'total_absents__gte': Setting.objects.get(
                        variable="CRITICAL_ABSENT_COUNT").value}
                    statistics = StudentStatistics.objects.filter(**kwargs)
                    count = statistics.count()
            except Exception as e:
                print(e.message)

            header = format_html('''<a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-bell-o"></i>
                                        <span class="label label-warning">{}</span></a>''', count)

            absents = format_html('''<ul class="dropdown-menu">
                                        <li class="header">{}</li>
                                        <li>
                                            <ul class="menu">
                                              <li>
                                                <a href="{}">
                                                  <i class="fa fa-users text-aqua"></i> {} {}
                                                </a>
                                              </li>
                                            </ul>
                                        </li>
                                        <li class="footer">
                                            <a href="{}">{}</a>
                                        </li>
                                    </ul>''',
                                  _('You have new notifications'),
                                  reverse('api_truants_list'),
                                  str(count),
                                  _('students exhausted absent limits'),
                                  reverse('api_notifications_list'),
                                  _('All notifications'))

            request.user.notifications = header + absents


class TeachersThatNotPassedMarks(object):
    def process_request(self, request):
        if hasattr(request.user, 'teacher'):
            teacher = Teacher.objects.get(user_ptr=request.user.pk)

            count = 0
            kwargs = {}

            try:
                if teacher.position.code == 'dean':
                    kwargs['student__learning_group__speciality__faculty'] = teacher.faculty
                elif teacher.position.code == 'curator':
                    kwargs['student__in'] = Student.objects.filter(
                        learning_group__in=MarkBook.objects.filter(teacher=teacher))

                if teacher.position.code == 'dean' or teacher.position.code == 'curator':
                    kwargs = {'total_absents__gte': Setting.objects.get(
                        variable="CRITICAL_ABSENT_COUNT").value}
                    statistics = StudentStatistics.objects.filter(**kwargs)
                    count = statistics.count()
            except Exception as e:
                print(e.message)

            header = format_html('''<a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="fa fa-bell-o"></i>
                                        <span class="label label-warning">{}</span></a>''', count)

            absents = format_html('''<ul class="dropdown-menu">
                                        <li class="header">{}</li>
                                        <li>
                                            <ul class="menu">
                                              <li>
                                                <a href="{}">
                                                  <i class="fa fa-users text-aqua"></i> {} {}
                                                </a>
                                              </li>
                                            </ul>
                                        </li>
                                        <li class="footer">
                                            <a href="{}">{}</a>
                                        </li>
                                    </ul>''',
                                  _('You have new notifications'),
                                  reverse('api_truants_list'),
                                  str(count),
                                  _('students exhausted absent limits'),
                                  reverse('api_notifications_list'),
                                  _('All notifications'))

            request.user.notifications = header + absents
