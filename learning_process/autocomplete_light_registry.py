# -*- coding: utf-8 -*-
import autocomplete_light.shortcuts as al

from django.utils.translation import ugettext_lazy as _
from learning_process.models import Specialization, LearningGroup, Speciality,\
    Faculty

al.register(Specialization,
            search_fields=['name', 'code'],
            attrs={
                'placeholder': _('Speciality'),
                'data-autocomplete-minimum-characters': 1,
            },
            widget_attrs={
                'data-widget-maximum-values': 4,
                'class': 'modern-style',
            },
            )


al.register(Speciality,
            search_fields=['name', 'code'],
            attrs={
                'placeholder': _('Speciality'),
                'data-autocomplete-minimum-characters': 1
            },
            widget_attrs={
                'data-widget-maximum-values': 4,
                'class': 'modern-style',
            },
            )

al.register(Faculty,
            search_fields=['name'],
            attrs={
                'placeholder': _('Faculty'),
                'data-autocomplete-minimum-characters': 1
            },
            widget_attrs={
                'data-widget-maximum-values': 4,
                'class': 'modern-style',
            },
            )

class LearningGroupAutocomplete(al.AutocompleteModelBase):
    def choices_for_request(self):
        assert self.choices is not None, 'choices should be a query set'

        q = self.request.GET.get('q', '')

        exclude = self.request.GET.getlist('exclude')

        t = q.split('-')

        kwargs = {}

        if len(t) == 2:
            kwargs['course__icontains'] = t[0].strip()
            kwargs['speciality__code__icontains'] = t[1].strip()
        elif len(t) == 1:
            kwargs['speciality__code__icontains'] = t[0].strip()

        return self.order_choices(self.choices.filter(
            **kwargs).exclude(pk__in=exclude))[0:self.limit_choices]


al.register(LearningGroup, LearningGroupAutocomplete, widget_attrs={
                'data-widget-maximum-values': 4,
                'class': 'modern-style',
            })
