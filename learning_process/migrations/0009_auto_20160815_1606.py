# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-08-15 11:06
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('learning_process', '0008_auto_20160203_1712'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='learninggroup',
            unique_together=set([]),
        ),
    ]
