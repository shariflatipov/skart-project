# -*- coding: utf-8 -*-
from django.db import transaction
from django.http import HttpResponse

from openpyxl import Workbook
from openpyxl.writer.excel import save_virtual_workbook
from openpyxl.styles import Font

from people.models import Student
from .models import LearningGroup


def calculate_gpa(data, total_score):
    """{
    u'UW2': u'20',
    u'w15': {u'absent': u'0', u'mark': u'7'},
    u'w14': {u'absent': u'0', u'mark': u'7'},
    u'w13': {u'absent': u'0', u'mark': u'7'},
    u'w12': {u'absent': u'0', u'mark': u'6'},
    u'w11': {u'absent': u'0', u'mark': u'7'},
    u'w10': {u'absent': u'0', u'mark': u'7'},
    u'w7': {u'absent': u'0', u'mark': u'7'},
    u'w6': {u'absent': u'0', u'mark': u'7'},
    u'w5': {u'absent': u'0', u'mark': u'7'},
    u'w4': {u'absent': u'0', u'mark': u'7.5'},
    u'w3': {u'absent': u'0', u'mark': u'7.5'},
    u'w2': {u'absent': u'0', u'mark': u'7'},
    u'w1': {u'absent': u'0', u'mark': u'7.5'},
    u'w9': {u'absent': u'0', u'mark': u'6'},
    u'w8': {u'absent': u'0', u'mark': u'7'},
    u'w16': {u'absent': u'0', u'mark': u'7'},

    u'R1': 24.625,
    u'R2': 23.75,
    u'R3': 3.5,
    u'CP1': u'55',
    u'CP2': u'35'}
    :returns tuple of objects (4.0, 'A', rating_3, 5)
    """

    result = (0, 'F', 0, 2)
    if data:
        r1 = r2 = w13 = w14 = w15 = w16 = 0

        if 'R1' in data:
            r1 = data['R1']
        if 'R2' in data:
            r2 = data['R2']

        if 'w13' in data:
            w13 = data['w13']['mark']
        if 'w14' in data:
            w14 = data['w14']['mark']
        if 'w15' in data:
            w15 = data['w15']['mark']
        if 'w16' in data:
            w16 = data['w16']['mark']

        r3 = (total_score + float(w13) + float(w14) + float(w15) + float(w16)) / 2

        gpa = r3 + r2 + r1

        if gpa >= 95:
            result = (4.0, 'A', gpa, 5)
        elif 90 <= gpa < 95:
            result = (3.67, 'A-', gpa, 5)
        elif 85 <= gpa < 90:
            result = (3.33, 'B+', gpa, 4)
        elif 80 <= gpa < 85:
            result = (3.0, 'B', gpa, 4)
        elif 75 <= gpa < 80:
            result = (2.67, 'B-', gpa, 4)
        elif 70 <= gpa < 75:
            result = (2.33, 'C+', gpa, 3)
        elif 65 <= gpa < 70:
            result = (2.0, 'C', gpa, 3)
        elif 60 <= gpa < 65:
            result = (1.67, 'C-', gpa, 3)
        elif 55 <= gpa < 60:
            result = (1.33, 'D+', gpa, 3)
        elif 50 <= gpa < 55:
            result = (1.0, 'D', gpa, 3)
        elif 45 <= gpa < 50:
            result = (0, 'Fx', gpa, 2)
        elif 0 <= gpa < 45:
            result = (0, 'F', gpa, 2)

    return result


def get_learning_group_transcript(learning_group):
    ft = Font(size=13, bold=True)
    workbook = Workbook()

    worksheet = workbook.active
    i = 1

    for mark_book in learning_group.markbook_set.all():

        worksheet.merge_cells('A{}:Y{}'.format(i, i))
        worksheet['A' + str(i)] = mark_book.subject.name
        worksheet['A' + str(i)].font = ft

        i += 1
        worksheet.merge_cells('A{}:Y{}'.format(i, i))
        worksheet['A' + str(i)] = mark_book.teacher.get_full_name()
        worksheet['A' + str(i)].font = ft

        i += 1
        worksheet.merge_cells('A{}:Y{}'.format(i, i))
        worksheet['A' + str(i)] = mark_book.semester_year.__unicode__()
        worksheet['A' + str(i)].font = ft

        i += 1

        worksheet['A' + str(i)].font = ft
        worksheet['A' + str(i)] = u'ФИО'

        worksheet['B' + str(i)].font = ft
        worksheet['B' + str(i)] = u'Н1'
        worksheet['C' + str(i)].font = ft
        worksheet['C' + str(i)] = u'Н2'
        worksheet['D' + str(i)].font = ft
        worksheet['D' + str(i)] = u'Н3'
        worksheet['E' + str(i)].font = ft
        worksheet['E' + str(i)] = u'Н4'
        worksheet['F' + str(i)].font = ft
        worksheet['F' + str(i)] = u'Н5'
        worksheet['G' + str(i)].font = ft
        worksheet['G' + str(i)] = u'Н6'

        worksheet['H' + str(i)].font = ft
        worksheet['H' + str(i)] = u'СР1'
        worksheet['I' + str(i)].font = ft
        worksheet['I' + str(i)] = u'КР1'
        worksheet['J' + str(i)].font = ft
        worksheet['J' + str(i)] = u'Р1'

        worksheet['K' + str(i)].font = ft
        worksheet['K' + str(i)] = u'Н7'
        worksheet['L' + str(i)].font = ft
        worksheet['L' + str(i)] = u'Н8'
        worksheet['M' + str(i)].font = ft
        worksheet['M' + str(i)] = u'Н9'
        worksheet['N' + str(i)].font = ft
        worksheet['N' + str(i)] = u'Н10'
        worksheet['O' + str(i)].font = ft
        worksheet['O' + str(i)] = u'Н11'
        worksheet['P' + str(i)].font = ft
        worksheet['P' + str(i)] = u'Н12'

        worksheet['Q' + str(i)].font = ft
        worksheet['Q' + str(i)] = u'СР2'
        worksheet['R' + str(i)].font = ft
        worksheet['R' + str(i)] = u'КР2'
        worksheet['S' + str(i)].font = ft
        worksheet['S' + str(i)] = u'Р2'

        worksheet['T' + str(i)].font = ft
        worksheet['T' + str(i)] = u'Н13'
        worksheet['U' + str(i)].font = ft
        worksheet['U' + str(i)] = u'Н14'
        worksheet['V' + str(i)].font = ft
        worksheet['V' + str(i)] = u'Н15'
        worksheet['W' + str(i)].font = ft
        worksheet['W' + str(i)] = u'Н16'

        worksheet['X' + str(i)].font = ft
        worksheet['X' + str(i)] = u'Р2'
        worksheet['Y' + str(i)].font = ft
        worksheet['Y' + str(i)] = u'ФЭ'
        worksheet['Z' + str(i)].font = ft
        worksheet['Z' + str(i)] = u'GPA'

        i += 1

        for k, v in mark_book.marks.items():
            student = Student.objects.get(pk=int(k))
            for t, m in v.items():
                worksheet['A' + str(i)] = student.get_full_name()
                if t == 'w1':
                    worksheet['B' + str(i)] = m['mark']
                if t == 'w2':
                    worksheet['C' + str(i)] = m['mark']
                if t == 'w3':
                    worksheet['D' + str(i)] = m['mark']
                if t == 'w4':
                    worksheet['E' + str(i)] = m['mark']
                if t == 'w5':
                    worksheet['F' + str(i)] = m['mark']
                if t == 'w6':
                    worksheet['G' + str(i)] = m['mark']
                if t == 'UW1':
                    worksheet['H' + str(i)] = m
                if t == 'CP1':
                    worksheet['I' + str(i)] = m
                if t == 'R1':
                    worksheet['J' + str(i)] = m
                if t == 'w7':
                    worksheet['K' + str(i)] = m['mark']
                if t == 'w8':
                    worksheet['L' + str(i)] = m['mark']
                if t == 'w9':
                    worksheet['M' + str(i)] = m['mark']
                if t == 'w10':
                    worksheet['N' + str(i)] = m['mark']
                if t == 'w11':
                    worksheet['O' + str(i)] = m['mark']
                if t == 'w12':
                    worksheet['P' + str(i)] = m['mark']
                if t == 'UW2':
                    worksheet['Q' + str(i)] = m
                if t == 'CP2':
                    worksheet['R' + str(i)] = m
                if t == 'R2':
                    worksheet['S' + str(i)] = m
                if t == 'w13':
                    worksheet['T' + str(i)] = m['mark']
                if t == 'w14':
                    worksheet['U' + str(i)] = m['mark']
                if t == 'w15':
                    worksheet['V' + str(i)] = m['mark']
                if t == 'w16':
                    worksheet['W' + str(i)] = m['mark']
                if t == 'R3':
                    worksheet['X' + str(i)] = m
                if t == 'FE':
                    worksheet['Y' + str(i)] = m
                if t == 'gpa':
                    worksheet['Z' + str(i)] = m

            i += 1

    response = HttpResponse(content=save_virtual_workbook(workbook), content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename=file.xlsx'
    return response


@transaction.atomic
def semester_up():
    for group in LearningGroup.objects.all():
        print("Before ...................... {}".format(group.current_semester))
        print("Before ...................... {}".format(group.course))
        group.semester_up()
        print("After  ...................... {}".format(group.current_semester))
        print("After  ...................... {}".format(group.course))
