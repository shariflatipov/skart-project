import json
from collections import defaultdict

from django.db import transaction
from django.db.models import F
from django.contrib import messages
from django.contrib.auth.decorators import permission_required, login_required
from django.core.urlresolvers import reverse
from django.http import Http404
from django.http.response import HttpResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.utils.translation import ugettext_lazy as _
from django.forms import modelformset_factory

import autocomplete_light.shortcuts as al

from core.models import DefaultSettings
from core.enums import ACTIVE
from learning_process.forms import ChooseWeekForm
from people.models import Student, StudentStatistics, StudentOperationDocument, UserOpsStates
from educational_plan.models import MarkBook
from .forms import (
    LearningGroupForm,
    LanguageForm,
    SpecialityForm,
    SpecializationForm,
    FacultyForm,
    SetMarkModelFormset,
    SetMarkAndCheckPointMarkModelFormset,
    SetMarkPenaltyAndStimulateModelFormset,
    StudentOrdersForm,
    ChooseStudentToOperateFormset,
    StudentOperationsForm
)

from .models import LearningGroup, Language, Specialization, Speciality,\
    Faculty

from utils import get_learning_group_transcript

al.autodiscover()


def learning_group_add(request):
    if request.method == 'POST':
        form = LearningGroupForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(reverse('learning_group_list'))
    else:
        form = LearningGroupForm()

    return render(request, 'general_form/add_form.html', {"form": form, "title": "Learning group",
                                                          "cancel_url": reverse('learning_group_list')})


def learning_group_list(request):
    return render(request, 'learning_process/group_list.html',
                  {'groups': LearningGroup.objects.all()})


def lang_add(request):
    if request.method == 'POST':
        form = LanguageForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(reverse('lang_list'))
    else:
        form = LanguageForm()

    return render(request, 'general_form/add_form.html',
                  {"form": form, "title": 'Languages', "cancel_url": reverse('lang_list')})


def language_list(request):
    return render(request, 'learning_process/language_list.html',
                  {'languages': Language.objects.all()})


def specialization_add(request):
    if request.method == 'POST':
        form = SpecializationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(reverse('specialization_list'))
    else:
        form = SpecializationForm()

    return render(request, 'general_form/add_form.html',
                  {"form": form, "title": 'Specialization',
                   "cancel_url": reverse('specialization_list')})


def specialization_list(request):
    return render(request, 'learning_process/specialization_list.html',
                  {'specializations': Specialization.objects.all()})


def speciality_add(request):
    if request.method == 'POST':
        form = SpecialityForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(reverse('speciality_list'))
    else:
        form = SpecialityForm()

    return render(request, 'general_form/add_form.html',
                  {"form": form, "title": 'Speciality',
                   "cancel_url": reverse('speciality_list')})


def speciality_list(request):
    return render(request, 'learning_process/speciality_list.html',
                  {'specialities': Speciality.objects.all()})


def faculty_add(request):
    if request.method == 'POST':
        form = FacultyForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(reverse('faculty_list'))
    else:
        form = FacultyForm()

    return render(request, 'general_form/add_form.html',
                  {"form": form, "title": 'Faculty',
                   "cancel_url": reverse('faculty_list')})


def faculty_list(request):
    return render(request, 'learning_process/faculty_list.html',
                  {'faculties': Faculty.objects.all()})


@login_required
@permission_required('educational_plan.add_markbook')
def mark_pass_journal(request, mark_book_id):
    # teacher = get_object_or_404(Teacher, user_ptr=request.user)
    # mark_book = get_object_or_404(MarkBook, pk=mark_book_id, teacher=teacher)
    mark_book = get_object_or_404(MarkBook, pk=mark_book_id)
    return render(request, 'learning_process/journal/add.html',
                  {'mark_book': mark_book})


@login_required
@permission_required('educational_plan.add_markbook')
def generate(request, mark_book_id):
    mark_book = get_object_or_404(MarkBook, pk=mark_book_id)
    header = {'data': {}}
    header['data']['weekCount'] = mark_book.week_count
    header['data']['activeWeek'] = mark_book.active_week
    header['data']['mark_book'] = mark_book.pk
    header['data']['learning_group'] = {}

    data = MarkBook.objects.get(pk=mark_book_id).marks
    header['data'].update(data)
    return HttpResponse(json.dumps(header))


@login_required
@permission_required('educational_plan.add_markbook')
@transaction.atomic
def set_mark(request, mark_book_id):
    mb = MarkBook.get_for_teacher(request.user, mark_book_id)
    if mb:
        current_week = DefaultSettings.objects.all()[0].current_week

        # This shouldn't happen unless the mark field of MarkBook
        # set through django admin interface
        if isinstance(mb.marks, dict) and mb.marks:
            # FIXME if student was expelled than may be situation
            # where this record will be recognized as not closed
            # week. I need to add some key that will indicate that
            # week was closed

            iterator = mb.marks.itervalues()

            closed_week = False
            if current_week.code in iterator.next():
                closed_week = True
            if current_week.code in iterator.next():
                closed_week = True
            if current_week.code in iterator.next():
                closed_week = True

            if closed_week:
                raise Http404(_("You closed this week for this group"))

        if current_week.code == 'w7' or current_week.code == 'w13':
            markbook_formset = modelformset_factory(Student,
                                                    formset=SetMarkAndCheckPointMarkModelFormset,
                                                    fields=(), extra=0)
            template = 'learning_process/journal/set_mark_and_cp.html'
        elif current_week.code == 'w16':
            markbook_formset = modelformset_factory(Student,
                                                    formset=SetMarkPenaltyAndStimulateModelFormset,
                                                    fields=(), extra=0)
            template = 'learning_process/journal/set_mark_and_penalty_stimulate.html'
        else:
            markbook_formset = modelformset_factory(Student,
                                                    formset=SetMarkModelFormset,
                                                    fields=(), extra=0)
            template = 'learning_process/journal/set_mark.html'
        if request.method == 'POST':
            form = markbook_formset(request.POST,
                                    queryset=mb.learning_group.student_set.filter(state=UserOpsStates.ACTIVE))
            if form.is_valid():
                data = mb.marks
                if not isinstance(mb.marks, dict):
                    data = {}
                for f in form:
                    cd = f.cleaned_data

                    try:
                        student = StudentStatistics.objects.get(
                            student__pk=cd['user_ptr'].pk,
                            student__learning_group__current_semester=mb.semester_year.semester)
                    except Exception:
                        student = StudentStatistics()
                        student.student_id = cd['user_ptr'].pk
                        student.semester = mb.semester_year.semester

                    user = str(cd['user_ptr'].pk)
                    if user not in data:
                        data[user] = {}
                    if current_week.code not in data:
                        data[user][current_week.code] = {}

                    data[user][current_week.code]['mark'] = cd['mark']
                    data[user][current_week.code]['absent'] = cd['absent']
                    if current_week.code == 'w7':
                        data[user]['CP1'] = cd['cp_mark']
                        data[user]['UW1'] = cd['uw_mark']
                        student.total_score += float(cd['cp_mark'])
                        mb.set_rating(user, MarkBook.R1)
                    elif current_week.code == 'w13':
                        data[user]['CP2'] = cd['cp_mark']
                        data[user]['UW2'] = cd['uw_mark']
                        student.total_score += float(cd['cp_mark'])
                        mb.set_rating(user, MarkBook.R2)
                    elif current_week.code == 'w16':
                        data[user]['PENALTY'] = cd['penalty']
                        data[user]['STIMULATE'] = cd['stimulate']

                    # Save some data for speed optimization Student statistics
                    student.total_absents += float(cd['absent'])
                    student.total_score += float(cd['mark'])
                    student.total_weeks += 1
                    student.save()

                mb.marks = data
                mb.save()
                messages.info(request, _("Marks was saved correctly"))
                return redirect(reverse('index'))
        else:
            form = markbook_formset(queryset=mb.learning_group.student_set.filter(state=UserOpsStates.ACTIVE))
        return render(request, template, {'formset': form, 'mb': mb, 'week': current_week})
    else:
        raise Http404(_("You cannot set mark to this group"))


@login_required
@permission_required('educational_plan.add_markbook')
def show_marks(request, mark_book_id):
    mb = MarkBook.get_for_teacher(request.user, mark_book_id)

    for key, val in mb.marks.items():
        student = Student.objects.get(pk=key)
        mb.marks[key]['student'] = student.get_short_name()

    return render(request, 'learning_process/journal/show_marks.html',
                  {'marks': mb.marks, 'subject': mb.subject.name})


@login_required
def show_journal(request, mark_book_id):
    if hasattr(request.user, 'student'):
        student = Student.objects.get(user_ptr=request.user)
        mark_book = MarkBook.objects.get(learning_group=student.learning_group,
                                         pk=mark_book_id)
        marks = mark_book.marks[str(student.pk)]
        subject = mark_book.subject
        teacher = mark_book.teacher

        abs_count = 0
        total_marks = 0
        week_count = 0

        cp_marks = 0

        for k, v in marks.items():
            if k == 'CP1' or k == 'CP2' or k == 'UW1'\
                    or k == 'UW2' or k == 'FE' or k == 'FW' or k == 'R1' or k == 'R2' or k == 'R3':
                cp_marks += float(v)
            else:
                abs_count += float(v['absent'])
                week_count += 1
                total_marks += float(v['mark'])

        marks['total'] = {}
        marks['total']['mark'] = total_marks + cp_marks
        marks['total']['absent'] = abs_count

        marks['avg'] = {}
        marks['avg']['mark'] = "%.1f" % (total_marks / week_count)
        marks['avg']['absent'] = "%.1f" % (abs_count / week_count)

        return render(request, 'learning_process/journal/student.html',
                      {'marks': marks, 'subject': subject,
                       'teacher': teacher})
    else:
        raise Http404()


@login_required
def show_journal_full(request):
    result_dict = {}

    if hasattr(request.user, 'student'):
        student = Student.objects.get(user_ptr=request.user)
        mark_book = MarkBook.objects.filter(marks__has_key=str(student.pk),
                                            learning_group=student.learning_group,
                                            learning_group__current_semester=F('semester_year__semester'),
                                            state=ACTIVE)

        for mb in mark_book:
            result_dict[mb.subject.name] = mb.marks[str(student.pk)]

    return render(request, 'learning_process/journal/general_student_journal.html',
                  {'marks': result_dict})


@login_required
def mark_book_creator(request):
    if request.user.is_superuser:
        mark_books = MarkBook.objects.filter(learning_group__current_semester=F('semester_year__semester'),
                                             state=ACTIVE).order_by('learning_group')
    else:
        mark_books = MarkBook.objects.filter(created_by__pk=request.user.pk,
                                             learning_group__current_semester=F('semester_year__semester'),
                                             state=ACTIVE).order_by('learning_group')
    return render(request, 'learning_process/show_mark_books.html', {"mark_book": mark_books})


@login_required
@permission_required('learning_group.get_transcript')
def download_transcript(request, pk):
    learning_group = LearningGroup.objects.get(pk=pk)
    return get_learning_group_transcript(learning_group)


@login_required
@permission_required('student.operate_student')
def choose_group_to_operate(request):
    form = StudentOrdersForm()
    if request.method == 'POST':
        form = StudentOrdersForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            print(cd['operations'], cd['learning_group'])
            return redirect(reverse(operate_student, args=[int(cd['operations']), cd['learning_group'].pk]))
    return render(request, 'general_submit_form.html', {'form': form})


@login_required
@transaction.atomic
@permission_required('student.operate_student')
def operate_student(request, operation, learning_group):

    lg = LearningGroup.objects.get(pk=learning_group)
    StudentFormset = modelformset_factory(Student, formset=ChooseStudentToOperateFormset,
                                          fields=(), extra=0)
    if request.method == 'POST':
        operations_form = StudentOperationsForm(request.POST, request.FILES)
        formset = StudentFormset(request.POST, queryset=lg.student_set.all())

        if operations_form.is_valid() and formset.is_valid():
            print('form is valid')
            op_cd = operations_form.cleaned_data
            s_op = StudentOperationDocument()
            s_op.created_by = request.user
            s_op.document_sign_date = op_cd['operation_date']
            s_op.operation = op_cd['operation_type']
            s_op.order_file = op_cd['attachment']
            s_op.operation_document_number = op_cd['document_number']
            s_op.save()

            students_operation = int(op_cd['operation_type'])
            for form in formset:
                cd = form.cleaned_data
                if cd['choose']:
                    s_op.students.add(form.instance)

                    if students_operation == UserOpsStates.MOVE:
                        form.instance.learning_group = op_cd['learning_group_to']
                    elif students_operation == UserOpsStates.ACADEMIC_LEAVE:
                        form.instance.state = UserOpsStates.DISABLED
                    elif students_operation == UserOpsStates.REINSTATE:
                        form.instance.state = UserOpsStates.ACTIVE
                    elif students_operation == UserOpsStates.EXPEL:
                        form.instance.learning_group = None
                    form.save()
            return redirect(reverse('operate_student_choose_group'))
        else:
            print('form is invalid')
    else:
        operations_form = StudentOperationsForm()
        formset = StudentFormset(queryset=lg.student_set.all())

    return render(request, 'learning_process/student_operations.html', {'formset': formset,
                                                                        'operations_form': operations_form,
                                                                        'learning_group': lg})


def teachers_not_set_marks(week):
    """
    :param Week object that is in core module week:
    :return a dictionary with all teachers that doesn't set marks with mark book id:
    """
    mark_books = MarkBook.objects.filter(state=ACTIVE)

    d = defaultdict(dict)
    for mark_book in mark_books:
        match = False
        for k, v in mark_book.marks.items():
            if week.code in v:
                match = True
                break
        if not match:
            if mark_book.teacher in d[week.code]:
                d[week.code][mark_book.teacher].append(mark_book)
            else:
                d[week.code][mark_book.teacher] = list()
                d[week.code][mark_book.teacher].append(mark_book)
    return d


@login_required
@permission_required('student.operate_student')
def not_set_marks(request):

    if request.method == 'POST':
        form = ChooseWeekForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            week = cd['week']
            d = teachers_not_set_marks(week=week)
            return render(request, 'learning_process/teachers_not_set_marks.html', {'data': dict(d)})
    else:
        form = ChooseWeekForm()
        return render(request, 'general_submit_form.html', {'form': form})
