# -*- coding: utf-8 -*-
from django.conf.urls import url
from . import views
from educational_plan.views import select_educational_plan_for_group

urlpatterns = [
    url(r'^learning_group_add$', views.learning_group_add, name='learning_group_add'),
    url(r'^learning_group_list$', views.learning_group_list, name='learning_group_list'),
    url(r'^download/transcript/(?P<pk>\d+)/$', views.download_transcript, name='download_transcript'),
    url(r'^language_list$', views.language_list, name='lang_list'),
    url(r'^language_add$', views.lang_add, name='lang_add'),
    url(r'^speciality_list$', views.speciality_list, name='speciality_list'),
    url(r'^speciality_add$', views.speciality_add, name='speciality_add'),
    url(r'^specialization_list$', views.specialization_list, name='specialization_list'),
    url(r'^specialization_add$', views.specialization_add, name='specialization_add'),
    url(r'^faculty/list$', views.faculty_list, name='faculty_list'),
    url(r'^faculty/add$', views.faculty_add, name='faculty_add'),
    url(r'^select_group_semester/$', select_educational_plan_for_group, name='select_group_semester'),
    url(r'^mark_pass_journal/(?P<mark_book_id>\d+)/$', views.mark_pass_journal, name='mark_pass_journal'),
    url(r'^set_mark/(?P<mark_book_id>\d+)/$', views.set_mark, name='set_mark'),
    url(r'^get_marks/(?P<mark_book_id>\d+)/$', views.generate, name='generate'),
    url(r'^show/journal/(?P<mark_book_id>\d+)/$', views.show_journal, name='show_journal'),
    url(r'^show/marks/(?P<mark_book_id>\d+)/$', views.show_marks, name='show_mark'),
    url(r'^show/mark_books/$', views.mark_book_creator, name='mark_book_creator'),
    url(r'^show/full_journal/$', views.show_journal_full, name='edu_show_full_journal'),
    url(r'^student/operate/$', views.choose_group_to_operate, name='operate_student_choose_group'),
    url(r'^student/operate/(?P<operation>\d+)/(?P<learning_group>\d+)/$', views.operate_student, name='operate_student'),
    url(r'^teachers/notsetmarks/$', views.not_set_marks, name='teachers_not_set_marks'),
]
