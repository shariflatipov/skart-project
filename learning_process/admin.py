from django.contrib import admin

# Register your models here.
from learning_process.models import *

class LearningGroupAdmin(admin.ModelAdmin):
    list_filter = ('speciality', )

admin.site.register(Speciality)
admin.site.register(Specialization)
admin.site.register(Language)
admin.site.register(LearningGroup, LearningGroupAdmin)
admin.site.register(Faculty)
admin.site.register(Department)
admin.site.register(TestTypes)
