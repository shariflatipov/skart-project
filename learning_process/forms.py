# -*- coding: utf-8 -*-
import autocomplete_light.shortcuts as al

from django import forms
from django.forms import BaseModelFormSet

from core.models import Week
from people.models import Student
from .models import LearningGroup, Language, Speciality,\
    Specialization, Faculty
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from people.models import UserOpsStates


def MarkValidator(value):
    if float(value) < 0 or float(value) > 7.5:
        raise ValidationError(
            _('%(value)s is not in range 0 .. 7.5'),
            params={'value': value},
        )


def CheckPointValidator(value):
    if float(value) < 0 or float(value) > 35:
        raise ValidationError(
            _('%(value)s is not in range 0 .. 35'),
            params={'value': value},
        )


def PenaltyStimulateValidator(value):
    if float(value) < 0 or float(value) > 20:
        raise ValidationError(
            _('%(value)s is not in range 0 .. 35'),
            params={'value': value},
        )


def UnrevisedWorkValidator(value):
    if float(value) < 0 or float(value) > 20:
        raise ValidationError(
            _('%(value)s is not in range 0 .. 20'),
            params={'value': value},
        )


class LearningGroupForm(forms.ModelForm):
    class Meta:
        model = LearningGroup
        exclude = []


class LanguageForm(forms.ModelForm):
    class Meta:
        model = Language
        exclude = []


class SpecialityForm(forms.ModelForm):
    class Meta:
        model = Speciality
        exclude = []


class SpecializationForm(al.ModelForm):
    speciality = al.ModelChoiceField('SpecialityAutocomplete')

    class Meta:
        model = Specialization
        exclude = []


class FacultyForm(al.ModelForm):
    speciality = al.ModelChoiceField('SpecialityAutocomplete')

    class Meta:
        model = Faculty
        exclude = []


class SetMarkModelFormset(BaseModelFormSet):

    class Meta:
        model = Student
        fields = ('first_name', 'last_name', 'middle_name')

    def __init__(self, *args, **kwargs):
        super(SetMarkModelFormset, self).__init__(*args, **kwargs)

    def _construct_form(self, i, **kwargs):
        form = super(SetMarkModelFormset, self)._construct_form(i, **kwargs)
        form.fields['mark'] = forms.CharField(widget=forms.TextInput(
            attrs={'class': 'number', 'pattern': '(\d*[.])?\d+'}),
            validators=[MarkValidator])
        form.fields['absent'] = forms.CharField(widget=forms.TextInput(
            attrs={'class': 'number', 'pattern': '(\d*[.])?\d+'}),
            initial=0)
        return form


class SetMarkAndCheckPointMarkModelFormset(BaseModelFormSet):

    class Meta:
        model = Student
        fields = ('first_name', 'last_name', 'middle_name')

    def __init__(self, *args, **kwargs):
        super(SetMarkAndCheckPointMarkModelFormset, self).__init__(*args, **kwargs)

    def _construct_form(self, i, **kwargs):
        form = super(SetMarkAndCheckPointMarkModelFormset, self)._construct_form(i, **kwargs)
        form.fields['mark'] = forms.CharField(widget=forms.TextInput(
            attrs={'class': 'number', 'pattern': '(\d*[.])?\d+'}),
            validators=[MarkValidator]
        )
        form.fields['cp_mark'] = forms.CharField(widget=forms.TextInput(
            attrs={'class': 'number', 'pattern': '(\d*[.])?\d+'}),
            validators=[CheckPointValidator]
        )
        form.fields['uw_mark'] = forms.CharField(widget=forms.TextInput(
            attrs={'class': 'number', 'pattern': '(\d*[.])?\d+'}),
            validators=[UnrevisedWorkValidator]
        )
        form.fields['absent'] = forms.CharField(widget=forms.TextInput(
            attrs={'class': 'number', 'pattern': '(\d*[.])?\d+'}),
            initial=0)
        return form


class SetMarkPenaltyAndStimulateModelFormset(BaseModelFormSet):

    class Meta:
        model = Student
        fields = ('first_name', 'last_name', 'middle_name')

    def __init__(self, *args, **kwargs):
        super(SetMarkPenaltyAndStimulateModelFormset, self).__init__(*args, **kwargs)

    def _construct_form(self, i, **kwargs):
        form = super(SetMarkPenaltyAndStimulateModelFormset, self)._construct_form(i, **kwargs)
        form.fields['mark'] = forms.CharField(widget=forms.TextInput(
            attrs={'class': 'number', 'pattern': '(\d*[.])?\d+'}),
            validators=[MarkValidator]
        )
        form.fields['absent'] = forms.CharField(widget=forms.TextInput(
            attrs={'class': 'number', 'pattern': '(\d*[.])?\d+'}),
            initial=0)
        form.fields['penalty'] = forms.CharField(widget=forms.TextInput(
            attrs={'class': 'number', 'pattern': '(\d*[.])?\d+'}),
            validators=[PenaltyStimulateValidator]
        )
        form.fields['stimulate'] = forms.CharField(widget=forms.TextInput(
            attrs={'class': 'number', 'pattern': '(\d*[.])?\d+'}),
            validators=[PenaltyStimulateValidator]
        )
        return form


class StudentOrdersForm(forms.Form):
    operations = forms.ChoiceField(label=_('Operation'), choices=UserOpsStates.OPERATIONS)
    learning_group = al.ModelChoiceField("LearningGroupAutocomplete")


class ChooseStudentToOperateFormset(BaseModelFormSet):

    class Meta:
        model = Student
        fields = ('first_name', 'last_name', 'middle_name')

    def __init__(self, *args, **kwargs):
        super(ChooseStudentToOperateFormset, self).__init__(*args, **kwargs)

    def _construct_form(self, i, **kwargs):
        form = super(ChooseStudentToOperateFormset, self)._construct_form(i, **kwargs)
        form.fields['choose'] = forms.BooleanField(required=False)
        return form


class StudentOperationsForm(forms.Form):
    document_number = forms.IntegerField()
    operation_type = forms.ChoiceField(choices=UserOpsStates.OPERATIONS)
    learning_group_to = al.ModelChoiceField("LearningGroupAutocomplete", required=False)
    operation_date = forms.DateField(widget=forms.DateInput(attrs={'class': 'datepicker'}))
    attachment = forms.FileField()

    def clean(self):
        cleaned_data = super(StudentOperationsForm, self).clean()
        oper_type = cleaned_data.get("operation_type")
        lg_to = cleaned_data.get("learning_group")

        if oper_type:
            print(oper_type)
            print(type(oper_type))
            print(UserOpsStates.EXPEL)
            print(type(UserOpsStates.EXPEL))
            if 1 != 1:
                print('Should not get here twice')
            if int(oper_type) != int(UserOpsStates.EXPEL):
                print('Should not get here')
                if not lg_to:
                    raise forms.ValidationError(
                        "Field learning group to is required"
                    )


class ChooseWeekForm(forms.Form):
    week = forms.ModelChoiceField(queryset=Week.objects.all())
