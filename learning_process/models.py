# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext_lazy as _
from core.enums import STATE, EDU_ATTENDANT_TYPE_CHOICES, \
    EDU_MODEL_CHOICES, DISTANT, FINISHED
from core.models import Semester


class Faculty(models.Model):
    """Факультеты"""
    name = models.CharField(_('Name'), max_length=100)

    def __unicode__(self):
        return u'{}'.format(self.name)

    class Meta:
        verbose_name = _("Faculty")
        verbose_name_plural = _("Faculties")


class Speciality(models.Model):
    """Специальность"""

    # Шифр 24.01.02
    code = models.CharField(_('Code'), max_length=100, unique=True)
    name = models.CharField(_('Name'), max_length=100)
    faculty = models.ForeignKey(Faculty, verbose_name=_('Faculty'))

    def __unicode__(self):
        return '%s - %s' % (unicode(self.code), unicode(self.name))

    class Meta:
        verbose_name = _("Speciality")
        verbose_name_plural = _("Speciality")


class Specialization(models.Model):
    """Специализация"""
    # Шифр 103 (вместе с шифром из специальности составит 24.01.02-103)
    code = models.CharField(_('Code'), max_length=5)
    name = models.CharField(_('Name'), max_length=150)
    speciality = models.ForeignKey(Speciality, verbose_name=_("Speciality"), null=True, blank=True)

    def __unicode__(self):
        return '%s - %s' % (unicode(self.code), unicode(self.name))

    class Meta:
        unique_together = (('code', 'speciality'),)
        verbose_name = _("Specialization")
        verbose_name_plural = _("Specializations")


class Language(models.Model):
    """Язык обучения"""
    code = models.CharField(_("Code"), max_length=10, unique=True)
    name = models.CharField(_("Name"), max_length=20)
    short_name = models.CharField(_("Short name"), max_length=2)

    def __unicode__(self):
        return unicode(self.name)

    class Meta:
        verbose_name = _("Learning language")
        verbose_name_plural = _("Learning languages")


class LearningGroup(models.Model):
    """Группа обучения"""
    speciality = models.ForeignKey(Speciality, verbose_name=_("Speciality"))
    language = models.ForeignKey(Language, verbose_name=_("Language"))
    subgroup_letter = models.CharField(_('Subgroup letter'), max_length=1)
    study_type = models.PositiveSmallIntegerField(_('Study type'), choices=EDU_ATTENDANT_TYPE_CHOICES)
    study_model = models.PositiveSmallIntegerField(_('Study model'), choices=EDU_MODEL_CHOICES)
    entrance_year = models.DateField(_('Entrance year'))
    course = models.PositiveSmallIntegerField(_('Course'))
    current_semester = models.ForeignKey(Semester, verbose_name=_('Semester'))
    state = models.PositiveSmallIntegerField(_('State'), choices=STATE)
    max_course = models.PositiveSmallIntegerField(_("Max course"))

    def __unicode__(self):
        return self.get_full_name()

    def get_full_name(self):
        return '%s - %s%s%s%s' % (self.course, unicode(self.speciality.code),
                                  unicode(self.language.short_name), unicode(self.subgroup_letter),
                                  u'ғ' if self.study_type == DISTANT else '')

    def get_abbriviation(self):
        return u"{}{}{}".format(self.language.short_name, self.subgroup_letter,
                                u'ғ' if self.study_type == DISTANT else '')

    def semester_up(self):
        # Last educational year change state to finished
        if (self.current_semester.order / self.course) == 2:
            if self.course == self.max_course:
                self.state = FINISHED
                self.save()
                return

        # Up learning group by one semester
        self.current_semester = Semester.objects.get(order=self.current_semester.order + 1)
        self.course = (self.current_semester.order % 2) + (self.current_semester.order // 2)
        self.save()

    class Meta:
        verbose_name = _("Learning group")
        verbose_name_plural = _("Learning groups")
        permissions = (('move_next_course', 'Move group to the next course'),)


# Departments(Кафедра) belongs to Faculties
# departments can produce specialities
class Department(models.Model):
    """Кафедры"""
    from people.models import User

    name = models.CharField(_('Name'), max_length=100)
    faculty = models.ForeignKey(Faculty, verbose_name=_("Faculty"))
    chief = models.ForeignKey(User, verbose_name=_("User"))

    def __unicode__(self):
        return unicode(self.name)

    class Meta:
        verbose_name = _("Department")
        verbose_name_plural = _("Departments")


class TestTypes(models.Model):
    """Виды экзаменов"""
    code = models.CharField(_('Code'), max_length=20, unique=True)
    name = models.CharField(_('Name'), max_length=50)

    def __unicode__(self):
        return unicode(self.name)

    class Meta:
        verbose_name = _("Test type")
        verbose_name_plural = _("Test types")
