# -*- coding: utf-8 -*-
from learning_process.models import LearningGroup
from core.models import Semester
from django.core.management.base import BaseCommand, CommandError
from django.db import transaction


class Command(BaseCommand):
    help = u'Fix semesters for learning groups'

    def add_arguments(self, parser):
        parser.add_argument('--current_year_half', type=str)

    def handle(self, *args, **options):
        try:
            with transaction.atomic():
                current_year_half = int(options['current_year_half'])
                learning_groups = LearningGroup.objects.all()
                for group in learning_groups:
                    course = group.course
                    current_semester = course * 2
                    if current_year_half % 2 != 0:
                        current_semester -= 1

                    semester = Semester.objects.get(order=str(current_semester))
                    group.current_semester = semester
                    group.save()
        except Exception as e:
            raise CommandError('Exception %s' % e)
