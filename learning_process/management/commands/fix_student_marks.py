# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from educational_plan.models import MarkBook
from django.db import transaction
from people.models import Student, StudentStatistics


class Command(BaseCommand):
    help = u'Fix total marks for student'

    @transaction.atomic
    def handle(self, *args, **options):
        for mark_book in MarkBook.objects.all():
            for key, val in mark_book.marks.items():
                try:
                    total_marks = 0
                    total_absent = 0
                    total_weeks = 0

                    try:
                        student_stat = StudentStatistics.objects.get(
                            student__pk=int(key))
                    except Exception, e:
                        student_stat = StudentStatistics()
                        student_stat.student_id = int(key)
                        student_stat.semester = mark_book.semester_year.semester

                    for k, v in val.items():
                        total_weeks += 1

                        if k == 'CP1' or k == 'CP2':
                            total_marks += float(v)
                        else:
                            total_marks += float(v['mark'])
                            total_absent += float(v['absent'])

                    print "OUTSIDE: Total marks: {}, Total absents: {}".format(
                        total_marks, total_absent
                    )

                    student_stat.total_score += total_marks
                    student_stat.total_weeks += total_weeks
                    student_stat.total_absents += total_absent
                    student_stat.save()

                except Exception, e:
                    print e.message
