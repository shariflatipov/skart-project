# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from educational_plan.models import MarkBook
from learning_process.models import LearningGroup
from learning_process.utils import calculate_gpa
from django.db import transaction


class Command(BaseCommand):
    help = u'Fix student ratings'

    @transaction.atomic
    def handle(self, *args, **options):

        lg = LearningGroup.objects.get(course=3, speciality__code='1.40.01.02.02', language__id=2)

        for mark_book in MarkBook.objects.filter(learning_group=lg):

            print mark_book
            for key, val in mark_book.marks.items():
                score = mark_book.group_subject.testattempts_set.filter(student__id=int(key))
                if len(score) != 0:
                    the_score = score[0].total_score

                    mark_book.marks[str(key)]['FE'] = the_score
                    mark_book.marks[str(key)]['gpa'] = calculate_gpa(mark_book.marks[str(key)], the_score)[0]
                    mark_book.save()

                mark_book.set_rating(key, MarkBook.R_ALL, True)

