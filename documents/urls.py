# -*- coding: utf-8 -*-
from django.conf.urls import url
from . import views

urlpatterns = [
    # ------------ document -------------------
    url(r'document/$', views.document_index, name='document_list'),
    url(r'document/(?P<document_id>[0-9]+)/$', views.document_show, name='document_show'),
    url(r'document/edit/(?P<document_id>[0-9]+)/$', views.document_edit, name='document_edit'),
    url(r'document/add', views.document_add, name='document_add'),

    # ------------ employee -------------------
    # url(r'document/$', 'documents.views.document_index', name='document_index'),
    # url(r'document/(?P<document_id>[0-9]+)/$', 'documents.views.document_show', name='document_show'),
    # url(r'document/edit/(?P<document_id>[0-9]+)/$', 'documents.views.document_edit', name='document_edit'),
    url(r'employee/add', views.employee_contract_add, name='employee_contract_add'),
]
