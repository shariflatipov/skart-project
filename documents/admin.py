from django.contrib import admin

# Register your models here.
from documents.models import *

admin.site.register(Document)
admin.site.register(StudentContract)
admin.site.register(EmployeeContract)
