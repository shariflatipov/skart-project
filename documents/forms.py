# -*- coding: utf-8 -*-
from documents.models import Document, EmployeeContract
from django import forms


class DocumentForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(DocumentForm, self).__init__(*args, **kwargs)
        self.fields['acceptance_date'].widget.attrs['class'] = 'datepicker'

    class Meta:
        model = Document
        exclude = []


class EmployeeContractForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(EmployeeContractForm, self).__init__(*args, **kwargs)
        self.fields['signed_date'].widget.attrs['class'] = 'datepicker'

    class Meta:
        model = EmployeeContract
        exclude = []
