# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext_lazy as _
from core.enums import EDU_ATTENDANT_TYPE_CHOICES
# from core.models import Organization


from people.models import User, Teacher, Student

# --------- Agreement time select --------------

DISTANT = 0
FULL_TIME = 1
PART_TIME = 2
AGREEMENT_TIME_CHOICES = ((DISTANT, _('distant')), (FULL_TIME, _('full time')), (PART_TIME, _('part time')))


class Document(models.Model):
    code = models.CharField(_("Code"), max_length=10)
    name = models.CharField(_("Name"), max_length=200)
    acceptance_date = models.DateField(_("Acceptance date"), blank=True, null=True)
    comment = models.TextField(_("Comment"), blank=True, null=True)
    link_to_template = models.FileField(_("Link to template"), upload_to="documents/templates")

    def __unicode__(self):
        return _(self.name)

    class Meta:
        verbose_name = _('Document')
        verbose_name_plural = _('Documents')


class AbstractContract(models.Model):
    """Базовый класс для договоров"""

    # organization = models.ForeignKey(Organization, verbose_name=_("Organization"))
    signed_date = models.DateField(_('Signed date'))

    class Meta:
        abstract = True


class EmployeeContract(AbstractContract):
    """Договор (контракт) работника"""

    worker = models.ForeignKey(User, verbose_name=_("User"))
    type = models.CharField(_('time'), choices=AGREEMENT_TIME_CHOICES, max_length=100)


class StudentContract(AbstractContract):
    """Договор (контракт) студента"""
    student = models.ForeignKey(Student,verbose_name="Student", limit_choices_to={'is_staff': False})
    education_type = models.PositiveSmallIntegerField(_('Educational type'), choices=EDU_ATTENDANT_TYPE_CHOICES)
    type = models.PositiveSmallIntegerField(_('time'), choices=EDU_ATTENDANT_TYPE_CHOICES)
