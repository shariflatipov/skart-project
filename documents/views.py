# -*- coding: utf-8 -*-
import autocomplete_light.shortcuts as al
from django.core.urlresolvers import reverse
from documents.forms import DocumentForm, EmployeeContractForm
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext, loader
from documents.models import Document

al.autodiscover()


def document_index(request):
    return render(request, 'documents/document/index.html',
                  {'documents': Document.objects.all()})


def document_show(request, document_id):
    document = get_object_or_404(Document, pk=document_id)
    return render(request, 'documents/document/show.html',
                  {'document': document})


def document_edit(request, document_id):
    document = get_object_or_404(Document, pk=document_id)
    form = DocumentForm(instance=document)
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            cd = form.cleaned_data
            document.code = cd["code"]
            document.name = cd["name"]
            document.acceptance_date = cd["acceptance_date"]
            document.comment = cd["comment"]
            document.link_to_template = cd["link_to_template"]
            document.save()
            return HttpResponseRedirect("/documents/document")

    return render(request, 'documents/document/edit.html', {"form": form})


def document_add(request):
    if request.method == 'POST':
        form = DocumentForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect("/documents/document")
    else:
        form = DocumentForm()

    return render(request, 'general_form/add_form.html', {"form": form, "title": 'Document',
                                             "cancel_url": reverse('document_list')})


def employee_contract_add(request):
    if request.method == 'POST':
        form = EmployeeContractForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect("/documents/employee")
    else:
        form = EmployeeContractForm()
        return render(request, 'documents/employee/add.html', {"form": form})
