# -*- coding: utf-8 -*-
from operator import itemgetter
import os
import thread
from datetime import datetime
from django.db.models.query_utils import Q
from django.http.response import HttpResponse
from django.core.files import File
from docx.enum.section import WD_SECTION_START
from docx.enum.style import WD_STYLE_TYPE
from docx.shared import Cm, Pt
from HTMLParser import HTMLParser

import zipfile
import time
import random
from lxml import etree


from docx import Document
from docx.enum.text import WD_ALIGN_PARAGRAPH

from core.enums import DISTANT
from core.models import Grade
from testing.models import (
    Question,
    AnswerOption,
    FitnessQuestion,
    FITNESS,
    MULTISELECT,
    STANDARD,
    TestVariantBase,
    TestVariantShuffled,
    TestAttempts,
    YesNoQuestion,
    YES_NO,
)


class TestParserFromDOCX:
    def __init__(self, *args):
        self.file_name = args[0]
        self.document = Document(self.file_name)
        self.test_case = args[1]

    def parse(self):
        if self.file_name != "":
            try:
                thread.start_new_thread(
                    process_standard, (self.document.tables[0], self.test_case,))
                thread.start_new_thread(
                    process_fitness, (self.document.tables[1], self.test_case,))
                thread.start_new_thread(
                    process_standard, (self.document.tables[2], self.test_case, True,))
                thread.start_new_thread(
                    process_yesno, (self.document.tables[3], self.test_case,))

            except Exception as e:
                print(e)
        else:
            print("file name is required")


class TestParserFromTXT:
    def __init__(self, *args):
        self.file_name = args[0]
        self.document = File(self.file_name)
        self.test_case = args[1]

    def parse(self):

        q = None
        fq = None
        first = True
        g_data = ''
        for line in self.document:
            data = line.decode('utf8')
            if first:
                prefix = data[1:4].strip()
                first = False
                data = data[4:]
            else:
                prefix = data[0:3].strip()
                if len(g_data) == 0:
                    data = data[3:]

            g_data += data

            if prefix == u'Q1':
                q = Question()
                q.question = g_data
                q.test_case = self.test_case
                q.state = Question.ACTIVE
                q.test_type = STANDARD
                q.save()
                g_data = ''
            elif prefix == u'A1':
                ao = AnswerOption()
                ao.is_correct = True
                ao.answer_option = g_data
                ao.question = q
                ao.save()
                g_data = ''
            elif prefix == u'A2' or prefix == u'A3' or prefix == u'A4':
                ao = AnswerOption()
                ao.is_correct = False
                ao.answer_option = g_data
                ao.question = q
                ao.save()
                g_data = ''
            elif prefix == u'F1':
                q = Question()
                q.question = g_data
                q.state = Question.ACTIVE
                q.test_case = self.test_case
                q.test_type = FITNESS
                q.save()
                g_data = ''
            elif prefix == u'S1' or prefix == u'S2' or prefix == u'S3' or prefix == u'S4' or prefix == u'S5':
                fq = FitnessQuestion()
                fq.question = q
                fq.sub_question = g_data
                g_data = ''
            elif prefix == u'O1' or prefix == u'O2' or prefix == u'O3' or prefix == u'O4' or prefix == u'O5':
                fq.answer = g_data
                fq.save()
                g_data = ''


def process_standard(table, test_case, is_multiple=False):
    rows = table.rows
    count = 0

    first = True

    for row in range(len(rows)):
        cells = rows[row].cells

        if count == 5 or first:
            question = cells[1].text.strip()
            if question != "":
                try:
                    # trying to parse the string to int because
                    # question char mast be digit only
                    q = Question()
                    q.test_case = test_case
                    q.question = question
                    q.state = Question.ACTIVE
                    if is_multiple:
                        q.test_type = MULTISELECT
                    else:
                        q.test_type = STANDARD
                    q.save()

                    print("Question saved {}".format(row))

                    count = 0
                    if first:
                        first = False
                except ValueError:
                    raise Exception("invalid row data in row {}, on table".format(row))
            else:
                raise Exception("question is reqiered please check row {}, on table".format(row))
        else:
            # first table (standard questions)
            answer = cells[1].text.strip()
            if answer != "":
                a = AnswerOption()
                a.question = q
                a.answer_option = answer

                # valid answer
                if not is_multiple:
                    a.is_correct = True if count == 1 else False
                else:
                    if cells[2].text.strip() == "+":
                        a.is_correct = True
                    else:
                        a.is_correct = False
                a.save()

                print("Anwers saved {}".format(row))
            else:
                raise Exception("answer is required please check row {}".format(row))
        # second table (fitness questions)
        count += 1


def process_fitness(table, test_case):
    rows = table.rows
    count = 0

    first = True

    for row in range(len(rows)):
        cells = rows[row].cells

        if count == 6 or first:
            question = cells[1].text.strip()
            if question != "":
                try:
                    # trying to parse the string to int because
                    # question char mast be digit only
                    q = Question()
                    q.test_case = test_case
                    q.question = question
                    q.state = Question.ACTIVE
                    q.test_type = FITNESS
                    q.save()

                    print("Question saved {}".format(row))

                    count = 0
                    if first:
                        first = False
                except ValueError:
                    raise Exception("invalid row data in row {}, on table".format(row))
            else:
                raise Exception("question is reqiered please check row {}, on table".format(row))
        else:
            sub_question = cells[1].text.strip()
            answer = cells[3].text.strip()
            fitness_question = FitnessQuestion()
            fitness_question.question = q
            fitness_question.answer = answer
            fitness_question.sub_question = sub_question
            fitness_question.save()

            print("Anwers saved {}".format(row))
        count += 1


def process_yesno(table, test_case):
    rows = table.rows
    count = 0

    first = True

    for row in range(len(rows)):
        cells = rows[row].cells

        if count == 3 or first:
            answer = False
            question = cells[1].text.strip()
            if question != "":
                try:
                    # trying to parse the string to int because
                    # question char mast be digit only
                    q = Question()
                    q.test_case = test_case
                    q.question = question
                    q.state = Question.ACTIVE
                    q.test_type = YES_NO
                    q.save()

                    print("Question saved {}".format(row))

                    count = 0
                    if first:
                        first = False
                except ValueError:
                    raise Exception("invalid row data in row {}, on table".format(row))
            else:
                raise Exception("question is reqiered please check row {}, on table".format(row))
        elif count == 1:
            answer = YesNoQuestion()
            answer.question = q
            # if second row cell 2 is equals to + then Yes is correct
            # else No is correct answer for that question
            answer.correct = True if cells[2].text.strip() == "+" else False
            answer.save()
        elif count == 2:
            count += 1
            continue

            print("Anwers saved {}".format(row))
        count += 1


class TestVersionCreator:
    """ Creates different versions of the tests
    after running make_versions() function

    :argument TestCase
    :argument User
    """

    def __init__(self, *args):
        self.test_case = args[0]
        self.user = args[1]

        if args[0] is None:
            raise ValueError('Test case not set')

        if args[1] is None:
            raise ValueError('User not set')

        if args[2] is not None:
            self.version_count = args[2]
        else:
            self.version_count = 6

    def make_versions(self):

        for i in range(0, int(self.version_count)):
            tvb = TestVariantBase()
            tvb.test_case = self.test_case
            tvb.created_at = datetime.now()
            tvb.code = str(i + 1)
            tvb.creator = self.user
            tvb.save()

            qs = list(self.test_case.question_set.all())

            random.shuffle(qs)
            question_position = 1
            for j in qs:
                tvs = TestVariantShuffled()
                tvs.question = j
                tvs.question_position = question_position
                question_position += 1
                tvs.test_variant_base = tvb

                d = {}
                answer_position = 1

                if j.test_type == STANDARD:
                    answer_option = list(j.answeroption_set.all())
                    random.shuffle(answer_option)
                    for l in answer_option:
                        d[str(l.pk)] = str(answer_position)
                        if l.is_correct:
                            d['c-pos'] = str(answer_position)
                        answer_position += 1

                else:
                    # Todo for better speed i should remove below statement with following j.fitnessquestion_set.all().values('pk')

                    answer_option = list(j.fitnessquestion_set.all().order_by('pk'))

                    correct_answer_position = ['1', '2', '3', '4', '5']
                    random.shuffle(correct_answer_position)

                    for l in answer_option:
                        d[str(l.pk) + "_p"] = str(answer_position)
                        d[str(l.pk) + "_answer-pos"] = correct_answer_position[answer_position - 1]

                        answer_position += 1
                tvs.answers_positions = d

                tvs.save()


class MLStripper(HTMLParser):
    def __init__(self):
        self.reset()
        self.fed = []

    def handle_data(self, d):
        self.fed.append(d)

    def get_data(self):
        return ''.join(self.fed)


def strip_tags(html):
    s = MLStripper()
    s.feed(html)
    return s.get_data()


class TestDocumentCreator:
    def __init__(self, *args):
        self.test_case = args[0]

        self.document = Document()

        section = self.document.sections
        section[0].left_margin = Cm(0.8)
        section[0].right_margin = Cm(0.8)
        section[0].top_margin = Cm(0.8)
        section[0].bottom_margin = Cm(0.8)

        self.response = HttpResponse(
            content_type='application/vnd.openxmlformats-officedocument.wordprocessingml.document'
        )
        self.response['Content-Disposition'] = 'attachment; filename=' + unicode(
            self.test_case.code) + '.docx'

        styles = self.document.styles
        heading_style = styles.add_style('Heading style', WD_STYLE_TYPE.PARAGRAPH)
        font = heading_style.font
        font.size = Pt(12)
        font.name = 'Arial'
        font.bold = True

    # TODO clean duplication of code of parse_question(variant, *args, **kwargs):
    def print_values(self):
        self.document.add_paragraph(u'Номи фан: ' + unicode(self.test_case.subject.name) + '\r' + u'Коди фан: ' +
                                    unicode(self.test_case) + '\r', style='Heading style')

        self.document.add_paragraph(u'I.Тест бо як ҷавоби дуруст', style='Heading style')

        table = self.document.add_table(rows=0, cols=2)
        table.style = 'TableGrid'
        table.autofit = False
        table.columns[0].width = Cm(1)
        table.columns[1].width = Cm(19)

        li = ['A)', 'B)', 'C)', 'D)', 'E)']
        ul = ['1.', '2.', '3.', '4.', '5.']

        question_counter = 1

        # Standard questions
        for t in self.test_case.question_set.filter(test_type=STANDARD):
            row_cells = table.add_row().cells

            row_cells[0].add_paragraph(str(question_counter) + '.', style='Heading style')
            q_text = strip_tags(t.question)
            row_cells[1].add_paragraph(q_text, style='Heading style')

            sub_counter = 0
            for answer in t.answeroption_set.all():
                row_cells = table.add_row().cells
                row_cells[0].add_paragraph(li[sub_counter])
                row_cells[1].add_paragraph(strip_tags(answer.answer_option))
                sub_counter += 1

            question_counter += 1

        question_counter = 1

        self.document.add_paragraph()
        self.document.add_paragraph(u'II. Тести мувофиқоварӣ ', style='Heading style')

        # Fitness questions
        for t in self.test_case.question_set.filter(test_type=FITNESS):

            table = self.document.add_table(rows=0, cols=4)
            table.style = 'TableGrid'

            table.autofit = False
            table.columns[0].width = Cm(1)
            table.columns[1].width = Cm(9)

            table.columns[2].width = Cm(1)
            table.columns[3].width = Cm(9)

            row_cells = table.add_row().cells

            row_cells[0].add_paragraph(str(question_counter) + '.', style='Heading style')
            merged_cell = row_cells[1].merge(row_cells[2])
            merged_cells = merged_cell.merge(row_cells[3])
            merged_cells.add_paragraph(strip_tags(t.question), style='Heading style')

            sub_counter = 0

            for answer in t.fitnessquestion_set.all().order_by("pk"):
                row_cells = table.add_row().cells
                row_cells[0].add_paragraph(li[sub_counter])
                row_cells[1].add_paragraph(strip_tags(answer.sub_question))
                row_cells[2].add_paragraph(ul[sub_counter])
                row_cells[3].add_paragraph(strip_tags(answer.answer))

                sub_counter += 1

            question_counter += 1

        self.document.add_section(WD_SECTION_START.NEW_PAGE)

        self.document.save(self.response)

        return self.response


class ImageGetterFromZip:
    def __init__(self, *args):
        self.file_name = args[0]
        self.test_file_name = args[1]
        self.main()

    def main(self):
        self.openFile()
        self.getListOfFiles()

    def openFile(self):
        try:
            self.zf = zipfile.ZipFile(self.file_name)
        except IOError:
            raise Exception("can not open the file {}".format(self.file_name))

    def getListOfFiles(self):
        files_list = []
        document = None
        xmlcontent = self.zf.read('word/document.xml')
        document = etree.fromstring(xmlcontent)
        for info in self.zf.infolist():
            if (info.filename.find("media") > 0):
                currentFile = self.zf.read(info.filename)
                filePref = info.filename.split('/')[-1]

                directory = "media/testing/sources/images/{}".format(self.test_file_name)
                if not os.path.exists(directory):
                    os.makedirs(directory)
                f = open(directory + "/" + filePref, "w")
                f.write(currentFile)

                files_list.append(filePref)
        if document is not None:
            for tbl in document.iter("{http://schemas.openxmlformats.org/wordprocessingml/2006/main}tbl"):
                for tr in tbl.iter("{http://schemas.openxmlformats.org/wordprocessingml/2006/main}tr"):
                    for element in tr.iter("{http://schemas.openxmlformats.org/wordprocessingml/2006/main}drawing"):

                        element_parent = element.getparent()
                        t = etree.SubElement(element_parent,
                                             "{http://schemas.openxmlformats.org/wordprocessingml/2006/main}t")
                        try:
                            for fn in files_list:
                                height = int(element[0][3].get("cx")) // 100000
                                width = int(element[0][3].get("cy")) // 100000
                                if fn.split(".")[0].lower() == element[0][6].get("name").lower():
                                    print("----------------------------------------------------")
                                    t.text = "<img width = '{1}' height = '{2}'>{3}/{0}</img>".format(
                                        fn, width, height, directory)

                        except:
                            pass
                        element_parent.remove(element)

                    for element in tr.iter("{http://schemas.openxmlformats.org/wordprocessingml/2006/main}object"):
                        element_parent = element.getparent()
                        t = etree.SubElement(element_parent,
                                             "{http://schemas.openxmlformats.org/wordprocessingml/2006/main}t")
                        try:
                            t.text = "<img>" + files_list[1] + "</img>"
                        except:
                            pass
                        element_parent.remove(element)

            zf1 = zipfile.ZipFile(self.file_name, mode='a', )
            try:
                info = zipfile.ZipInfo('word/document.xml', date_time=time.localtime(time.time()), )
                info.compress_type = zipfile.ZIP_DEFLATED
                info.comment = 'Remarks go here'
                info.create_system = 0
                zf1.writestr(info, etree.tostring(document))
            finally:
                zf1.close()


class RegexImage:
    def __init__(self, regex="<img.*>(.+?)</img>"):
        self.strStart = ""
        self.strEnd = ""
        self.image = ""
        self.regex = regex
        self.string = ""

    def get_image_from_string(self, string):
        import re

        self.string = string
        pattern = re.compile(self.regex)
        test = re.search(pattern, self.string)

        if test:
            self.image = test.group(1)
            start = test.start()
            end = test.end()
            self.strStart = self.string[:start]
            self.strEnd = self.string[end:]
        else:
            return False

    def getWidth(self, string):
        import re

        self.regex = "<img.* width = '(.+?)'.*</img>"
        self.string = string
        pattern = re.compile(self.regex)

        test = re.search(pattern, self.string)
        if test:
            return test.group(1)
        else:
            return None

    def getHeight(self, string):
        import re

        self.regex = "<img.* height = '(.+?)'.*</img>"
        self.string = string
        pattern = re.compile(self.regex)

        test = re.search(pattern, self.string)
        if test:
            return test.group(1)
        else:
            return None


def parse_question(test_variant_base):
    """
    Creates list of dicts with question answer pair from TestVariantBase object

    :argument TestVariantBase
    :returns the list of dict of QA where correct answer decorated with css 'class=correct'"""

    tvs = TestVariantShuffled.objects.select_related().filter(test_variant_base=test_variant_base)
    # Standard questions
    tvs_s = tvs.filter(question__test_type=STANDARD).order_by("question_position")
    # Fitness questions
    tvs_f = tvs.filter(question__test_type=FITNESS).order_by("question_position")

    return_l = []
    standard = []
    fitness = []

    # Loop through STANDARD questions
    for t in tvs_s:
        # Get all answers for current question
        answer_option = dict(AnswerOption.objects.filter(question=t.question).values_list('pk', 'answer_option'))

        l = []
        d = {t.question.question: l}

        sorted_list = sorted(t.answers_positions.items(), key=itemgetter(1))
        c_pos = 0
        # Loop through answers for the questions sorted by answer position
        for answer_position in sorted_list:
            if answer_position[0] != "c-pos":
                l.append(answer_option[int(answer_position[0])])
            else:
                c_pos = int(answer_position[1])

        # Decorate correct answer with html paragraph and css class correct
        l[c_pos - 1] = "<p class='correct'>" + l[c_pos - 1] + "</p>"
        standard.append(d)

    # Fitness questions
    for t in tvs_f:
        l = []
        d = {}
        qa = ["", ""]

        # Sorting dictionary with position of answers
        sorted_list = sorted(t.answers_positions.items(), key=itemgetter(1))
        i = 0
        for val in sorted_list:
            g = val[0].split("_")

            if g[1] != "answer-pos":
                qa[0] = t.question.fitnessquestion_set.all().get(pk=g[0]).sub_question
                for v in sorted_list:
                    k = v[0].split("_")
                    if g[0] == k[0]:
                        if k[1] == "answer-pos":
                            if i != 8 and i != 9:
                                qa[0] = qa[0] + "<span class='label label-success' style='float:right'>" + v[
                                    1] + "</span>"
            else:
                qa[1] = t.question.fitnessquestion_set.all().get(pk=g[0]).answer

            i += 1

            if i % 2 == 0 and i > 0:
                l.append(qa)
                qa = ["", ""]

        d[t.question.question] = l
        fitness.append(d)

    return_l.append(standard)
    return_l.append(fitness)
    return return_l


def student_test_result_parser(*args):
    document = args[0]
    print(document)


class ReportToDoc:

    def __init__(self, group_subject):
        self.group_subject = group_subject

    def create_doc(self):
        if self.group_subject:
            course = self.group_subject.learning_group.course
            speciality = u"{} {}".format(
                self.group_subject.learning_group.speciality.code,
                self.group_subject.learning_group.get_abbriviation())
            group = u"{}".format(self.group_subject.learning_group.language.name,
                                 self.group_subject.learning_group.subgroup_letter)
            subject = self.group_subject.test_case.subject
            code = self.group_subject.test_case.code

            if self.group_subject.learning_group.study_type == DISTANT:
                edu_type = u"Гоибона"
            else:
                edu_type = u"Рузона"

            document = Document()

            font = document.styles['Normal'].font
            font.name = 'Arial'
            font.size = Pt(10)

            section = document.sections
            section[0].left_margin = Cm(2)
            section[0].right_margin = Cm(0.8)
            section[0].top_margin = Cm(0.8)
            section[0].bottom_margin = Cm(0.8)

            response = HttpResponse(
                content_type='application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            )

            response['Content-Disposition'] = "attachment; filename=" + unicode(code) + ".docx"

            doc_title = document.add_heading(u'Донишгоҳи давлатии ҳуқуқ, бизнес ва сиёсати Тоҷикистон', 1)
            para_section_name = document.add_paragraph(u'Маркази тести')
            para_doc_number = document.add_paragraph(u'Варақаи имтиҳонии № ___')
            para_subject = document.add_paragraph()
            para_subject.add_run(u" аз фанни ")
            para_subject.add_run(unicode(subject)).bold = True

            doc_title.alignment = WD_ALIGN_PARAGRAPH.CENTER
            para_section_name.alignment = WD_ALIGN_PARAGRAPH.CENTER
            para_doc_number.alignment = WD_ALIGN_PARAGRAPH.CENTER
            para_subject.alignment = WD_ALIGN_PARAGRAPH.CENTER

            p = document.add_paragraph()

            p.add_run(u'Соли хониши 2015-2016')
            p.add_run(u", шӯъбаи ")
            p.add_run(edu_type).bold = True
            p.add_run(u" курси ")
            p.add_run(str(course)).bold = True
            p.add_run(u"\r ихтисоси ")
            p.add_run(unicode(speciality) + ",").bold = True
            p.add_run(u" гр. ")
            p.add_run(group).bold = True
            p.add_run(u"\r коди фан ")
            p.add_run(code).bold = True
            p.add_run(u' санаи: ')
            p.add_run(self.group_subject.date_begin.strftime('%d.%m.%Y'))

            p.alignment = WD_ALIGN_PARAGRAPH.CENTER

            table = document.add_table(rows=0, cols=7)
            table.autofit = False
            table.style = 'TableGrid'

            table.columns[0].width = Cm(1)
            table.columns[1].width = Cm(7)
            table.columns[2].width = Cm(2)
            table.columns[3].width = Cm(2)
            table.columns[4].width = Cm(2)
            table.columns[5].width = Cm(2)
            table.columns[6].width = Cm(2.5)

            row_cells = table.add_row().cells
            r0 = row_cells[0].paragraphs[0]
            r0.add_run(u'№').bold = True
            r0.alignment = WD_ALIGN_PARAGRAPH.CENTER

            r1 = row_cells[1].paragraphs[0]
            r1.add_run(u'Ному насаби донишҷӯ').bold = True
            r1.alignment = WD_ALIGN_PARAGRAPH.CENTER

            r2 = row_cells[2].paragraphs[0]
            r2.add_run(u'ID рақами донишҷӯ').bold = True
            r2.alignment = WD_ALIGN_PARAGRAPH.CENTER

            r3 = row_cells[3].paragraphs[0]
            r3.add_run(u'Варианти тест').bold = True
            r3.alignment = WD_ALIGN_PARAGRAPH.CENTER

            r4 = row_cells[4].paragraphs[0]
            r4.add_run(u'Суммаи холҳо').bold = True
            r4.alignment = WD_ALIGN_PARAGRAPH.CENTER

            r5 = row_cells[5].paragraphs[0]
            r5.add_run(u'Баҳои анъанави').bold = True
            r5.alignment = WD_ALIGN_PARAGRAPH.CENTER

            r6 = row_cells[6].paragraphs[0]
            r6.add_run(u'Сана').bold = True
            r6.alignment = WD_ALIGN_PARAGRAPH.CENTER

            i = 1
            for gss in self.group_subject.testattempts_set.filter(
                    ~Q(state=TestAttempts.PENDING)).order_by('student__last_name'):

                row_cells = table.add_row().cells
                row_cells[0].text = str(i)

                row_cells[1].text = unicode(gss.student)

                row_cells[2].text = gss.student.username
                rc2 = row_cells[2].paragraphs[0]
                rc2.alignment = WD_ALIGN_PARAGRAPH.CENTER

                row_cells[3].text = unicode(gss.test_variant)
                rc3 = row_cells[3].paragraphs[0]
                rc3.alignment = WD_ALIGN_PARAGRAPH.CENTER

                row_cells[4].text = str(gss.total_score)
                rc4 = row_cells[4].paragraphs[0]
                rc4.alignment = WD_ALIGN_PARAGRAPH.CENTER

                row_cells[5].text = str(Grade.get_digital(gss.total_score))
                rc5 = row_cells[5].paragraphs[0]
                rc5.alignment = WD_ALIGN_PARAGRAPH.CENTER

                row_cells[6].text = str(gss.end_time.strftime('%d.%m.%Y'))

                # row_cells[4].text = str(gss.end_time.time().strftime("%H:%M:%S"))
                i += 1

            row_cells = table.add_row().cells
            a = row_cells[0]
            b = row_cells[1]
            c = row_cells[2]
            d = row_cells[3]
            e = row_cells[4]
            A = a.merge(b)
            B = A.merge(c)
            C = B.merge(d)
            D = C.merge(e)

            pc = D.add_paragraph()
            pc.add_run(u"Ба имтиҳон ҳозир нашуданд").bold = True
            pc.alignment = WD_ALIGN_PARAGRAPH.CENTER

            absent = 0
            for gss in self.group_subject.testattempts_set.filter(
                    state=TestAttempts.PENDING).order_by('-total_score'):
                if 1 == 1:  # gss.student.is_active:
                    absent += 1
                    row_cells = table.add_row().cells
                    row_cells[0].text = str(absent)
                    row_cells[1].text = unicode(gss.student)

                    row_cells[2].text = gss.student.username
                    rc2 = row_cells[2].paragraphs[0]
                    rc2.alignment = WD_ALIGN_PARAGRAPH.CENTER

                    row_cells[3].text = str(gss.test_variant)
                    rc3 = row_cells[3].paragraphs[0]
                    rc3.alignment = WD_ALIGN_PARAGRAPH.CENTER

                    row_cells[4].text = str(gss.total_score)
                    rc4 = row_cells[4].paragraphs[0]
                    rc4.alignment = WD_ALIGN_PARAGRAPH.CENTER

                    row_cells[5].text = str(Grade.get_digital(gss.total_score))
                    rc5 = row_cells[5].paragraphs[0]
                    rc5.alignment = WD_ALIGN_PARAGRAPH.CENTER

                    row_cells[6].text = str(gss.end_time.strftime('%d.%m.%Y'))
                    i += 1

            in_group = i - 1
            document.add_paragraph(u'\r Иштирок карданд: {} нафар, ҳ/н {} нафар, \r Ҳамаги дар гуруҳ {} нафар.'.format(
                in_group - absent, absent, in_group))

            p1 = document.add_paragraph()
            p1.add_run(u'\r Сардори маркази тести: ____________________________________ Шамсиев А. А.').bold = True
            document.add_page_break()

            document.save(response)
            return response

        else:
            raise Exception


def return_error_answers(test_attempt_object):
    """
    :param test_attempt_object: is a TestAttempt instance
    :returns dict of answers with it scores"""
    result_std = {}
    result_fit = {}
    group_subject = test_attempt_object.group_subject

    std_count = int(group_subject.standard_question_count)

    for a in range(1, std_count + 1):
        try:
            d = compare_standard(
                test_attempt_object.shuffled_questions[unicode(a)],
                test_attempt_object.answers[unicode(a)],
                group_subject.standard_question_score)
            if d is not None:
                result_std[a] = d
        except KeyError as e:
            continue

    for a in range(std_count + 1, std_count + int(group_subject.fitness_question_count) + 1):
        try:
            d = compare_fitness(
                test_attempt_object.shuffled_questions[unicode(a)],
                test_attempt_object.answers[unicode(a)],
                group_subject.standard_question_score)
            if d.count > 0:
                result_fit[a] = d
        except KeyError as e:
            print(e)
            continue
    return result_std, result_fit,


def compare_fitness(question, answer, score):
    tmp_val = 0
    result = []
    for key, val in answer.items():
        if key == question[val]['answer_position']:
            tmp_val += score
        else:
            q = Question.objects.get(pk=int(question['question_pk']))
            incorrect_answer = FitnessQuestion.objects.get(
                pk=int(question[question[val]['answer_position']]['pk']))
            correct_answer = FitnessQuestion.objects.get(
                pk=int(question[key]['pk']))
            result.append((q.question, correct_answer.sub_question, incorrect_answer.answer, correct_answer.answer))

    return result


def compare_standard(question, answer, score):
    if str(question[str(question['c_pos'])]) == str(answer):
        return None
    else:
        q = Question.objects.get(pk=int(question['question_pk']))
        correct_answer = AnswerOption.objects.get(pk=int(question[str(question['c_pos'])]))

        # If student doesn't chose any answer
        if answer is None:
            return q.question, "", correct_answer.answer_option

        incorrect_answer = AnswerOption.objects.get(pk=int(answer))
        return q.question, incorrect_answer.answer_option, correct_answer.answer_option,


class ErrorToDoc:
    def __init__(self, test_attempt):
        self.test_attempt = test_attempt

    def create_doc(self):
        if self.test_attempt:
            course = self.test_attempt.group_subject.learning_group.course
            speciality = u"{} {}".format(
                self.test_attempt.group_subject.learning_group.speciality.code,
                self.test_attempt.group_subject.learning_group.get_abbriviation())
            group = u"{}".format(self.test_attempt.group_subject.learning_group.language.name,
                                 self.test_attempt.group_subject.learning_group.subgroup_letter)
            subject = self.test_attempt.group_subject.test_case.subject
            code = self.test_attempt.group_subject.test_case.code

            if self.test_attempt.group_subject.learning_group.study_type == DISTANT:
                edu_type = u"Гоибона"
            else:
                edu_type = u"Рузона"

            document = Document()

            font = document.styles['Normal'].font
            font.name = 'Arial'
            font.size = Pt(10)

            section = document.sections
            section[0].left_margin = Cm(2)
            section[0].right_margin = Cm(0.8)
            section[0].top_margin = Cm(0.8)
            section[0].bottom_margin = Cm(0.8)

            response = HttpResponse(
                content_type='application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            )

            response['Content-Disposition'] = "attachment; filename=" + unicode(code) + ".docx"

            doc_title = document.add_heading(u'Донишгоҳи давлатии ҳуқуқ, бизнес ва сиёсати Тоҷикистон', 1)
            para_section_name = document.add_paragraph(u'Маркази тести')
            para_doc_number = document.add_paragraph(u'Натиҷаи имтиҳон')
            para_subject = document.add_paragraph()
            para_subject.add_run(u" аз фанни ")
            para_subject.add_run(unicode(subject)).bold = True

            doc_title.alignment = WD_ALIGN_PARAGRAPH.CENTER
            para_section_name.alignment = WD_ALIGN_PARAGRAPH.CENTER
            para_doc_number.alignment = WD_ALIGN_PARAGRAPH.CENTER
            para_subject.alignment = WD_ALIGN_PARAGRAPH.CENTER

            p = document.add_paragraph()

            p.add_run(u'Соли хониши 2015-2016')
            p.add_run(u", шӯъбаи ")
            p.add_run(edu_type).bold = True
            p.add_run(u" курси ")
            p.add_run(str(course)).bold = True
            p.add_run(u"\r ихтисоси ")
            p.add_run(unicode(speciality) + ",").bold = True
            p.add_run(u" гр. ")
            p.add_run(group).bold = True
            p.add_run(u"\r коди фан ")
            p.add_run(code).bold = True
            p.add_run(u' санаи: ')
            p.add_run(self.test_attempt.group_subject.date_begin.strftime('%d.%m.%Y'))

            p.alignment = WD_ALIGN_PARAGRAPH.CENTER

            st = document.add_paragraph()
            st.add_run(u'Ному насаби донишчу: ')
            st.add_run(self.test_attempt.student.get_full_name())
            t = document.add_table(rows=0, cols=4)
            t.autofit = False
            t.style = 'TableGrid'
            t.columns[0].width = Cm(7)
            t.columns[1].width = Cm(5)
            t.columns[2].width = Cm(4)
            t.columns[3].width = Cm(4)
            r = t.add_row().cells
            r[0].text = u"ID раками донишчу: {}".format(self.test_attempt.student.username)
            r[1].text = u"Варианти тести № {}".format(self.test_attempt.test_variant)
            r[2].text = u"Суммаи холхо: {}".format(self.test_attempt.total_score)
            r[3].text = u"Бахои анъанави: {}".format(Grade.get_digital(self.test_attempt.total_score))

            std_errors, fit_errors = return_error_answers(self.test_attempt)

            t = document.add_table(rows=0, cols=4)
            t.columns[0].width = Cm(1)
            t.columns[1].width = Cm(6)
            t.columns[2].width = Cm(6)
            t.columns[3].width = Cm(6)
            r = t.add_row().cells
            m = r[0].merge(r[1].merge(r[2].merge(r[3])))
            m.paragraphs[0].add_run(u'Тест бо як чавоби дуруст').bold = True
            r = t.add_row().cells
            r[0].paragraphs[0].add_run(u'№').bold = True
            r[1].paragraphs[0].add_run(u'Савол').bold = True
            r[2].paragraphs[0].add_run(u'Ҷавоби интихобшуда').bold = True
            r[3].paragraphs[0].add_run(u'Ҷавоби дуруст').bold = True

            for k, v in std_errors.items():
                r = t.add_row().cells
                r[0].text = str(k)
                r[1].text = v[0]
                r[2].text = v[1]
                r[3].text = v[2]

            r = t.add_row().cells
            m = r[0].merge(r[1].merge(r[2].merge(r[3])))
            m.paragraphs[0].add_run(u'Тести мувофиковари').bold = True

            for k, v in fit_errors.items():
                try:
                    r = t.add_row().cells
                    r[0].text = str(k)
                    m = r[1].merge(r[2].merge(r[3]))
                    m.paragraphs[0].add_run(v[0][0]).bold = True

                    r = t.add_row().cells
                    s = r[0].merge(r[1])
                    s.paragraphs[0].add_run(u'Савол').bold = True
                    r[2].paragraphs[0].add_run(u'Ҷавоби интихобшуда').bold = True
                    r[3].paragraphs[0].add_run(u'Ҷавоби дуруст').bold = True
                    for l in v:
                        r = t.add_row().cells
                        s = r[0].merge(r[1])
                        s.text = l[1]
                        r[2].text = l[2]
                        r[3].text = l[3]
                except Exception:
                    continue

            p1 = document.add_paragraph()
            p1.add_run(u'\r Сардори маркази тести: ____________________________________ Шамсиев А. А.').bold = True
            document.add_page_break()

            document.save(response)
            return response

        else:
            raise Exception
