# -*- coding: utf-8 -*-
# Generated by Django 1.9a1 on 2015-12-15 17:21
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('testing', '0014_auto_20151214_1612'),
    ]

    operations = [
        migrations.AddField(
            model_name='groupsubject',
            name='fitness_question_score',
            field=models.FloatField(default=2, verbose_name='Score for fitness question'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='groupsubject',
            name='standard_question_score',
            field=models.FloatField(default=3, verbose_name='Score for standard question'),
            preserve_default=False,
        ),
    ]
