# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-06-27 06:06
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('testing', '0022_testattempts_test_variant'),
    ]

    operations = [
        migrations.AddField(
            model_name='groupsubject',
            name='current_attempt',
            field=models.IntegerField(choices=[(0, 'First'), (1, 'Second'), (2, 'Third')], default=0),
            preserve_default=False,
        ),
    ]
