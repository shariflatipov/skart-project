# -*- coding: utf-8 -*-
import random
import zipfile
from datetime import datetime
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.db import transaction
from django.db.models.query_utils import Q
from django.http import Http404
from django.http.response import HttpResponse
from django.shortcuts import render, get_object_or_404, redirect
from django.utils.translation import ugettext_lazy as _
from django.utils.timezone import utc
from core.models import Grade, Setting
from core.enums import CREDIT, FINISHED
from learning_process.utils import calculate_gpa
from people.models import Teacher, Student

from .forms import (
    UploadTestForm,
    ShowTestListForm,
    ShowResultsListForm,
    GrantTestAccessForm,
    TestResultForm,
    UnzipTestImages,
    UnauthorizedTestAccessForm
)

from .models import (
    AnswerOption,
    Attempt,
    FITNESS,
    FitnessQuestion,
    GroupSubject,
    MULTISELECT,
    Question,
    StudentTestResult,
    STANDARD,
    TestCase,
    TestVariantBase,
    TestAttempts,
    YES_NO,
    YesNoQuestion,
)

from testing.utils import (
    ErrorToDoc,
    parse_question,
    return_error_answers,
    ReportToDoc,
    TestDocumentCreator,
    TestParserFromTXT,
    TestParserFromDOCX,
)

from django.contrib import messages
from django.core.paginator import Paginator
from skart import settings
from testing.forms import ChangeTestCaseForm
from educational_plan.models import MarkBook


@login_required
@transaction.atomic()
def upload_test(request):
    if request.method == "POST":
        form = UploadTestForm(request.POST, request.FILES)
        if form.is_valid():
            cd = form.cleaned_data

            with transaction.atomic():
                test_case = TestCase.objects.get(code=cd['code'])
                if cd['from_docx']:
                    document_parser = TestParserFromDOCX(request.FILES['file'],
                                                         test_case)
                else:
                    document_parser = TestParserFromTXT(request.FILES['file'],
                                                        test_case)
                document_parser.parse()

                return redirect((reverse(upload_test)))
    else:
        form = UploadTestForm()

    return render(request, 'testing/upload_test.html', {'form': form})


@login_required
def show_test_list(request):
    context = {'show_filters': True}
    kwargs = {}

    if request.method == "POST":
        form = ShowTestListForm(request.POST)

        if form.is_valid():
            cd = form.cleaned_data

            if cd['subject'] is not None:
                kwargs['subject'] = cd['subject']
            if cd['teacher'] is not None:
                kwargs['teacher'] = cd['teacher']
            if cd['code'] is not None:
                kwargs['group_subject__test_case'] = cd['code']
            if cd['group'] is not None:
                kwargs['learning_group'] = cd['group']
    else:
        form = ShowTestListForm()

    if not request.user.is_superuser:
        kwargs['teacher'] = Teacher.objects.filter(user_ptr=request.user)
        context['show_filters'] = False

    test_case_list = MarkBook.objects.filter(**kwargs).exclude(state=FINISHED)
    paginator = Paginator(test_case_list, settings.PAGINATION_LIMIT)
    page = request.GET.get('page')
    try:
        test_case = paginator.page(page)
    except Exception as e:
        print(e)
        # If page is not an integer, deliver first page.
        test_case = paginator.page(1)
    context.update({'form': form, 'data': test_case})

    return render(request, 'testing/show_test_list.html', context)


@login_required
def delete_test(request, test_id):
    t_c = get_object_or_404(TestCase, pk=test_id)
    t_c.question_set.all().delete()
    return redirect(reverse(show_test_list))


@login_required
def download_test(request, test_id):
    t_c = get_object_or_404(TestCase, ~Q(state=TestCase.DELETED), pk=test_id)
    tdc = TestDocumentCreator(t_c)
    return tdc.print_values()


@login_required
def show_test_internal(request, test_id):
    t_c = get_object_or_404(TestCase, ~Q(state=TestCase.DELETED), pk=test_id)
    standard = t_c.question_set.filter(test_type=STANDARD).order_by('pk')
    fitness = t_c.question_set.filter(test_type=FITNESS).order_by('pk')
    return render(request,
                  'testing/test_internals.html',
                  {'test_case': t_c, 'fitness': fitness, 'standard': standard})


@login_required
def show_variant(request, variant_id):
    data = get_object_or_404(TestVariantBase,
                             ~Q(test_case__state=TestCase.DELETED),
                             pk=variant_id)

    val = parse_question(data)
    return render(request, 'testing/show_variant.html',
                  {'standard': val[0], 'fitness': val[1]})


@login_required
def show_results(request):
    results = None

    if request.method == 'POST':
        form = ShowResultsListForm(request.POST)
        if form.is_valid():

            kwargs = {}
            cd = form.cleaned_data
            test_case = cd['test_case']

            if test_case:
                kwargs['test_variant_base__test_case'] = test_case

            group = cd['group']
            if group:
                kwargs['student__learning_group'] = group
            results = StudentTestResult.objects.filter(
                **kwargs).select_related('student',
                                         'test_variant_base__test_case__subject')
    else:

        form = ShowResultsListForm()
    return render(request, 'testing/show_results.html',
                  {'form': form, 'data': results})


@login_required
def to_document_test_result(request, group_subject_id):
    group_subject = get_object_or_404(GroupSubject, pk=group_subject_id)

    report_to_doc = ReportToDoc(group_subject)
    create = report_to_doc.create_doc()
    return create


@login_required
def append_standard_test(request, test_case):
    t_c = get_object_or_404(TestCase, ~Q(state=TestCase.DELETED), pk=test_case)
    ctx = {'subject': t_c.subject, 'question_count': t_c.question_set.count()}

    if request.method == 'POST':
        question = Question()
        question.test_case = t_c
        question.question = request.POST.get('v0', '')
        question.test_type = STANDARD
        question.state = Question.ACTIVE
        question.save()

        correct_answer = AnswerOption()
        correct_answer.answer_option = request.POST.get('v1', '')
        correct_answer.is_correct = True
        correct_answer.question = question
        correct_answer.save()

        for i in range(2, 5):
            correct_answer = AnswerOption()
            correct_answer.answer_option = request.POST.get('v%s' % i, '')
            correct_answer.is_correct = False
            correct_answer.question = question
            correct_answer.save()
    return render(request,
                  'testing/online_test/test_editor_standard.html', ctx)


@login_required
def append_fitness_test(request, test_case):
    t_c = get_object_or_404(TestCase, ~Q(state=TestCase.DELETED), pk=test_case)
    ctx = {'subject': t_c.subject, 'question_count': t_c.question_set.count()}

    if request.method == 'POST':
        question = Question()
        question.test_case = t_c
        question.question = request.POST.get('v0', '')
        question.test_type = FITNESS
        question.state = Question.ACTIVE
        question.save()

        i = 0
        while i < 10:
            i += 1
            fitness = FitnessQuestion()
            fitness.sub_question = request.POST.get('v%s' % i, '')
            i += 1
            fitness.answer = request.POST.get('v%s' % i, '')
            fitness.question = question
            fitness.save()

    return render(request, 'testing/online_test/test_editor_fitness.html', ctx)


@login_required
def show_available_test_list(request):
    allowed_ips = ['192.168.0.215', '192.168.0.216', '192.168.0.197',
                   'localhost', '127.0.0.1']

    if request.META['REMOTE_ADDR'] not in allowed_ips:
        raise Http404

    student = Student.objects.get(user_ptr=request.user)
    group_subject = GroupSubject.objects.filter(
        markbook__learning_group=student.learning_group,
        state=GroupSubject.ACTIVE)
    if group_subject.count() > 1:
        return render(request, 'testing/online_test/choose_test.html',
                      {"tests": group_subject})
    elif group_subject.count() == 1:
        return redirect('pass_test_check', test=group_subject[0].pk)
    else:
        return HttpResponse(_("Error"))


@login_required
def pass_test_check(request, test):
    """ Choose test from list of available tests"""
    student = Student.objects.get(user_ptr=request.user)
    group_subject = GroupSubject.objects.get(pk=test)
    attempt = TestAttempts.objects.get(group_subject=group_subject,
                                       student=student, attempt=group_subject.current_attempt)

    if attempt.state == TestAttempts.FINISHED:
        return redirect('test_result', group_subject_student=attempt.pk)
    return redirect('real_pass_test', group_student=attempt.pk, question=1)


def shuffle_tests(group_subject):
    print("Begin shuffling tests")
    question_positions = 1

    result = {}
    standard_count = group_subject.standard_question_count
    fitness_count = group_subject.fitness_question_count
    yes_no_count = group_subject.yes_no_count
    multiselect_count = group_subject.multiselect_count

    print("Collected questions count")
    standard_questions = group_subject.test_case.question_set.filter(
        test_type=STANDARD).order_by('?')[:standard_count]
    fitness_questions = group_subject.test_case.question_set.filter(
        test_type=FITNESS).order_by('?')[:fitness_count]
    multiselect_questions = group_subject.test_case.question_set.filter(
        test_type=MULTISELECT).order_by('?')[:multiselect_count]
    yesno_questions = group_subject.test_case.question_set.filter(
        test_type=YES_NO).order_by('?')[:yes_no_count]
    print("Retrieved questions")
    # {{ question_position : {answer_option_position : answer_pk, ...
    # { {1: { 1: 1, 2: 4, 3: 2, 4: 2, c_pos: 2, question_pk=1 }, ... }
    correct_answer_position = ['1', '2', '3', '4']

    print("Generating standard questions")

    for question in standard_questions:
        random.shuffle(correct_answer_position)
        i = 0
        result[question_positions] = {}
        for answer in question.answeroption_set.filter(question__test_type=STANDARD):
            result[question_positions][correct_answer_position[i]] = answer.pk
            if answer.is_correct:
                result[question_positions]['c_pos'] = correct_answer_position[i]
            i += 1
        result[question_positions]['question_pk'] = question.pk
        result[question_positions]['test_type'] = question.test_type
        question_positions += 1
    # { 20 : { 1 : {'pk': 2, 'answer_position': 4 },
    #           2 : {'pk': 3, 'answer_position': 5 },
    #           3 : {'pk': 1, 'answer_position': 3 },
    #           4 : {'pk': 5, 'answer_position': 1 },
    #           5 : {'pk': 4, 'answer_position': 2 },
    #    question_pk=21 }

    correct_answer_position = ['1', '2', '3', '4', '5']
    print("Generating fitness questions")
    for question in fitness_questions:
        random.shuffle(correct_answer_position)
        i = 1
        result[question_positions] = {}
        for answer in question.fitnessquestion_set.all().order_by('pk'):
            result[question_positions][i] = {}
            result[question_positions][i]['pk'] = answer.pk
            result[question_positions][i]['answer_position'] = correct_answer_position[i - 1]
            i += 1
        result[question_positions]['question_pk'] = question.pk
        result[question_positions]['test_type'] = question.test_type
        question_positions += 1

    print("Generating multiselect questions")
    for question in multiselect_questions:
        random.shuffle(correct_answer_position)
        i = 0
        result[question_positions] = {}
        result[question_positions]['c_pos'] = []

        for answer in question.answeroption_set.filter(question__test_type=MULTISELECT):
            result[question_positions][correct_answer_position[i]] = answer.pk
            if answer.is_correct:
                result[question_positions]['c_pos'].append(answer.pk)
            i += 1
        result[question_positions]['question_pk'] = question.pk
        result[question_positions]['test_type'] = question.test_type
        question_positions += 1
        # { 20 : { 1 : {'pk': 2, 'answer_position': 4 },
        #           2 : {'pk': 3, 'answer_position': 5 },
        #           3 : {'pk': 1, 'answer_position': 3 },
        #           4 : {'pk': 5, 'answer_position': 1 },
        #           5 : {'pk': 4, 'answer_position': 2 },
        #    question_pk=21 }

    print("Generating yes/no questions")
    for question in yesno_questions:
        result[question_positions] = {}
        for answer in question.yesnoquestion_set.all():
            result[question_positions]['question_pk'] = question.pk
            result[question_positions]['test_type'] = question.test_type
            result[question_positions]['correct'] = \
                'no' if answer.correct == YesNoQuestion.NO else 'yes'

        question_positions += 1

    return result


def compare_standard(answered, comparable, score):
    if str(comparable[str(comparable['c_pos'])]) == str(answered):
        return score
    else:
        return 0


def compare_fitness(answered, comparable, score):
    tmp_val = 0
    for key, val in answered.items():
        if key == comparable[val]['answer_position']:
            tmp_val += score
    return tmp_val


def compare_multiselect(answered, comparable, score):
    tmp_score = score / len(comparable['c_pos'])
    total_score = 0

    for answer in answered:
        if int(answer) not in comparable['c_pos']:
            return 0
        else:
            total_score += tmp_score
    return round(total_score, 2)


def compare_yesno(answered, comparable, score):
    return score if answered == comparable['correct'] else 0


@login_required
@transaction.atomic
def test_result(request, group_subject_student):
    test_attempt = get_object_or_404(TestAttempts, pk=group_subject_student)
    std_count = test_attempt.group_subject.standard_question_count
    std_score = test_attempt.group_subject.standard_question_score

    ftn_count = test_attempt.group_subject.fitness_question_count
    ftn_score = test_attempt.group_subject.fitness_question_score

    multiselect_count = test_attempt.group_subject.multiselect_count
    multiselect_score = test_attempt.group_subject.multiselect_score

    yesno_count = test_attempt.group_subject.yes_no_count
    yesno_score = test_attempt.group_subject.yes_no_score

    max_score = (std_count * std_score) + (ftn_count * 4 * ftn_score) + (
        multiselect_count * multiselect_score) + (yesno_score * yesno_count)

    total_score = 0
    if test_attempt.total_score == 0:
        for key, value in test_attempt.shuffled_questions.items():
            try:
                print(value)

                if 'test_type' in value:
                    if value['test_type'] == STANDARD:
                        total_score += compare_standard(test_attempt.answers[key], value, std_score)
                        print("Score for standard questions: {}".format(total_score))
                    elif value['test_type'] == FITNESS:
                        total_score += compare_fitness(test_attempt.answers[key], value, ftn_score)
                        print("Score for fitness questions: {}".format(total_score))
                    elif value['test_type'] == MULTISELECT:
                        total_score += compare_multiselect(
                            test_attempt.answers[key],
                            value,
                            multiselect_score
                        )
                        print("Score for multiselect questions: {}".format(total_score))
                    elif value['test_type'] == YES_NO:
                        total_score += compare_yesno(test_attempt.answers[key], value, yesno_score)
                        print("Score for yesno questions: {}".format(total_score))
                else:
                    if int(key) > std_count:
                        total_score += compare_fitness(test_attempt.answers[key], value, ftn_score)
                    else:
                        total_score += compare_standard(test_attempt.answers[key], value, std_score)
            except KeyError:
                continue
        test_attempt.total_score = total_score
        test_attempt.save()
    else:
        total_score = test_attempt.total_score

    student_errors_std, student_errors_fit = return_error_answers(test_attempt)

    study_model = {}
    if test_attempt.group_subject.markbook.learning_group.study_model == CREDIT:
        study_model['study_model'] = 'CREDIT'

    # Calculate GPA
    mark_book = test_attempt.group_subject.markbook
    gpa = (0, 'F', 0, 0)

    if str(request.user.pk) in mark_book.marks:
        gpa = calculate_gpa(mark_book.marks[str(request.user.pk)], total_score)
        mb = mark_book
        mb.marks[str(request.user.pk)]['FE'] = total_score
        mb.marks[str(request.user.pk)]['gpa'] = gpa[0]
        mb.save()
        mb.set_rating(str(request.user.pk), MarkBook.R_ALL, True)
    else:
        gpa = (0, 'F', 0, 0)

    percent = total_score * 100 / max_score
    grade = Grade.get_digital(int(percent))

    ctx = {'total_score': total_score,
           'percent': percent,
           'grade': grade,
           'student_errors': student_errors_std,
           'student_errors_fit': student_errors_fit,
           'test_variant': test_attempt.test_variant,
           'gpa': gpa}

    ctx.update(study_model)

    return render(request, 'testing/online_test/test_result.html', ctx)


@login_required
@transaction.atomic
def real_pass_test(request, group_student, question=1):
    test_attempt = TestAttempts.objects.get(pk=group_student)

    # Student started passing test
    if test_attempt.state == TestAttempts.PENDING:
        test_attempt.state = TestAttempts.STARTED
        test_attempt.begin_time = datetime.now()
        test_attempt.save()

    std_count = test_attempt.group_subject.standard_question_count
    ftn_count = test_attempt.group_subject.fitness_question_count
    multi_count = test_attempt.group_subject.multiselect_count
    yesno_count = test_attempt.group_subject.yes_no_count

    if test_attempt.state == TestAttempts.FINISHED:
        return redirect('test_result', group_subject_student=test_attempt.pk)

    begin = test_attempt.begin_time.replace(tzinfo=utc)
    now = datetime.utcnow().replace(tzinfo=utc)
    diff = now - begin
    minutes_passed = divmod(diff.days * 86400 + diff.seconds, 60)

    if minutes_passed[0] >= test_attempt.group_subject.time_limit:
        test_attempt.state = TestAttempts.FINISHED
        test_attempt.end_time = datetime.now()
        test_attempt.save()
        return redirect('test_result', group_subject_student=test_attempt.pk)

    question_index = int(question)

    if question_index <= 0:
        question_index = 1
        question = "1"
    if question_index >= std_count + ftn_count + multi_count + yesno_count + 1:
        question_index = std_count + ftn_count
        question = str(std_count + ftn_count)

    if request.method == 'POST':
        if 'back' in request.POST:
            return redirect('real_pass_test', group_student=group_student,
                            question=question_index - 1)
        else:
            if 'fitness' in request.POST:
                test_attempt.answers[question] = {}
                for i in range(1, 5):
                    test_attempt.answers[question][i] = request.POST.get('answer_%s' % i)
            elif 'standard' in request.POST:
                test_attempt.answers[question] = request.POST.get('question')
            elif 'multiselect' in request.POST:
                test_attempt.answers[question] = []

                for i in range(1, 5):
                    if request.POST.get('question_%s' % i):
                        test_attempt.answers[question].append(request.POST.get('question_%s' % i))

            elif 'yesno' in request.POST:
                test_attempt.answers[question] = request.POST.get('yesno')
            test_attempt.save()
            if 'finish' in request.POST:
                test_attempt.state = TestAttempts.FINISHED
                test_attempt.end_time = datetime.now()
                test_attempt.save()
                return redirect('test_result', group_subject_student=test_attempt.pk)
            else:
                print('here from url maybe')

        return redirect('real_pass_test', group_student=group_student,
                        question=question_index + 1)
    else:
        quest = test_attempt.shuffled_questions
        previous_answer = {'answered': ''}
        shuffled_question = {}
        q = Question.objects.get(pk=quest[question]['question_pk'])
        shuffled_question.update({'question': q.question})
        answers_fitness = {}
        answers_standard = []

        # Standard questions
        if q.test_type == STANDARD:
            shuffled_question.update({'type': 'standard'})
            question_answers = q.answeroption_set.all().order_by('?')
            for a in question_answers:
                answers_standard.append({a.pk: a.answer_option})

                if question in test_attempt.answers:
                    if test_attempt.answers[question]:
                        previous_answer['answered'] = test_attempt.answers[question]

        # Fitness questions
        elif q.test_type == FITNESS:
            shuffled_question.update({'type': 'fitness'})
            for i in range(1, 6):
                fq = FitnessQuestion.objects.get(pk=quest[question][str(i)]['pk'])
                answer_position_pk = int(quest[question][str(i)]['answer_position'])
                fqa = FitnessQuestion.objects.get(
                    pk=quest[question][str(answer_position_pk)]['pk'])
                # {position: {'question': sub_question, 'answer': answer}, ...}
                answers_fitness.update({i: {'question': fq.sub_question, 'answer': fqa.answer}})
                l = {}

                if question in test_attempt.answers:
                    for k, v in test_attempt.answers[question].items():
                        l[int(k)] = int(v)
                    previous_answer['answered'] = l
        elif q.test_type == MULTISELECT:
            shuffled_question.update({'type': 'multiselect'})

            question_answers = q.answeroption_set.all().order_by('?')
            for a in question_answers:
                answers_standard.append({a.pk: a.answer_option})

                if question in test_attempt.answers:
                    if test_attempt.answers[question]:
                        previous_answer['answered'] = test_attempt.answers[question]

        elif q.test_type == YES_NO:
            shuffled_question.update({'type': 'yesno'})

        shuffled_question.update({'answers_fitness': answers_fitness,
                                  'answers_standard': answers_standard})
        hours = 0
        minutes = test_attempt.group_subject.time_limit - minutes_passed[0] - 1
        seconds = 59 - minutes_passed[1]

        # Show finish button
        finish = (question_index == std_count + ftn_count + multi_count + yesno_count)

        return render(request, 'testing/online_test/student_pass_test.html',
                      {'student': test_attempt.student, 'question': shuffled_question,
                       'subject': test_attempt.group_subject.markbook.subject,
                       'time_limit': {'h': hours, 'm': minutes, 's': seconds},
                       'finish': finish, 'question_number': question,
                       'answered': previous_answer})


@login_required
def grant_test_access(request):
    if request.method == 'POST':
        form = GrantTestAccessForm(request.POST)
        if form.is_valid():
            try:
                cd = form.cleaned_data
                test_case = cd['test_case']
                # subject = cd['subject']
                group = cd['group']
                time_limit = cd['time_limit']
                access_granted = cd['access_granted']
                attempt = int(cd['attempt'])

                group_subject = GroupSubject.objects.get(
                    test_case=test_case,
                    markbook__learning_group=group
                )

                with transaction.atomic():
                    if access_granted:
                        if group_subject.state == GroupSubject.ACTIVE:
                            messages.error(request, _("Access to this test already granted"))
                        else:
                            variant = 1
                            if attempt == Attempt.FIRST:
                                # Activate test and set properties
                                group_subject.state = GroupSubject.ACTIVE
                                group_subject.time_limit = time_limit
                                group_subject.current_attempt = Attempt.FIRST

                                group_subject.standard_question_count = Setting.objects.get(
                                    variable='STANDARD_TEST_COUNT').value
                                group_subject.standard_question_score = Setting.objects.get(
                                    variable='STANDARD_TEST_SCORE').value

                                group_subject.fitness_question_count = Setting.objects.get(
                                    variable='FITNESS_TEST_COUNT').value
                                group_subject.fitness_question_score = Setting.objects.get(
                                    variable='FITNESS_TEST_SCORE').value

                                group_subject.multiselect_score = Setting.objects.get(
                                    variable='MULTISELECT_SCORE').value
                                group_subject.multiselect_count = Setting.objects.get(
                                    variable='MULTISELECT_COUNT').value

                                group_subject.yes_no_score = Setting.objects.get(
                                    variable='YESNO_SCORE').value
                                group_subject.yes_no_count = Setting.objects.get(
                                    variable='YESNO_COUNT').value

                                group_subject.date_begin = datetime.now()
                                group_subject.save()

                                # Shuffle test for all students
                                for student in group.student_set.all():

                                    # Generate test atempt for student
                                    test_attempt = TestAttempts()
                                    test_attempt.test_variant = variant
                                    test_attempt.group_subject = group_subject
                                    test_attempt.student = student
                                    test_attempt.answers = dict({})
                                    test_attempt.begin_time = datetime.now()
                                    test_attempt.end_time = datetime.now()
                                    test_attempt.total_score = 0
                                    test_attempt.attempt = attempt
                                    test_attempt.shuffled_questions = shuffle_tests(group_subject)
                                    test_attempt.state = TestAttempts.PENDING
                                    test_attempt.save()
                                    variant += 1
                            elif attempt == Attempt.SECOND:

                                # Reactivate Group subject
                                group_subject.state = GroupSubject.ACTIVE
                                group_subject.current_attempt = Attempt.SECOND
                                group_subject.save()

                                for student in group.student_set.all():
                                    ta = TestAttempts.objects.filter(
                                        group_subject=group_subject,
                                        student=student).order_by('-pk')

                                    if ta[0].attempt != Attempt.FIRST and Grade.get_digital(
                                       ta[0].total_score) == 2:
                                        for t in ta:
                                            if Grade.get_digital(t.total_score) == 2:
                                                test_attempt = TestAttempts()
                                                test_attempt.test_variant = variant
                                                test_attempt.group_subject = group_subject
                                                test_attempt.student = student
                                                test_attempt.answers = dict({})
                                                test_attempt.begin_time = datetime.now()
                                                test_attempt.end_time = datetime.now()
                                                test_attempt.total_score = 0
                                                test_attempt.attempt = attempt
                                                test_attempt.shuffled_questions = shuffle_tests(group_subject)
                                                test_attempt.state = TestAttempts.PENDING
                                                test_attempt.save()
                                                variant += 1

                            messages.info(request, _("Access granted to test"))
                    else:
                        group_subject.state = GroupSubject.FINISHED
                        # group_subject.testattempts_set.all().update(state=TestAttempts.FINISHED)
                        group_subject.save()
                        messages.info(request, _("Access closed to test"))
            except Exception as e:
                messages.error(request, _("Error while saving: {}".format(str(e))))
    else:
        form = GrantTestAccessForm()
    return render(request, 'testing/online_test/grant_test_access.html',
                  {'form': form})


def show_unauthorized_tests(request):
    if request.method == 'POST':
        form = UnauthorizedTestAccessForm(request.POST)
        if form.is_valid():
            kwargs = {}
            try:
                cd = form.cleaned_data
                subject = cd['subject']
                if subject:
                    kwargs['test_case__subject'] = subject
                group = cd['group']
                if group:
                    kwargs['markbook__learning_group'] = group

                group_subject = GroupSubject.objects.filter(**kwargs)

                if len(group_subject) == 1:
                    return redirect(reverse(
                        'show_internal_unauthorized_tests',
                        kwargs={'group_subject_id': group_subject[0].pk}))
                else:
                    return render(request, 'testing/online_test/search_tests.html',
                                  {'form': form, 'test_list': group_subject})

            except Exception as e:
                messages.error(request, _("Error while saving: {}".format(str(e))))
    else:
        form = UnauthorizedTestAccessForm()
    return render(request, 'testing/online_test/search_tests.html',
                  {'form': form})


def choose_unauthorized_test(request, group_subject_id):
    group_subject = get_object_or_404(GroupSubject, pk=group_subject_id)

    ctx = {'test_case': group_subject.test_case}
    ctx.update(get_shuffled_test(group_subject.test_case))
    return render(request,
                  'testing/online_test/show_unauthorized_test.html',
                  ctx)


def get_shuffled_test(test_case):
    return {'standard': test_case.question_set.filter(test_type=STANDARD),
            'fitness': test_case.question_set.filter(test_type=FITNESS)}


@login_required
def edit_standard_test(request, question):
    q = get_object_or_404(Question, ~Q(state=Question.DISABLED), pk=question)

    if request.method == 'GET':
        ao = q.answeroption_set.all().order_by('pk')
        ctx = {}
        i = 2

        for a in ao:
            if a.is_correct:
                ctx['c_a'] = a.answer_option
            else:
                ctx['a%s' % i] = a.answer_option
                i += 1

        ctx.update({'q': q.question})
        ctx['subject'] = q.test_case.subject
        ctx['question_count'] = q.test_case.question_set.count()

        return render(request,
                      'testing/online_test/test_editor_standard.html',
                      ctx)
    else:
        q.question = request.POST.get('v0', '')
        q.save()

        q.answeroption_set.all().delete()

        correct_answer = AnswerOption()
        correct_answer.answer_option = request.POST.get('v1', '')
        correct_answer.is_correct = True
        correct_answer.question = q
        correct_answer.save()

        for i in range(2, 5):
            correct_answer = AnswerOption()
            correct_answer.answer_option = request.POST.get('v%s' % i, '')
            correct_answer.is_correct = False
            correct_answer.question = q
            correct_answer.save()
    return redirect(reverse('show_test', kwargs={'test_id': q.test_case.pk}))


@login_required
def edit_fitness_test(request, question):
    q = get_object_or_404(Question, ~Q(state=Question.DISABLED), pk=question)
    ctx = {'q': q.question}
    i = 1
    if request.method == 'GET':
        fq = FitnessQuestion.objects.filter(question__state=Question.ACTIVE, question=q).order_by('pk')
        for f in fq:
            ctx['q%s' % str(i)] = f.sub_question
            ctx['a%s' % str(i)] = f.answer
            i += 1
        ctx['subject'] = q.test_case.subject
        ctx['question_count'] = q.test_case.question_set.count()
    else:
        q.question = request.POST.get('v0', '')
        q.save()

        q.fitnessquestion_set.all().delete()

        i = 0
        while i < 10:
            i += 1
            fitness = FitnessQuestion()
            fitness.sub_question = request.POST.get('v%s' % i, '')
            i += 1
            fitness.answer = request.POST.get('v%s' % i, '')
            fitness.question = q
            fitness.save()

        return redirect(reverse('show_tests'))
    return render(request,
                  'testing/online_test/test_editor_fitness.html', ctx)


@login_required
def online_test_result(request):
    kwargs = {'state': GroupSubject.FINISHED}

    if request.method == 'POST':
        form = TestResultForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data

            group = cd['group']
            subject = cd['subject']

            if group:
                kwargs['learning_group'] = group
            if subject:
                kwargs['test_case__subject'] = subject

            test_results = GroupSubject.objects.filter(**kwargs)
        else:
            test_results = None
    else:
        form = TestResultForm()
        test_results = GroupSubject.objects.filter(state=GroupSubject.FINISHED)
    return render(request, 'testing/online_test/online_test_results.html',
                  {'form': form, 'test_results': test_results})


@login_required
def download_test_result(request, group_subject):
    group = get_object_or_404(GroupSubject, pk=group_subject)
    report_to_doc = ReportToDoc(group)
    result = report_to_doc.create_doc()
    return result


@login_required
def download_student_errors(request, test_attempt):
    ta = get_object_or_404(TestAttempts, pk=test_attempt)
    error_to_doc = ErrorToDoc(ta)
    result = error_to_doc.create_doc()
    return result


@login_required
def unzip_test_images(request):
    """Unzip uploaded images (testing)"""
    if request.method == 'POST':
        form = UnzipTestImages(request.POST)
        if form.is_valid():
            with zipfile.ZipFile(request.FILES['file'], 'r') as z:
                z.extractall(settings.MEDIA_ROOT)
    else:
        form = UnzipTestImages()
    return render(request, 'testing/online_test/upload_test_zip_images.html',
                  {'form': form})


@login_required
def close_tests_access(request):
    """Close all test access """

    GroupSubject.objects.filter(state=GroupSubject.ACTIVE).update(state=GroupSubject.FINISHED)
    return redirect(reverse('online_test_result'))


@login_required
@transaction.atomic()
def change_mark_book_test_case(request):
    """
    Change test case of all mark books to another
    if the teacher is own both of them
    """
    form = ChangeTestCaseForm()
    if request.method == 'POST':
        form = ChangeTestCaseForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            if cd['old_test_case'] == cd['new_test_case']:
                messages.error(request, _('Impossible to change test case to itself'))
            else:
                GroupSubject.objects.filter(
                    test_case=cd['old_test_case']).update(test_case=cd['new_test_case'])
                cd['old_test_case'].state = TestCase.DELETED
                cd['old_test_case'].save()
                messages.info(request, _("All data saved correctly"))

    return render(request, 'testing/change_test_case.html', {"form": form})


@login_required
def to_document_student_error_doc(request):
    kwargs = {'state': GroupSubject.FINISHED}

    if request.method == 'POST':
        form = TestResultForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data

            group = cd['group']
            subject = cd['subject']

            if group:
                kwargs['group_subject__learning_group'] = group
            if subject:
                kwargs['group_subject__test_case__subject'] = subject

            test_results = TestAttempts.objects.filter(**kwargs)
        else:
            test_results = None
    else:
        form = TestResultForm()
        test_results = TestAttempts.objects.filter(state=TestAttempts.FINISHED)
    return render(request, 'testing/online_test/student_errors.html',
                  {'form': form, 'test_results': test_results})
