# -*- coding: utf-8 -*-

from django.db import models
from core.models import Subject
from django.utils.translation import ugettext_lazy as _
from django.contrib.postgres.fields import HStoreField
from django.contrib.postgres.fields.jsonb import JSONField

from people.models import Teacher, User, Student

STANDARD = u'0'
FITNESS = u'1'
MULTISELECT = u'2'
YES_NO = u'3'
TEST_TYPE = (
    (_('Standard'), STANDARD),
    (_('Fitness'), FITNESS),
    (_('Multiselect'), MULTISELECT),
    (_('Yes/No'), YES_NO)
)


class TestCase(models.Model):
    NEW = 0
    DISABLED = 1
    DELETED = 2
    PROCESSED = 3

    STATE = ((DISABLED, _('Disabled')), (DELETED, _('Deleted')),
             (NEW, _('New')), (PROCESSED, _('Processed')))

    """ Test case is a base class for tests
        Contains questions, fitness questions
        Variants for tests
    """

    subject = models.ForeignKey(Subject, verbose_name=_('Subject'))
    teacher = models.ForeignKey(Teacher, verbose_name=_('Teacher'))
    description = models.CharField(_('Description'), max_length=250)
    code = models.CharField(_('Code'), max_length=80, unique=True)
    state = models.PositiveSmallIntegerField(_('State'), choices=STATE)

    def __unicode__(self):
        return self.code


class Question(models.Model):
    """ Questions """

    ACTIVE = 1
    DISABLED = 2
    DELETED = 3

    STATE = ((ACTIVE, _('Active')), (DISABLED, _('Disabled')), (DELETED, _('Deleted')))

    test_case = models.ForeignKey(TestCase, verbose_name=_("Test case"))
    question = models.TextField(verbose_name=_("Question"))
    test_type = models.CharField((_('Test type')), choices=TEST_TYPE, max_length=10)
    state = models.PositiveSmallIntegerField(_('State'), choices=STATE)

    def __unicode__(self):
        return self.question


class AnswerOption(models.Model):
    """ Answer options for standard questions
        usually there are four options with one correct
    """
    question = models.ForeignKey(Question, verbose_name=_("Question"))
    answer_option = models.TextField(verbose_name=_("Answer option"))
    is_correct = models.BooleanField(_("Is correct"))

    def __unicode__(self):
        return self.answer_option

    class Meta:
        ordering = ['answer_option']


class YesNoQuestion(models.Model):
    NO = 0
    YES = 1
    ANSWER_OPTIONS = ((NO, _('No')), (YES, _('Yes')))

    question = models.ForeignKey(Question, verbose_name=_('Question'))
    correct = models.PositiveSmallIntegerField(choices=ANSWER_OPTIONS)

    def __unicode__(self):
        return u"{} - {}".format(self.question.question, self.get_correct_display())

    class Meta:
        verbose_name = _('Yes no question')
        verbose_name_plural = _('Yes no questions')


class FitnessQuestion(models.Model):
    """ Answer options for fitness questions
        every question contains sub_question
        which have own answer
    """
    question = models.ForeignKey(Question, verbose_name=_('Question'))
    sub_question = models.TextField(_('Sub question'))
    answer = models.TextField(_('Answer'))

    def __unicode__(self):
        return '%s %s' % (self.sub_question, self.answer)


class TestVariantBase(models.Model):
    """ Base class for shuffled variants of tests """

    test_case = models.ForeignKey(TestCase, verbose_name=_('Test case'))
    creator = models.ForeignKey(User, verbose_name=_('Creator'))
    code = models.CharField(_("Code"), max_length=10, db_index=True)
    created_at = models.DateTimeField(_('Created at'), auto_now=True)

    class Meta:
        unique_together = ('test_case', 'code')

    def __unicode__(self):
        return self.code


class TestVariantShuffled(models.Model):
    """
    Shuffled tests

    Test shuffled positions are stored in answers_positions(HStoreField)
    For standard questions
    { 1682 = 4, 1681 = 2, c-pos = 2, 1683 = 1, 1684 = 3 }

    1682, 1681, 1683, 1684 are the answers pk and c-pos is correct answer position
    4, 2, 1, 3 are order of answers
    2 in c-pos means that correct answer for question is positioned in 2 nd place (1681 is correct answer)

    For fitness questions
    { 413_answer-pos = 5, 412_p = 2, 411_answer-pos = 1, 413_p = 3, 414_answer-pos = 4,
             414_p = 4, 415_answer-pos = 3, 411_p = 1, 415_p = 5, 412_answer-pos = 2 }

    413_answer-pos = 5 means that answer for question 413(pk of fitness question) is in position 5
    412_p = 2 means that question 412(pk of fitness question) is positioned in 2 nd place
    """

    test_variant_base = models.ForeignKey(TestVariantBase, verbose_name=_('Test variant'))
    question = models.ForeignKey(Question, verbose_name=_("Question"))
    question_position = models.PositiveSmallIntegerField(_("Question position"))
    answers_positions = HStoreField()


class StudentTestResult(models.Model):
    """ Test results of students """

    student = models.ForeignKey(Student, verbose_name=_('Student'))
    test_variant_base = models.ForeignKey(TestVariantBase, verbose_name=_('Test variant base'))

    # Student chosen answers for questions
    # q1 = 2 means that student selected 2 answer for question 1 (standard questions)
    # fq1 = 2 means that student selected 2 answer for question 1 (fitness questions)
    # { q1 = 2, q2 = 1, q3 = 2, q4=2, q5=1 ... q25 = 3, fq1 = 2, fq2 = 3, fq4 = 5, fq5 = 1 ... fq20 = 3  }
    answers = HStoreField()
    total_score = models.PositiveSmallIntegerField(_('Total score'))


class Attempt:
    FIRST = 0
    SECOND = 1
    THIRD = 2

    ATTEMPTS_COUNT = (
        (FIRST, _('First')),
        (SECOND, _('Second')),
        (THIRD, _('Third'))
    )


class GroupSubject(models.Model):
    NEW = 0
    ACTIVE = 1
    PROCESSING = 2
    FINISHED = 3
    DISABLED = 4
    DELETED = 5

    STATE = ((ACTIVE, _('Active')), (DISABLED, _('Disabled')), (DELETED, _('Deleted')),
             (FINISHED, _('Finished')), (NEW, _('New')), (PROCESSING, _('Processing')))

    test_case = models.ForeignKey(TestCase, verbose_name=_('Test case'))

    standard_question_count = models.IntegerField(_('Standard question count'))
    standard_question_score = models.FloatField(_('Score for standard question'))

    fitness_question_count = models.IntegerField(_('Fitness question count'))
    fitness_question_score = models.FloatField(_('Score for fitness question'))

    yes_no_score = models.FloatField(_('Score for Yes/No question'))
    yes_no_count = models.IntegerField(_('Yes/No question count'))

    multiselect_score = models.FloatField(_('Score for multiselect question'))
    multiselect_count = models.IntegerField(_('Multiselect question count'))

    current_attempt = models.IntegerField(choices=Attempt.ATTEMPTS_COUNT)
    time_limit = models.IntegerField(_('Time limit in minutes'))
    state = models.IntegerField(_('State'), choices=STATE)

    def __unicode__(self):
        return self.test_case.code

    class Meta:
        verbose_name = _('Group and subject')
        verbose_name_plural = _('Group and subjects')


class TestAttempts(models.Model):
    """Attempts for each test """
    PENDING = 0
    STARTED = 1
    FINISHED = 2
    DELETED = 3

    STATE = ((PENDING, _('Pending')), (STARTED, _('Active')),
             (DELETED, _('Deleted')), (FINISHED, _('Finished')))

    group_subject = models.ForeignKey(GroupSubject, verbose_name=_("Group and subject"))
    student = models.ForeignKey(Student, verbose_name=_("Student"))
    test_variant = models.IntegerField(default=1, null=True, blank=True)

    shuffled_questions = JSONField()
    answers = JSONField()

    attempt = models.IntegerField(_('Attempt'), choices=Attempt.ATTEMPTS_COUNT)
    total_score = models.IntegerField(_('Total score'))
    begin_time = models.DateTimeField()
    end_time = models.DateTimeField()
    state = models.IntegerField(_('State'), choices=STATE)

    def __unicode__(self):
        return u"{} - {} - {}".format(self.group_subject.markbook.learning_group,
                                      self.student, self.attempt)

    class Meta:
        unique_together = ('group_subject', 'attempt', 'student')
        verbose_name = "Test attempt"
        verbose_name_plural = "Test attempts"
