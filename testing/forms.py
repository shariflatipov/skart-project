# -*- coding: utf-8 -*-
from django import forms
from django.utils.translation import ugettext_lazy as _
import autocomplete_light.shortcuts as al
from core.models import Subject
from learning_process.models import LearningGroup
from people.models import Teacher

from .models import TestCase, Attempt

class UploadTestForm(forms.Form):
    # subject = al.ModelChoiceField('SubjectAutocomplete')
    # teacher = al.ModelChoiceField('TeacherAutocomplete')
    code = forms.CharField(max_length=10)
    file = forms.FileField()
    from_docx = forms.BooleanField(required=False)


class ShowTestListForm(forms.Form):
    subject = forms.ModelChoiceField(required=False,
                                     queryset=Subject.objects.all(),
                                     widget=al.ChoiceWidget('SubjectAutocomplete'))
    teacher = forms.ModelChoiceField(required=False,
                                     queryset=Teacher.objects.all(),
                                     widget=al.ChoiceWidget('TeacherAutocomplete'))
    code = forms.ModelChoiceField(required=False,
                                  queryset=TestCase.objects.all(),
                                  widget=al.ChoiceWidget('TestCaseAutocomplete'))
    group = forms.ModelChoiceField(required=False,
                                   queryset=LearningGroup.objects.all(),
                                   widget=al.ChoiceWidget('LearningGroupAutocomplete'))


class ShowResultsListForm(forms.Form):
    test_case = forms.ModelChoiceField(required=False,
                                       queryset=TestCase.objects.all(),
                                       widget=al.ChoiceWidget('TestCaseAutocomplete'))
    group = forms.ModelChoiceField(required=False,
                                   queryset=LearningGroup.objects.all(),
                                   widget=al.ChoiceWidget('LearningGroupAutocomplete'))


class GrantTestAccessForm(forms.Form):
    # subject = forms.ModelChoiceField(queryset=Subject.objects.all(),
    #                                    widget=al.ChoiceWidget('SubjectAutocomplete'))
    #
    group = forms.ModelChoiceField(queryset=LearningGroup.objects.all(),
                                   widget=al.ChoiceWidget('LearningGroupAutocomplete'))

    test_case = forms.ModelChoiceField(queryset=TestCase.objects.all(),
                                       widget=al.ChoiceWidget('TestCaseAutocomplete'))

    time_limit = forms.IntegerField(_('Time limit'))
    
    attempt = forms.ChoiceField(choices=Attempt.ATTEMPTS_COUNT)

    access_granted = forms.BooleanField(required=False)


class TestResultForm(forms.Form):
    group = forms.ModelChoiceField(queryset=LearningGroup.objects.all(),
                                   widget=al.ChoiceWidget('LearningGroupAutocomplete'),
                                   required=False)
    subject = forms.ModelChoiceField(queryset=Subject.objects.all(),
                                     widget=al.ChoiceWidget('SubjectAutocomplete'),
                                     required=False)


class UnzipTestImages(forms.Form):
    """Docstring for UnzipTestImages. """
    file = forms.FileField(required=False)


class UnauthorizedTestAccessForm(forms.Form):
    group = forms.ModelChoiceField(queryset=LearningGroup.objects.all(),
                                   widget=al.ChoiceWidget('LearningGroupAutocomplete'))
    subject = forms.ModelChoiceField(queryset=Subject.objects.all(),
                                           widget=al.ChoiceWidget('SubjectAutocomplete'),
                                           required=False)


class ChangeTestCaseForm(forms.Form):
    old_test_case = forms.ModelChoiceField(queryset=TestCase.objects.all(), 
                                           widget=al.ChoiceWidget('TestCaseForTeacherAutocomplete'))
    new_test_case = forms.ModelChoiceField(queryset=TestCase.objects.all(), 
                                           widget=al.ChoiceWidget('TestCaseForTeacherAutocomplete'))
