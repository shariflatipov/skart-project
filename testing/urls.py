# -*- coding: utf-8 -*-
from django.conf.urls import url
from testing import views


urlpatterns = [
    url(r'^upload/$', views.upload_test,
        name='upload_test'),
    url(r'^show/$', views.show_test_list, name='show_tests'),
    url(r'^download/(?P<test_id>\d+)$', views.download_test, name='download_test'),
    url(r'^test/show/(?P<test_id>\d+)$', views.show_test_internal, name='show_test'),
    url(r'^test/delete/(?P<test_id>\d+)$', views.delete_test, name='delete_test'),
    url(r'^test/variant/(?P<variant_id>\d+)$', views.show_variant, name='show_variant'),
    url(r'^results$', views.show_results, name='test_results'),
    url(r'^test/to_document_test_result/$', views.to_document_test_result, name='to_document_test_result'),
    url(r'^test/standard/append/(?P<test_case>\d+)$', views.append_standard_test, name='append_standard_test'),
    url(r'^test/fitness/append/(?P<test_case>\d+)$', views.append_fitness_test, name='append_fitness_test'),
    url(r'^test/pass/$', views.show_available_test_list, name='show_available_tests'),
    url(r'^test/standard/edit/(?P<question>\d+)$', views.edit_standard_test, name='edit_standard_test'),
    url(r'^test/fitness/edit/(?P<question>\d+)$', views.edit_fitness_test, name='edit_fitness_test'),
    url(r'^test/grant_access/$', views.grant_test_access, name='grant_test_access'),
    url(r'^test/pass/check/(?P<test>\d+)/$', views.pass_test_check, name='pass_test_check'),
    url(r'^test/pass/test/(?P<group_student>\d+)/(?P<question>\d+)/$',
        views.real_pass_test, name='real_pass_test'),
    url(r'^test/result/(?P<group_subject_student>\d+)/$', views.test_result, name='test_result'),

    url(r'^online/test/result/$', views.online_test_result, name='online_test_result'),
    url(r'^online/test/student/errors/$', views.to_document_student_error_doc, name='to_document_student_error_doc'),
    url(r'^online/test/result/download/(?P<group_subject>\d+)/$',
        views.download_test_result, name='download_test_result'),
    url(r'^online/test/student/errors/download/(?P<test_attempt>\d+)/$',
        views.download_student_errors, name='download_student_errors'),
    url(r'^online/test/upload/zip/$', views.unzip_test_images, name='upload_zip_test'),
    url(r'^test/unauthorized/show/$', views.show_unauthorized_tests, name='show_unauthorized_tests'),
    url(r'^test/unauthorized/show/internal/(?P<group_subject_id>\d+)$', views.choose_unauthorized_test,
        name='show_internal_unauthorized_tests'),
    url(r'^test/access/close/$', views.close_tests_access, name="close_tests_access"),
    url(r'^test/change/$', views.change_mark_book_test_case, name="change_test_case"),
]
