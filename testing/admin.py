from django.contrib import admin

from testing.models import (
    AnswerOption,
    FitnessQuestion,
    GroupSubject,
    Question,
    TestCase,
    TestVariantBase,
    TestVariantShuffled,
    TestAttempts,
    YesNoQuestion
)


class GroupSubjectAdmin(admin.ModelAdmin):
    list_display = ('test_case',)
    search_fields = ['test_case__subject__name', 'test_case__code']


class AnswerOptionAdmin(admin.ModelAdmin):
    raw_id_fields = ('question',)


class GroupSubjectStudentAdmin(admin.ModelAdmin):
    list_display = ('group_subject', 'student')
    search_fields = ['group_subject__test_case__code']
    raw_id_fields = ('student', 'group_subject',)


class TestAttemptAdmin(admin.ModelAdmin):
    list_filter = ('attempt',)
    search_fields = ['group_subject__test_case__code']
    raw_id_fields = ('group_subject', 'student',)


class FitnessQuestionAdmin(admin.ModelAdmin):
    raw_id_fields = ('question',)


class TestCaseAdmin(admin.ModelAdmin):
    search_fields = ['teacher__username', 'code']
    raw_id_fields = ('teacher', 'subject')


class YesNoQuestionAdmin(admin.ModelAdmin):
    raw_id_fields = ('question',)


admin.site.register(AnswerOption, AnswerOptionAdmin)
admin.site.register(FitnessQuestion, FitnessQuestionAdmin)
admin.site.register(GroupSubject, GroupSubjectAdmin)
admin.site.register(Question)
admin.site.register(TestCase, TestCaseAdmin)
admin.site.register(TestVariantBase)
admin.site.register(TestVariantShuffled)
admin.site.register(TestAttempts, TestAttemptAdmin)
admin.site.register(YesNoQuestion, YesNoQuestionAdmin)
