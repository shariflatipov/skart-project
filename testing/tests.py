from django.test import TestCase
from testing.models import TestCase as T
from people.models import Teacher, Position
from core.models import Subject
from learning_process.models import Faculty, Speciality


class TestingTest(TestCase):
    def setUp(self):
        speciality = Speciality()
        speciality.code = 'spec'
        speciality.name = 'spec'
        speciality.save()
    
        
        faculty = Faculty()
        faculty.name = 'faculty'
        faculty.speciality = speciality
        faculty.save()
        
        
        position = Position()
        position.code = 'director'
        position.name = 'director'
        position.save()
        
        
        user = Teacher()
        user.username = 'test'
        user.first_name = 'test'
        user.last_name = 'test'
        user.position = position
        user.faculty = faculty
        user.save()
        
        subject = Subject()
        subject.code = 'sub'
        subject.name = 'name'
        subject.save()
        
        t = T()
        t.teacher = user
        t.subject = subject
        t.code = 'test'
        t.state = 1
        t.save()
        
    def test_method(self): 
        t = T.objects.all()
        self.assertEqual(len(t), 1)
        self.assertEqual(t[0].teacher.username, 'test')
        
