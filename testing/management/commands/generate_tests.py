# -*- coding: utf-8 -*-
from educational_plan.models import MarkBook
from people.models import Teacher
from testing.models import TestCase
from core.models import Subject

from django.core.management.base import BaseCommand, CommandError

class Command(BaseCommand):
    help = u'Для каждого учителя создает Тесты'

    def handle(self, *args, **options):
        try:
            teachers = Teacher.objects.all()
            for teacher in teachers:
                group_subject = MarkBook.objects.filter(teacher=teacher).values('subject').distinct()
                for g_s in group_subject:
                    subject = Subject.objects.get(pk=g_s['subject'])
                    
                    test_case = TestCase()
                    test_case.code = "{}_{}".format(subject.code, str(teacher.username).zfill(4))
                    test_case.subject = subject
                    test_case.teacher = teacher
                    test_case.state = TestCase.NEW
                    test_case.save()
        except Exception, e:
            raise CommandError('Exception %s' % e)
