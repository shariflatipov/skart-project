# -*- coding: utf-8 -*-
import autocomplete_light.shortcuts as al
from django.utils.translation import ugettext_lazy as _
from testing.models import TestCase

al.register(TestCase,
            search_fields=['code'],
            choices=TestCase.objects.filter(state=TestCase.NEW),
            attrs={
                'placeholder': _('Test code'),
                'data-autocomplete-minimum-characters': 0,
            },
            widget_attrs={
                'data-widget-maximum-values': 4,
                'class': 'modern-style',
            },
            )

class ForTeacherAutocomplete(al.AutocompleteModelBase):
    def choices_for_request(self):
        user = self.request.user
        q = self.request.GET.get('q', '')

        choices = self.choices.filter(state=TestCase.NEW)
            
        if user.is_superuser:
            if q:
                choices = choices.filter(code__icontains=q)
        else:
            if q:
                choices = choices.filter(code__icontains=q, teacher=user)
            choices = choices.filter(teacher=user)

        return self.order_choices(choices)[0:self.limit_choices]


al.register(TestCase, ForTeacherAutocomplete, 
            attrs={
                'placeholder': _('Test code'),
                'data-autocomplete-minimum-characters': 0,
            },
            widget_attrs={
                'data-widget-maximum-values': 4,
                'class': 'modern-style',
            },)
