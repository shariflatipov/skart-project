# -*- coding: utf-8 -*-
# Generated by Django 1.9a1 on 2015-10-21 15:44
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('learning_process', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('people', '0001_initial'),
        ('educational_plan', '0002_auto_20151021_2044'),
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='markbook',
            name='teacher',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='people.Teacher', verbose_name='Teacher'),
        ),
        migrations.AddField(
            model_name='markbook',
            name='week',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Week', verbose_name='Week'),
        ),
        migrations.AddField(
            model_name='educationalplansemester',
            name='educational_plan',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='educational_plan.EducationalPlan', verbose_name='Educational plan'),
        ),
        migrations.AddField(
            model_name='educationalplansemester',
            name='semester',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Semester', verbose_name='Semester'),
        ),
        migrations.AddField(
            model_name='educationalplanmain',
            name='created_by',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='User'),
        ),
        migrations.AddField(
            model_name='educationalplanmain',
            name='speciality',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='learning_process.Speciality', verbose_name='Speciality'),
        ),
        migrations.AddField(
            model_name='educationalplanlessontype',
            name='educational_plan',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='educational_plan.EducationalPlan', verbose_name='Educational plan'),
        ),
        migrations.AddField(
            model_name='educationalplanlessontype',
            name='lesson_type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.LessonType', verbose_name='Lesson type'),
        ),
        migrations.AddField(
            model_name='educationalplancontrols',
            name='control_type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.ControlType', verbose_name='Control type'),
        ),
        migrations.AddField(
            model_name='educationalplancontrols',
            name='educational_plan',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='educational_plan.EducationalPlan', verbose_name='Educational plan'),
        ),
        migrations.AddField(
            model_name='educationalplancontrols',
            name='semester',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Semester', verbose_name='Semester'),
        ),
        migrations.AddField(
            model_name='educationalplan',
            name='control',
            field=models.ManyToManyField(through='educational_plan.EducationalPlanControls', to='core.ControlType'),
        ),
        migrations.AddField(
            model_name='educationalplan',
            name='educational_plan_main',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='educational_plan.EducationalPlanMain', verbose_name='Educational plan'),
        ),
        migrations.AddField(
            model_name='educationalplan',
            name='lesson_type',
            field=models.ManyToManyField(through='educational_plan.EducationalPlanLessonType', to='core.LessonType'),
        ),
        migrations.AddField(
            model_name='educationalplan',
            name='semester',
            field=models.ManyToManyField(through='educational_plan.EducationalPlanSemester', to='core.Semester'),
        ),
        migrations.AddField(
            model_name='educationalplan',
            name='subject',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Subject', verbose_name='Subject'),
        ),
        migrations.AlterUniqueTogether(
            name='educationalplan',
            unique_together=set([('subject', 'educational_plan_main')]),
        ),
    ]
