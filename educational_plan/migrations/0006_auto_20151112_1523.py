# -*- coding: utf-8 -*-
# Generated by Django 1.9a1 on 2015-11-12 10:23
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('testing', '0008_testcase_description'),
        ('core', '0004_auto_20151027_1412'),
        ('educational_plan', '0005_auto_20151108_1042'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='markbook',
            name='week',
        ),
        migrations.AddField(
            model_name='markbook',
            name='semester',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='core.Semester', verbose_name='Semester'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='markbook',
            name='test_case',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='testing.TestCase', verbose_name='Test case'),
            preserve_default=False,
        ),
    ]
