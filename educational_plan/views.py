# -*- coding: utf-8 -*-
from openpyxl import load_workbook
from datetime import datetime

from django.db import transaction
from django.contrib import messages
from django.contrib.auth.decorators import permission_required, login_required
from django.core.urlresolvers import reverse
from django.core.exceptions import ObjectDoesNotExist
from django.utils.translation import ugettext_lazy as _
from django.shortcuts import render, get_object_or_404, redirect
from django.forms.formsets import formset_factory
from django.forms import modelformset_factory
from core.models import ControlType, Semester, DefaultSettings, YearSemester, \
    SemesterWeek, Setting, Week, EducationalYear
from core.enums import ACTIVE, FINISHED
from people.models import Teacher, Student, User, Position
from learning_process.models import LearningGroup
from testing.models import TestCase, GroupSubject, Attempt
from refs.models import Nationality

from .forms import SpecialityForm, EduPlanFrom, SetGroupEducationalPlan, ModelFormsetFactoryFormset, \
    UploadMarkbookForm
from .models import EducationalPlanControls, EducationalPlan, EducationalPlanLessonType, \
    EducationalPlanSemester, MarkBook, EducationalPlanMain, LessonType, MarkBookControl
from .utils import get_markbook


@login_required
def learning_group_list(request):
    educational_plan = None
    if request.method == 'POST':
        form = SpecialityForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            educational_plan = EducationalPlanMain.objects.filter(speciality=cd['speciality'])
    else:
        form = SpecialityForm()

    return render(request, 'educational_plan/list.html', {'elements': educational_plan, "form": form})


@login_required
@permission_required('educational_plan.add_educationalplan')
@transaction.atomic
def educational_plan(request):
    form = formset_factory(EduPlanFrom)

    if request.method == "POST":
        formset = form(request.POST)
        speciality_form = SpecialityForm(request.POST)

        if formset.is_valid() and speciality_form.is_valid():
            """ Algorithm
            1. Create educational plan for speciality
            2. For each group in speciality create MarkBook
            """

            speciality_cd = speciality_form.cleaned_data

            epm = EducationalPlanMain()
            epm.created_by = request.user
            epm.speciality = speciality_cd['speciality']
            epm.save()
            for form in formset:
                cd = form.cleaned_data

                ep = EducationalPlan()
                ep.educational_plan_main = epm
                ep.subject = cd['subject']
                ep.total_credits = cd['total_credit']
                ep.save()

                if cd['kmro']:
                    eplt = EducationalPlanLessonType()
                    eplt.educational_plan = ep
                    eplt.lesson_type = LessonType.objects.get(code='SRSP')
                    eplt.lesson_credits = cd['kmro']
                    eplt.save()
                if cd['kmd']:
                    eplt = EducationalPlanLessonType()
                    eplt.educational_plan = ep
                    eplt.lesson_type = LessonType.objects.get(code='SRS')
                    eplt.lesson_credits = cd['kmro']
                    eplt.save()

                if cd['test']:
                    epc = EducationalPlanControls()
                    epc.control_type = ControlType.objects.get(code="test")
                    epc.educational_plan = ep
                    epc.semester = Semester.objects.get(code=cd['test'])
                    epc.save()

                if cd['exam']:
                    epc = EducationalPlanControls()
                    epc.control_type = ControlType.objects.get(code="exam")
                    epc.educational_plan = ep
                    epc.semester = Semester.objects.get(code=cd['exam'])
                    epc.save()

                for i in range(1, 9):
                    if cd["semester_{}".format(str(i))]:
                        eps = EducationalPlanSemester()
                        eps.semester_credits = cd["semester_{}".format(str(i))]
                        eps.educational_plan = ep
                        eps.semester = Semester.objects.get(code=str(i))
                        eps.save()

            messages.info(request, _("All data saved correctly"))
            return redirect(reverse('educational_plan'))
    else:
        formset = form()
        speciality_form = SpecialityForm()

    return render(request, 'educational_plan/index.html', {"form": formset,
                                                           "speciality_form": speciality_form})


@login_required
@permission_required('educational_plan.add_educationalplan')
def select_educational_plan_for_group(request):
    select_form = SetGroupEducationalPlan(request.POST or None)
    if select_form.is_valid():
        cd = select_form.cleaned_data
        group = cd['learning_group']
        semester = cd['semester']
        try:
            educational_plan_main = EducationalPlanMain.objects.filter(speciality=group.speciality)[0].pk
        except:
            messages.info(request, _("There is no educational plan for this group and semester"))
            return render(request, 'educational_plan/choose_learning_group_semester.html', {'form': select_form})
        return redirect(reverse(assign_to_teacher, args=[educational_plan_main, group.pk, semester.pk]))
    else:
        return render(request, 'educational_plan/choose_learning_group_semester.html', {'form': select_form})


@login_required
@permission_required('educational_plan.add_educationalplan')
@transaction.atomic
def assign_to_teacher(request, edu_id, group, selected_semester):
    """Assign educational plan subject to teacher
    """
    teacher = get_object_or_404(Teacher, user_ptr=request.user)
    learning_group = LearningGroup.objects.get(pk=int(group))
    semester = Semester.objects.get(pk=selected_semester)

    educational_plan = EducationalPlan.objects.filter(
        educational_plan_main__speciality__faculty=teacher.faculty,
        educational_plan_main__speciality=learning_group.speciality,
        semester=semester)
    EducationalPlanFormset = modelformset_factory(EducationalPlan,
                                                  formset=ModelFormsetFactoryFormset,
                                                  exclude=('educational_plan_main',
                                                           'control', 'semester',
                                                           'lesson_type',
                                                           'total_credits',
                                                           'subject'), extra=0)

    if request.method == 'POST':
        form = EducationalPlanFormset(request.POST, queryset=educational_plan)
    else:
        form = EducationalPlanFormset(queryset=educational_plan)

    if form.is_valid():
        for f in form:
            cd = f.cleaned_data

            learning_group = LearningGroup.objects.get(pk=int(group))

            # Generate TestCase
            test_case = TestCase()
            test_case.subject = cd['id'].subject
            test_case.teacher = teacher
            test_case.description = learning_group
            test_case.code = get_random_test_case_code(teacher)
            test_case.state = TestCase.NEW
            test_case.save()

            group_subject = GroupSubject()
            group_subject.learning_group = learning_group
            group_subject.test_case = test_case
            group_subject.time_limit = 40
            group_subject.current_attempt = Attempt.FIRST
            group_subject.state = GroupSubject.NEW
            group_subject.standard_question_count = Setting.objects.get(
                variable='STANDARD_TEST_COUNT').value
            group_subject.fitness_question_count = Setting.objects.get(
                variable='FITNESS_TEST_COUNT').value
            group_subject.standard_question_score = Setting.objects.get(
                variable='STANDARD_TEST_SCORE').value
            group_subject.fitness_question_score = Setting.objects.get(
                variable='FITNESS_TEST_SCORE').value
            group_subject.save()

            mark_book = MarkBook()
            teacher = Teacher.objects.get(pk=int(cd['teacher']))

            mark_book.teacher = teacher
            mark_book.created_by = request.user
            mark_book.learning_group = learning_group
            mark_book.group_subject = group_subject

            default_setting = DefaultSettings.objects.all()[0]
            current_year = default_setting.current_year_semester

            mark_book.semester_year = YearSemester.objects.get(year=current_year.year,
                                                               semester=semester)
            mark_book.active_week = 1
            mark_book.week_count = 16

            mark_book.marks = {}
            mark_book.subject = cd['id'].subject
            mark_book.state = ACTIVE

            mark_book.test_case = test_case
            mark_book.save()

            # Generate Controls form every markbook
            # First control point
            control = MarkBookControl()
            control.mark_book = mark_book
            week = Week.objects.get(code='w7')
            code = u's{}{}'.format(semester.order, week.code)
            control.semester_week, created = SemesterWeek.objects.get_or_create(code=code, semester=semester, week=week)
            control.control_type = ControlType.objects.get(code='CP1')
            control.save()

            # Second control point
            control = MarkBookControl()
            control.mark_book = mark_book
            week = Week.objects.get(code='w13')
            code = u's{}{}'.format(semester.order, week.code)
            control.semester_week, created = SemesterWeek.objects.get_or_create(code=code, semester=semester, week=week)
            control.control_type = ControlType.objects.get(code='CP2')
            control.save()

            # Final Exams
            control = MarkBookControl()
            control.mark_book = mark_book
            week = Week.objects.get(code='w16')
            code = u's{}{}'.format(semester.order, week.code)
            control.semester_week, created = SemesterWeek.objects.get_or_create(code=code, semester=semester, week=week)
            control.control_type = ControlType.objects.get(code='exam')
            control.save()
        messages.info(request, _("All data saved correctly"))
        return redirect(reverse('select_group_semester'))
    else:
        messages.error(request, _('Please correct errors'))
    return render(request, 'educational_plan/assign_to_teacher.html', {'data': form})


def get_random_test_case_code(teacher):
    import uuid
    return u"{}_{}".format(uuid.uuid4().hex[:5].upper(), teacher.pk)


@login_required
@permission_required('educational_plan.add_markbook')
def show_all_markbooks(request):
    markbooks = MarkBook.objects.all().order_by('learning_group')
    return render(request, 'educational_plan/all_markbooks.html', {'markbooks': markbooks})


@login_required
@permission_required('educational_plan.add_markbook')
def download_markbook(request, pk):
    mark_book = get_object_or_404(MarkBook, pk=pk)
    return get_markbook(mark_book)


@login_required
@permission_required('educational_plan.add_markbook')
@transaction.atomic
def upload_markbook(request):
    """ Upload markbook from XLS file
    """
    import hashlib
    from core.models import Subject

    if request.method == 'POST':
        form = UploadMarkbookForm(request.POST, request.FILES)
        if form.is_valid():
            cd = form.cleaned_data
            file = cd['file']
            learning_group = cd['learning_group']
            spreadsheet = load_workbook(filename=file, read_only=True)
            sheet = spreadsheet[u'Лист1']

            edu_year = None
            teacher = None
            subject = None
            semester = None
            mark_dict = dict()

            student_username_counter = Setting.objects.get(variable='ST_USERNAME_COUNT')
            counter_value = int(student_username_counter.value)

            i = 0
            for row in sheet.rows:
                i += 1
                if i < 2:
                    continue
                if i == 2:
                    sp_edu_year = row[1].value.split("-")
                    edu_year, created = EducationalYear.objects.get_or_create(start_year=sp_edu_year[0], end_year=sp_edu_year[1])
                elif i == 3:
                    semester = Semester.objects.get(code=row[1].value)
                elif i == 4:
                    pass
                elif i == 5:
                    pass
                elif i == 6:
                    pass
                elif i == 7:
                    pass
                elif i == 8:
                    fio = row[1].value.split(' ')
                    create_new_teacher = False
                    if len(fio) == 3:
                        try:
                            teacher = Teacher.objects.get(first_name=fio[1], middle_name=fio[2], last_name=fio[0])
                        except ObjectDoesNotExist:
                            teacher = Teacher()
                            teacher .first_name = fio[1]
                            teacher .middle_name = fio[2]
                            teacher .last_name = fio[0]
                            create_new_teacher = True
                    else:
                        try:
                            teacher = Teacher.objects.get(first_name=fio[1], last_name=fio[0])
                        except ObjectDoesNotExist:
                            teacher = Teacher()
                            teacher.first_name = fio[1]
                            teacher.last_name = fio[0]
                            create_new_teacher = True

                    if create_new_teacher:
                        counter_value += 1

                        uname_pass = "tch" + str(counter_value).zfill(8)

                        teacher.position = Position.objects.get(code='Teacher')
                        teacher.username = uname_pass
                        teacher.set_password(uname_pass)
                        teacher.save()

                elif i == 9:
                    h = hashlib.md5()
                    h.update(row[1].value.encode('utf8'))
                    digest = h.hexdigest()

                    try:
                        subject = Subject.objects.get(code=digest)
                    except ObjectDoesNotExist:
                        subject = Subject(code=digest, name=row[1].value)
                        subject.save()

                elif i == 10:
                    pass
                elif i == 11:
                    # Create markbook object
                    markbook = MarkBook()
                    markbook.subject = subject
                    markbook.teacher = teacher
                    markbook.learning_group = learning_group
                    semester_year, created = YearSemester.objects.get_or_create(year=edu_year, semester=semester)
                    markbook.semester_year = semester_year
                    markbook.week_count = 16
                    markbook.active_week = 16
                    markbook.state = FINISHED
                    markbook.created_by = User.objects.get(username='admin')
                else:
                    if row[0].value is None:
                        break

                    # Create or get student
                    fio = row[0].value.split(' ')
                    create_new_user = False
                    student = None
                    if len(fio) == 3:
                        try:
                            student = Student.objects.get(first_name=fio[1], middle_name=fio[2], last_name=fio[0])
                        except ObjectDoesNotExist:
                            student = Student()
                            student.first_name = fio[1]
                            student.middle_name = fio[2]
                            student.last_name = fio[0]
                            create_new_user = True
                    else:
                        try:
                            student = Student.objects.get(first_name=fio[1], last_name=fio[0])
                        except ObjectDoesNotExist:
                            student = Student()
                            student.first_name = fio[1]
                            student.last_name = fio[0]
                            create_new_user = True

                    if create_new_user:
                        counter_value += 1

                        uname_pass = "st" + str(counter_value).zfill(8)

                        student.learning_group = learning_group
                        student.username = uname_pass
                        student.set_password(uname_pass)
                        student.save()

                    # set marks
                    mark_dict[student.pk] = {
                        "w1": {"mark": row[1].value, "absent": 0},
                        "w2": {"mark": row[2].value, "absent": 0},
                        "w3": {"mark": row[3].value, "absent": 0},
                        "w4": {"mark": row[4].value, "absent": 0},
                        "w5": {"mark": row[5].value, "absent": 0},
                        "w6": {"mark": row[6].value, "absent": 0},
                        "UW1": 0,
                        "CP1": row[8].value,
                        "R1": round(float(row[9].value) / 4, 2),

                        "w7": {"mark": row[10].value, "absent": 0},
                        "w8": {"mark": row[11].value, "absent": 0},
                        "w9": {"mark": row[12].value, "absent": 0},
                        "w10": {"mark": row[13].value, "absent": 0},
                        "w11": {"mark": row[14].value, "absent": 0},
                        "w12": {"mark": row[15].value, "absent": 0},
                        "UW1": 0,
                        "CP1": row[17].value,
                        "R1": round(float(row[18].value) / 4, 2),

                        "w13": {"mark": row[19].value, "absent": 0},
                        "w14": {"mark": row[20].value, "absent": 0},
                        "w15": {"mark": row[21].value, "absent": 0},
                        "w16": {"mark": row[22].value, "absent": 0},
                        "FE": row[24].value,
                        "R3": round(float(row[25].value) / 2, 2)
                    }

            student_username_counter.value = counter_value
            student_username_counter.save()

            markbook.marks = mark_dict
            markbook.save()

    else:
        form = UploadMarkbookForm()
    return render(request, 'educational_plan/upload_markbook.html', {'form': form})


@login_required
@permission_required('educational_plan.add_markbook')
@transaction.atomic
def upload_new_students(request):
    """ Upload students from XLS file
    """
    if request.method == 'POST':
        form = UploadMarkbookForm(request.POST, request.FILES)
        if form.is_valid():
            cd = form.cleaned_data
            file = cd['file']
            learning_group = cd['learning_group']
            spreadsheet = load_workbook(filename=file, read_only=True)
            sheet = spreadsheet[u'Лист1']

            edu_year = None

            student_username_counter = Setting.objects.get(variable='ST_USERNAME_COUNT')
            counter_value = int(student_username_counter.value)

            i = 0
            for row in sheet.rows:
                i += 1
                if i < 2:
                    continue
                if i == 2:
                    sp_edu_year = row[1].value.split("-")
                    edu_year, created = EducationalYear.objects.get_or_create(start_year=sp_edu_year[0], end_year=sp_edu_year[1])
                elif i == 3:
                    pass
                elif i == 4:
                    pass
                elif i == 5:
                    pass
                elif i == 6:
                    pass
                elif i == 7:
                    pass
                elif i == 8:
                    pass
                else:
                    if row[0].value is None:
                        break
                    # Create or get student
                    last_name = row[0].value
                    first_name = row[1].value
                    middle_name = row[2].value

                    create_new_user = False
                    student = None
                    if middle_name:
                        try:
                            student = Student.objects.get(first_name=first_name, middle_name=middle_name, last_name=last_name)
                        except ObjectDoesNotExist:
                            student = Student()
                            student.first_name = first_name
                            student.middle_name = middle_name
                            student.last_name = last_name

                            student.gender = User.MALE if row[3].value == u'Муж' else User.FEMALE
                            student.birth_date = datetime.strptime(row[4].value, '%d.%m.%Y')
                            student.phone = row[5].value
                            student.nationality = Nationality.objects.get(name=row[6].value)
                            student.passport_series = row[7].value
                            student.passport_number = row[8].value

                            create_new_user = True
                    else:
                        try:
                            student = Student.objects.get(first_name=first_name, last_name=last_name)
                        except ObjectDoesNotExist:
                            student = Student()
                            student.first_name = first_name
                            student.last_name = last_name

                            student.gender = User.MALE if row[3].value == u'Муж' else User.FEMALE
                            student.birth_date = datetime.strptime(row[4].value, '%d.%m.%Y')
                            student.phone = row[5].value
                            student.nationality = Nationality.objects.get(name=row[6].value)
                            student.passport_series = row[7].value
                            student.passport_number = row[8].value

                            create_new_user = True

                    if create_new_user:
                        counter_value += 1

                        uname_pass = "st" + str(counter_value).zfill(8)

                        student.learning_group = learning_group
                        student.username = uname_pass
                        student.set_password(uname_pass)
                        student.save()

            student_username_counter.value = counter_value
            student_username_counter.save()
    else:
        form = UploadMarkbookForm()
    return render(request, 'educational_plan/upload_markbook.html', {'form': form})
