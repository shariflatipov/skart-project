# -*- coding: utf-8 -*-

from django.http import HttpResponse

from openpyxl import Workbook
from openpyxl.writer.excel import save_virtual_workbook

from people.models import Student


def get_markbook(markbook):
    workbook = Workbook()

    worksheet = workbook.active
    i = 1

    for k, v in markbook.marks.items():
        student = Student.objects.get(pk=int(k))
        for t, m in v.items():
            if t == 'w1':
                worksheet['A' + str(i)] = m['mark']
            if t == 'w2':
                worksheet['B' + str(i)] = m['mark']
            if t == 'w3':
                worksheet['C' + str(i)] = m['mark']
            if t == 'w4':
                worksheet['D' + str(i)] = m['mark']
            if t == 'w5':
                worksheet['E' + str(i)] = m['mark']
            if t == 'w6':
                worksheet['F' + str(i)] = m['mark']
            if t == 'UW1':
                worksheet['G' + str(i)] = m
            if t == 'CP1':
                worksheet['H' + str(i)] = m
            if t == 'R1':
                worksheet['I' + str(i)] = m
            if t == 'w7':
                worksheet['J' + str(i)] = m['mark']
            if t == 'w8':
                worksheet['K' + str(i)] = m['mark']
            if t == 'w9':
                worksheet['L' + str(i)] = m['mark']
            if t == 'w10':
                worksheet['M' + str(i)] = m['mark']
            if t == 'w11':
                worksheet['N' + str(i)] = m['mark']
            if t == 'w12':
                worksheet['O' + str(i)] = m['mark']
            if t == 'UW2':
                worksheet['P' + str(i)] = m
            if t == 'CP2':
                worksheet['Q' + str(i)] = m
            if t == 'R2':
                worksheet['R' + str(i)] = m
            if t == 'w13':
                worksheet['S' + str(i)] = m['mark']
            if t == 'w14':
                worksheet['T' + str(i)] = m['mark']
            if t == 'w15':
                worksheet['U' + str(i)] = m['mark']
            if t == 'w16':
                worksheet['V' + str(i)] = m['mark']
            if t == 'R3':
                worksheet['W' + str(i)] = m
            if t == 'FE':
                worksheet['X' + str(i)] = m
            if t == 'gpa':
                worksheet['Y' + str(i)] = m

        i += 1

    response = HttpResponse(content=save_virtual_workbook(workbook), content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename=file.xlsx'
    return response


