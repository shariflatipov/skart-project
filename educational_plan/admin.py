from django.contrib import admin

# Register your models here.
from educational_plan.models import EducationalPlan, EducationalPlanSemester, EducationalPlanControls, \
    EducationalPlanLessonType, EducationalPlanMain, MarkBook, MarkBookControl


class MarkBookAdmin(admin.ModelAdmin):
    search_fields = ['learning_group__speciality__code', 'group_subject__test_case__code']
    list_filter = ('state', 'semester_year__semester', 'learning_group')
    raw_id_fields = ('subject', 'teacher', 'learning_group', 'created_by')


class EducationalPlanMainAdmin(admin.ModelAdmin):
    search_fields = ['speciality__code']

admin.site.register(EducationalPlan)
admin.site.register(EducationalPlanSemester)
admin.site.register(EducationalPlanLessonType)
admin.site.register(EducationalPlanControls)
admin.site.register(EducationalPlanMain, EducationalPlanMainAdmin)
admin.site.register(MarkBook, MarkBookAdmin)
admin.site.register(MarkBookControl)
