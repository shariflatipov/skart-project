# -*- coding: utf-8 -*-
from django.contrib.postgres.fields.jsonb import JSONField
from django.db import models, transaction
from django.utils.translation import ugettext_lazy as _
from core.models import Semester, Subject, LessonType, ControlType, YearSemester,\
    SemesterWeek
from core.enums import STATE
from learning_process.models import LearningGroup, Speciality
from people.models import Teacher, User
from testing.models import GroupSubject


class EducationalPlanMain(models.Model):
    speciality = models.ForeignKey(Speciality, verbose_name=_("Speciality"))
    created_by = models.ForeignKey(User, verbose_name=_("User"))
    created_date = models.DateField(verbose_name=_("Created date"),
                                    auto_now_add=True)
    updated_date = models.DateField(verbose_name=_("Updated date"),
                                    auto_now=True)

    def __unicode__(self):
        return u"{} - {}".format(self.speciality.code, self.speciality.name)

    class Meta:
        verbose_name = _("Educational plan")
        verbose_name_plural = _("Educational plans")


class EducationalPlan(models.Model):
    """Учебный план"""
    educational_plan_main = models.ForeignKey(EducationalPlanMain,
                                              verbose_name=_("Educational plan"))
    subject = models.ForeignKey(Subject, verbose_name=_("Subject"))
    total_credits = models.SmallIntegerField(_("Total credits"))
    semester = models.ManyToManyField(Semester, through="EducationalPlanSemester")
    lesson_type = models.ManyToManyField(LessonType, through="EducationalPlanLessonType")
    control = models.ManyToManyField(ControlType, through="EducationalPlanControls")

    def __unicode__(self):
        return unicode(self.subject.name)

    class Meta:
        unique_together = ('subject', 'educational_plan_main')
        verbose_name_plural = _("Educational plan subjects")
        verbose_name = _("Educational plans subjects")


class EducationalPlanSemester(models.Model):
    """
    Сколько кредитов прикреплено к предмету в данном семестре
    """
    educational_plan = models.ForeignKey(EducationalPlan,
                                         verbose_name=_("Educational plan"))
    semester = models.ForeignKey(Semester, verbose_name=_("Semester"))
    semester_credits = models.IntegerField(_("Credits"))

    def __unicode__(self):
        return u'{}, {} credits'.format(self.semester.name, self.semester_credits)


class EducationalPlanLessonType(models.Model):
    """
    Сколько кредитов для каждого типа уроков
    """
    educational_plan = models.ForeignKey(EducationalPlan,
                                         verbose_name=_("Educational plan"))
    lesson_type = models.ForeignKey(LessonType, verbose_name=_("Lesson type"))
    lesson_credits = models.IntegerField(_("Credits"))

    def __unicode__(self):
        return self.lesson_type.name


class EducationalPlanControls(models.Model):
    """
    Какой вид контроля на какой семестр приходиться
    """
    educational_plan = models.ForeignKey(EducationalPlan, verbose_name=_("Educational plan"))
    control_type = models.ForeignKey(ControlType, verbose_name=_("Control type"))
    semester = models.ForeignKey(Semester, verbose_name=_("Semester"))

    def __unicode__(self):
        return self.control_type.name


class MarkBook(models.Model):
    """Журнал оценок"""
    R1 = 'R1'
    R2 = 'R2'
    R3 = 'R3'
    R_ALL = 'R_ALL'

    subject = models.ForeignKey(Subject, verbose_name=_("Subject"))
    teacher = models.ForeignKey(Teacher, verbose_name=_("Teacher"))
    learning_group = models.ForeignKey(LearningGroup, verbose_name=_("Learning group"))
    semester_year = models.ForeignKey(YearSemester, verbose_name=_("Year semester"))
    week_count = models.PositiveSmallIntegerField()
    active_week = models.PositiveSmallIntegerField()

    # final test
    group_subject = models.OneToOneField(GroupSubject, verbose_name=_("Group and subject"), null=True, blank=True)

    marks = JSONField(_("Marks"))
    state = models.IntegerField(_("State"), choices=STATE)
    created_by = models.ForeignKey(User, verbose_name=_("Created by"), related_name="creator")
    created_date = models.DateField(auto_now_add=True)

    @staticmethod
    def calculate_rating(marks, rating):
        """
        Calculate rating
        :param marks marks for student
        :param rating which rating to calculate possible values are r1, r2 or r3
        """

        # w = Week
        # CP = Check point
        # UW = Unsupervised work
        # FW = Final work
        # FE = Final exam
        # STIMULATE = Stimulation mark
        # PENALTY = Penalty mark

        if rating == MarkBook.R1:
            r = ['w1', 'w2', 'w3', 'w4', 'w5', 'w6']
            v = ['CP1', 'UW1']
            kf = 4
        elif rating == MarkBook.R2:
            r = ['w7', 'w8', 'w9', 'w10', 'w11', 'w12']
            v = ['CP2', 'UW2']
            kf = 4
        else:
            r = ['w13', 'w14', 'w15', 'w16']

            v = ['FW', 'FE']
            kf = 2

        result = 0

        for k in r:
            if k in marks:
                result += float(marks[k]['mark'])

        for k in v:
            if k in marks:
                result += float(marks[k])

        if rating == MarkBook.R3:
            if 'PENALTY' in marks:
                result -= float(marks['PENALTY'])
            if 'STIMULATE' in marks:
                result += float(marks['STIMULATE'])

        return result / kf

    @transaction.atomic
    def set_rating(self, student_id, rating=R_ALL, force=False):
        """Calculate or recalculate rating of the student"""

        if type(self.marks[student_id]) is dict:
            if rating == self.R_ALL:
                if force or 'R1' not in self.marks[student_id]:
                    self.marks[student_id]['R1'] = MarkBook.calculate_rating(
                        self.marks[student_id], MarkBook.R1)
                if force or 'R2' not in self.marks[student_id]:
                    self.marks[student_id]['R2'] = MarkBook.calculate_rating(
                        self.marks[student_id], MarkBook.R2)
                if force or 'R3' not in self.marks[student_id]:
                    self.marks[student_id]['R3'] = MarkBook.calculate_rating(
                        self.marks[student_id], MarkBook.R3)
            else:
                if force or rating not in self.marks[student_id]:
                    self.marks[student_id][rating] = MarkBook.calculate_rating(
                        self.marks[student_id], rating)

        self.save()

    @classmethod
    def get_for_teacher(cls, user, mark_book_id, semester_year=None):
        """ Get mark book object for teacher
        :user: user should be request.user and should be teacher
        :mark_book_id: MarkBook primary key
        :semester_year: SemesterYear model
        :returns:
        """
        try:
            if hasattr(user, 'teacher'):
                teacher = Teacher.objects.get(user_ptr=user)
                if semester_year is None:
                    return cls.objects.get(teacher=teacher, pk=mark_book_id)
                else:
                    return cls.objects.get(teacher=teacher, pk=mark_book_id, semester_year=semester_year)
            else:
                return None
        except:
            return None

    def __unicode__(self):
        return u"{}: {}".format(unicode(self.learning_group.get_full_name()), unicode(self.subject.name))

    class Meta:
        unique_together = ('subject', 'teacher', 'learning_group', 'semester_year')
        verbose_name = _("Mark book")
        verbose_name_plural = _("Mark books")


class MarkBookControl(models.Model):
    mark_book = models.ForeignKey(MarkBook, verbose_name=_("Mark book"))
    semester_week = models.ForeignKey(SemesterWeek, verbose_name=_("Semester and week"))
    control_type = models.ForeignKey(ControlType, verbose_name=("Control type"))

    def __unicode__(self):
        return u"{} - {}".format(self.mark_book.subject.name, self.semester_week)
