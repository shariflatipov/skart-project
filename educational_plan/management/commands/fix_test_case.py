# -*- coding: utf-8 -*-
from educational_plan.models import MarkBook
from django.core.management.base import BaseCommand, CommandError
from django.db import transaction

from testing.models import TestCase, GroupSubject


class Command(BaseCommand):
    help = u'Fix test case for Mark books'

    def add_arguments(self, parser):
        parser.add_argument('--current_year_half', type=str)

    def handle(self, *args, **options):
        count = 0
        failed_to_insert = 0
        try:
            for mark_book in MarkBook.objects.all():
                try:
                    count += 1
                    mark_book.group_subject = GroupSubject.objects.get(
                        learning_group=mark_book.learning_group,
                        test_case__subject=mark_book.subject,
                        test_case__teacher=mark_book.teacher
                    )
                    mark_book.save()
                except Exception, e:
                    failed_to_insert += 1
        except Exception, e:
            pass

        print "Inserted data {}, Failed to insert {}".format(
            count, failed_to_insert
        )
