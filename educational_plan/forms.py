# -*- coding: utf-8 -*-
try:
    from collections import UserList
except ImportError:  # Python 2
    from UserList import UserList

import autocomplete_light.shortcuts as al
from django import forms
from django.forms import BaseModelFormSet
from django.forms.formsets import BaseFormSet
from django.utils.html import html_safe, format_html
from django.utils.encoding import python_2_unicode_compatible
from .models import EducationalPlan


@html_safe
@python_2_unicode_compatible
class MyErrorList(UserList, list):

    def as_ul(self):
        if not self.data:
            return ''

        return format_html(
            u'class="{}"',
            "error"
        )

    def __str__(self):
        return self.as_ul()


class EduPlanFrom(forms.Form):

    subject = al.ModelChoiceField('SubjectAutocomplete')
    total_credit = forms.FloatField(
        widget=forms.TextInput(attrs={'class': 'small-input'})
    )

    kmro = forms.FloatField(
        widget=forms.TextInput(attrs={'class': 'small-input'})
    )

    kmd = forms.FloatField(
        widget=forms.TextInput(attrs={'class': 'small-input'})
    )

    semester_1 = forms.CharField(
        required=False,
        widget=forms.TextInput(attrs={'class': 'small-input'})
    )

    semester_2 = forms.CharField(
        required=False,
        widget=forms.TextInput(attrs={'class': 'small-input'})
    )

    semester_3 = forms.CharField(
        required=False,
        widget=forms.TextInput(attrs={'class': 'small-input'})
    )

    semester_4 = forms.CharField(
        required=False, widget=forms.TextInput(attrs={'class': 'small-input'})
    )

    semester_5 = forms.CharField(
        required=False,
        widget=forms.TextInput(attrs={'class': 'small-input'})
    )

    semester_6 = forms.CharField(
        required=False,
        widget=forms.TextInput(attrs={'class': 'small-input'})
    )

    semester_7 = forms.CharField(
        required=False, widget=forms.TextInput(attrs={'class': 'small-input'})
    )

    semester_8 = forms.CharField(
        required=False,
        widget=forms.TextInput(attrs={'class': 'small-input'})
    )

    test = forms.CharField(
        required=True,
        widget=forms.TextInput(attrs={'class': 'small-input'})
    )

    exam = forms.CharField(
        required=True,
        widget=forms.TextInput(attrs={'class': 'small-input'})
    )

    def __init__(self, *args, **kwargs):
        super(EduPlanFrom, self).__init__(*args, **kwargs)
        self.error_class = MyErrorList


class SpecialityForm(forms.Form):
    speciality = al.ModelChoiceField("SpecialityAutocomplete", label="")


class SetGroupEducationalPlan(forms.Form):
    learning_group = al.ModelChoiceField("LearningGroupAutocomplete")
    semester = al.ModelChoiceField("SemesterAutocomplete")


class AssignToTeacherForm(forms.Form):
    def __init__(self, *args, **kwargs):
        self.educational_plans = kwargs.pop('educational_plan', None)
        super(AssignToTeacherForm, self).__init__(*args, **kwargs)
        i = 0
        for edu_plan in self.educational_plans:
            self.fields['name_{}'.format(str(i))] = forms.CharField(initial=edu_plan.subject.name)
            self.fields['test_{}'.format(str(i))] = forms.CharField(initial=edu_plan.total_credits)
            self.fields['teacher_{}'.format(str(i))] = forms.CharField(widget=al.ChoiceWidget("TeacherAutocomplete"))
            i += 1


class EducationalPlanForm(forms.ModelForm):
    class Meta:
        model = EducationalPlan
        exclude = []
        widgets = {
                'subject': al.ChoiceWidget("SubjectAutocomplete")
                }

    def __init__(self, *args, **kwargs):
        super(EducationalPlanForm, self).__init__(*args, **kwargs)


class TestAssignFormset(BaseFormSet):
    def __init__(self, *args, **kwargs):
        super(TestAssignFormset, self).__init__(*args, **kwargs)

    def _construct_form(self, i, **kwargs):
        form = super(TestAssignFormset, self)._construct_form(i, **kwargs)
        form.fields['teacher'] = forms.CharField(widget=al.ChoiceWidget("TeacherAutocomplete"))
        return form


class ModelFormsetFactoryFormset(BaseModelFormSet):
    def __init__(self, *args, **kwargs):
        super(ModelFormsetFactoryFormset, self).__init__(*args, **kwargs)

    def _construct_form(self, i, **kwargs):
        form = super(ModelFormsetFactoryFormset, self)._construct_form(i, **kwargs)
        form.fields['teacher'] = forms.CharField(widget=al.ChoiceWidget("TeacherAutocomplete"))
        return form


class UploadMarkbookForm(forms.Form):
    file = forms.FileField()
    learning_group = al.ModelChoiceField("LearningGroupAutocomplete")
