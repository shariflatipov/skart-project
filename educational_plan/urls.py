# -*- coding: utf-8 -*-

from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^educational_plan/$', views.educational_plan, name='educational_plan'),
    url(r'^list/$', views.learning_group_list, name='educational_plan_list'),
    url(r'^markbooks/all/$', views.show_all_markbooks, name='show_all_markbooks'),
    url(r'^markbook/upload/$', views.upload_markbook, name='upload_markbook'),
    url(r'^students/upload/$', views.upload_new_students, name='upload_students'),
    url(r'^assign_to_teacher/(?P<edu_id>\d+)/(?P<group>\d+)/(?P<selected_semester>\d+)/$',
        views.assign_to_teacher, name='assign_to_teacher'),
    url(r'^download/(?P<pk>\d+)/$', views.download_markbook, name='download_markbook'),
    url(r'^select_group_semester/$', views.select_educational_plan_for_group, name='select_group_semester'),
]
