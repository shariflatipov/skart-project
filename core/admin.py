from django.contrib import admin
from core.models import Setting, Grade, Subject, LessonType, ControlType, Semester,\
    Week, SemesterWeek, YearSemester, EducationalYear, DefaultSettings


class SettingAdmin(admin.ModelAdmin):
    list_display = ('variable', 'value')
    list_filter = ('variable', 'value')


class SubjectAdmin(admin.ModelAdmin):
    search_fields = ['name']

admin.site.register(Setting, SettingAdmin)

admin.site.register(Grade)
admin.site.register(Subject, SubjectAdmin)
admin.site.register(LessonType)
admin.site.register(ControlType)
admin.site.register(Semester)
admin.site.register(Week)
admin.site.register(SemesterWeek)
admin.site.register(YearSemester)
admin.site.register(EducationalYear)
admin.site.register(DefaultSettings)
