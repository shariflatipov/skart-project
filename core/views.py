# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect
from core.forms import SubjectForm
from core.models import Subject


def subject_add(request):
    if request.method == 'POST':
        form = SubjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('subject_list')
    else:
        form = SubjectForm()

    return render(request, 'general_form/add_form.html', {"form": form,
                                                          "title": "Subjects",
                                                          "cancel_url": reverse('subject_list')})


def subject_list(request):
    return render(request, 'core/subject_list.html',
                  {'subjects': Subject.objects.all()})
