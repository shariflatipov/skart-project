# -*- coding: utf-8 -*-
import autocomplete_light.shortcuts as al
from django.utils.translation import ugettext_lazy as _
from .models import Subject, Semester

al.register(Subject,
            search_fields=['name'],
            choices=Subject.objects.all(),
            attrs={
                'placeholder': _('Subject name?'),
                'data-autocomplete-minimum-characters': 2,
            },
            widget_attrs={
                'data-widget-maximum-values': 4,
                'class': 'modern-style',
            },
            )

al.register(Semester,
        search_fields=['name'],
        choices=Semester.objects.all(),
        attrs={
            'placeholder': _('Semester'),
            'data-autocomplete-minimum-characters': 2,
        },
        widget_attrs={
            'data-widget-maximum-values': 4,
            'class': 'modern-style',
        },
        )
