# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-01-05 11:31
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0005_auto_20160105_1556'),
    ]

    operations = [
        migrations.CreateModel(
            name='DefaultSettings',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('current_semester', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Semester', verbose_name='Current semester')),
                ('current_semester_week', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.SemesterWeek', verbose_name='Current semester week')),
                ('current_week', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Week', verbose_name='Current week')),
                ('current_year_semester', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.YearSemester', verbose_name='Current year and semester')),
            ],
        ),
    ]
