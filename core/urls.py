# -*- coding: utf-8 -*-
from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^subject/add$', views.subject_add, name='subject_add'),
    url(r'^subject/list$', views.subject_list, name='subject_list'),
]
