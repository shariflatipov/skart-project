# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext_lazy as _
# from solo.models import SingletonModel


# class Organization(SingletonModel):
#     """Организация пример: ТГУ ПБП """
#     from people.models import User
#
#     name = models.CharField(max_length=200)
#     boss = models.ForeignKey(User)
#
#     def __unicode__(self):
#         return unicode(_(self.name))
#
#     class Meta:
#         verbose_name = _('Organization')
#         verbose_name_plural = _('Organizations')


# Grade class
class Grade(models.Model):
    letter = models.CharField(max_length=2, unique=True)
    digital = models.IntegerField()
    from_grade = models.FloatField()
    to_grade = models.FloatField()
    text = models.CharField(max_length=15)

    @classmethod
    def get_digital(cls, grade):
        return cls.objects.get(from_grade__lte=grade,
                               to_grade__gte=grade).digital

    def __unicode__(self):
        return self.text

    class Meta:
        verbose_name = _('Grade')
        verbose_name_plural = _('Grades')


# Subject class
class Subject(models.Model):
    """Предмет"""
    code = models.CharField(_("Code"), max_length=100, unique=True)
    name = models.CharField(_("Name"), max_length=100)

    def __unicode__(self):
        return unicode(_(self.name))

    class Meta:
        verbose_name = _('Subject')
        verbose_name_plural = _('Subjects')


class Setting(models.Model):
    """Различного рода глобальные настройки"""
    variable = models.CharField(max_length=50, unique=True)
    value = models.CharField(max_length=100)

    def __unicode__(self):
        return unicode(_(self.variable))

    class Meta:
        verbose_name = _('Setting')
        verbose_name_plural = _('Settings')


class LessonType(models.Model):
    """Виды уроков"""

    code = models.CharField(_('Code'), max_length=20, unique=True)
    name = models.CharField(_('Name'), max_length=50, unique=True)
    # Необходимость участия преподователя
    teacher_need = models.BooleanField()

    def __unicode__(self):
        return unicode(self.name)

    class Meta:
        verbose_name = _("Lesson Type")
        verbose_name_plural = _("Lesson Types")


class ControlType(models.Model):
    """Виды контроля (экзамены)"""

    code = models.CharField(_("code"), max_length=100, unique=True)
    name = models.CharField(_("name"), max_length=100, unique=True)

    def __unicode__(self):
        return unicode(self.name)

    class Meta:
        verbose_name = _("Control type")
        verbose_name_plural = _("Control types")


class Semester(models.Model):
    """Наименование семестров"""
    code = models.CharField(_("code"), max_length=100, unique=True)
    name = models.CharField(_("name"), max_length=100, unique=True)

    # This is the order of semesters so if I want to programmatically 
    # change the semester I will get next semester from this position
    # Digital represantation of semester e.g (I = 1, II = 2....)
    order = models.PositiveSmallIntegerField(_('Positional order'), null=True, unique=True)

    def __unicode__(self):
        return unicode(self.name)

    class Meta:
        verbose_name = _("Semester")
        verbose_name_plural = _("Semesters")


class Week(models.Model):
    """Недели обучения"""
    code = models.CharField(_("code"), max_length=50, unique=True)
    name = models.CharField(_("name"), max_length=50, unique=True)

    def __unicode__(self):
        return unicode(self.name)

    class Meta:
        verbose_name = _("Week")
        verbose_name_plural = _("Weeks")


class EducationalYear(models.Model):
    start_year = models.PositiveSmallIntegerField()
    end_year = models.PositiveSmallIntegerField()

    def __unicode__(self):
        return u'{} - {}'.format(self.start_year, self.end_year)

    class Meta:
        unique_together = ('start_year', 'end_year')


class YearSemester(models.Model):
    year = models.ForeignKey(EducationalYear, verbose_name=_('Educational year'))
    semester = models.ForeignKey(Semester, verbose_name=_('Semester'))

    def __unicode__(self):
        return u'{} - {}'.format(self.year, self.semester)

    class Meta:
        unique_together = ('year', 'semester')


class SemesterWeek(models.Model):
    """
    Предназначен для того чтобы из других моделей мог ссылатся на
    конкретную неделю в семестре
    """

    code = models.CharField(_('Code'), max_length=50, unique=True)
    semester = models.ForeignKey(Semester, verbose_name=_('Semester'))
    week = models.ForeignKey(Week, verbose_name=_('Week'))

    def __unicode__(self):
        return u'{} - {}'.format(self.semester.name, self.week.name)

    class Meta:
        unique_together=('semester', 'week')
        verbose_name = _('Week in semester')
        verbose_name_plural = _('Weeks in semester')


class DefaultSettings(models.Model):
    """Default application settings"""
    current_year_semester = models.ForeignKey(YearSemester, verbose_name=_("Current year and semester"))
    current_semester_week = models.ForeignKey(SemesterWeek, verbose_name=_("Current semester week"))
    current_semester = models.ForeignKey(Semester, verbose_name=_("Current semester"))
    current_week = models.ForeignKey(Week, verbose_name=_("Current week"))
