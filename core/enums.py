# -*- coding: utf-8 -*-

from django.utils.translation import ugettext_lazy as _

# ------- State --------------
ACTIVE = 0
DISABLED = 1
DELETED = 2
# Работает
WORKING = 3
# В отпуске
ON_LEAVE = 4
# Уволен
FIRED = 5
# Ушел с работы
LEAVE = 6
FINISHED = 7
NEW = 8
PROCESSING = 9

STATE = ((ACTIVE, _('Active')), (DISABLED, _('Disabled')), (DELETED, _('Deleted')),
          (WORKING, _("Working")), (ON_LEAVE, _("On leave")), (FIRED, _("Fired")), 
          (LEAVE, _("Leave")), (FINISHED, _('Finished')), (NEW, _('New')), 
          (PROCESSING, _('Processing')))

# --------- Educational attendant type ------
DISTANT = 0
ATTENDANT = 1
EDU_ATTENDANT_TYPE_CHOICES = ((DISTANT, _('distant')), (ATTENDANT, _('full time')))

# --------- Educational model ---------------
TRADITIONAL = 0
CREDIT = 1
EDU_MODEL_CHOICES = ((TRADITIONAL, _('traditional')), (CREDIT, _('credit')))

# -------- Marriage status ------------------
MARRIED = 0
SINGLE = 1
DIVORCED = 2
MARRIAGE_STATUS_CHOICES = ((MARRIED, _('Married')), (SINGLE, _('Single')), (DIVORCED, _('Divorced')))

# ------- Educational system ----------------
BACHELOR = 0
MAGISTRATE = 1
EDU_COURSE_TYPE = ((BACHELOR, _('Bachelor')), (MAGISTRATE, _('Magistrate')))

# -------- Education types ----------


# ------- Education language -------

TAJIK = 'TAJIK'
RUSSIAN = 'RUSSIAN'
ENGLISH = 'ENGLISH'
EDU_LANGUAGE_CHOICE = ((TAJIK, _("Tajik")), (RUSSIAN, _("Russian")), (ENGLISH, _('English')))


# ------- department_type -------

FULLTIME = 'FULLTIME'
INABSENTIA = 'INABSENTIA'   
LESSON_CHOICES = ((FULLTIME, ('Очный')),(INABSENTIA, ('Заочный'),))

# ------- type_of_training -------
CONTRACTUAL='CONTRACTUAL'
BUDGETARY='BUDGETARY'
TRAINING_CHOICES=((CONTRACTUAL, 'Контрактный'),(BUDGETARY,'Бюджетный'),)

#----------institution-----------
SCHOOL='SCHOOL'
HIGHTSCHOOL='HIGHTSCHOOL'
LITZY='LITZY'
GYMNASIUM='GYMNASIUM'
COLLEGE='COLLEGE'

TECHNICALCOLLEGE='TECHNICAL COLLEGE'
INSTITUTION_CHOICES=((SCHOOL, 'Средняя школа'),(HIGHTSCHOOL,'Высшая школа'),(LITZY,'Лицей'),(GYMNASIUM,'Гимназия'),(COLLEGE,'Колледж'),(TECHNICALCOLLEGE,'Техникум'),)

FEMALE = 0
MALE = 1  
GENDER_CHOICES = ((FEMALE, _('жеское')), (MALE, _('мужское')))

