from django.conf.urls import url
from . import views

urlpatterns = [
    # ------------ country -------------------
    url(r'country/$', views.country_list, name='country_list'),
    url(r'country/(?P<country_id>[0-9]+)/$', views.country_show, name='country_show'),
    url(r'country/edit/(?P<country_id>[0-9]+)/$', views.country_edit, name='country_edit'),
    url(r'country/add', views.country_add, name='country_add'),

    # ---------------- region -------------------
    url(r'region/$', views.region_list, name='region_list'),
    url(r'region/(?P<region_id>[0-9]+)/$', views.region_show, name='region_show'),
    url(r'region/edit/(?P<region_id>[0-9]+)/$', views.region_edit, name='region_edit'),
    url(r'region/add', views.region_add, name='region_add'),

    # ---------------- city -------------------
    url(r'city/$', views.city_list, name='city_list'),
    url(r'city/data_source', views.data_source, name='city_data_source'),
    url(r'city/(?P<city_id>[0-9]+)/$', views.city_show, name='city_show'),
    url(r'city/edit/(?P<city_id>[0-9]+)/$', views.city_edit, name='city_edit'),
    url(r'city/add', views.city_add, name='city_add'),

    # ---------------- address -------------------
    url(r'address/$', views.address_list, name='address_list'),
    url(r'address/(?P<address_id>[0-9]+)/$', views.address_show, name='address_show'),
    url(r'address/edit/(?P<address_id>[0-9]+)/$', views.address_edit, name='address_edit'),
    url(r'address/add', views.address_add, name='address_add'),
]
