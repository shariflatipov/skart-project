from django.contrib import admin
from location.models import *


class CountryAdmin(admin.ModelAdmin):
    list_display = ('name',)
    list_filter = ('name',)

admin.site.register(Country, CountryAdmin)
admin.site.register(Region)
admin.site.register(City)
admin.site.register(Address)

