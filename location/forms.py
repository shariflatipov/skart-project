# -*- coding: utf-8 -*-
import autocomplete_light.shortcuts as al
from django import forms
from location.models import Country, Address, Region, City


class AddressForm(al.ModelForm):
    city = al.ModelChoiceField('CityAutocomplete')

    def __init__(self, *args, **kwargs):
        super(AddressForm, self).__init__(*args, **kwargs)
        self.fields['address'].widget.attrs['class'] = 'form-control'
        self.fields['city'].widget.attrs['class'] = 'form-control'
        self.fields['state'].widget.attrs['class'] = 'form-control'

    class Meta:
        model = Address
        exclude = []


class CountryForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(CountryForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['class'] = 'form-control'

    class Meta:
        model = Country
        exclude = []


class RegionForm(al.ModelForm):
    country = al.ModelChoiceField('CountryAutocomplete')

    def __init__(self, *args, **kwargs):
        super(RegionForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['class'] = 'form-control'
        self.fields['country'].widget.attrs['class'] = 'form-control'

    class Meta:
        model = Region
        exclude = []


class CityForm(al.ModelForm):
    region = al.ModelChoiceField('RegionAutocomplete')

    def __init__(self, *args, **kwargs):
        super(CityForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['class'] = 'form-control'
        self.fields['region'].widget.attrs['class'] = 'form-control'

    class Meta:
        model = City
        exclude = []
