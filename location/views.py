import autocomplete_light.shortcuts as al
import json
from django.core.urlresolvers import reverse
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext, loader
from location.models import Country, City, Address, Region
from location.forms import CountryForm, RegionForm, CityForm, AddressForm

al.autodiscover()


def country_list(request):
    return render(request, 'location/country/index.html', {'countries': Country.objects.all()})


def country_show(request, country_id):
    country = get_object_or_404(Country, pk=country_id)
    return render(request, 'location/country/show.html', {'country': country})


def country_edit(request, country_id):
    country = get_object_or_404(Country, pk=country_id)
    if request.method == 'GET':
        form = CountryForm(instance=country)
    if request.method == 'POST':
        form = CountryForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            country.name = cd["name"]
            country.state = cd["state"]
            country.save()
            return HttpResponseRedirect('/location/country')

    return render(request, 'general_form/edit_form.html', {"form": form, "title": 'Country',
                                                           "cancel_url": reverse('country_list')})


def country_add(request):
    if request.method == 'POST':
        form = CountryForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect("/location/country")
    else:
        form = CountryForm()

    return render(request, 'general_form/add_form.html', {"form": form, "title": 'Country',
                                                          "cancel_url": reverse('country_list')})


# **************************************** region views *************************************************************
def region_list(request):
    return render(request, 'location/region/index.html', {'regions': Region.objects.all()})


def region_show(request, region_id):
    region = get_object_or_404(Region, pk=region_id)
    return HttpResponse(request, 'location/region/show.html', {'region': region})


def region_edit(request, region_id):
    region = get_object_or_404(Region, pk=region_id)
    form = RegionForm(instance=region)
    if request.method == 'POST':
        form = RegionForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            region.name = cd["name"]
            region.country = cd["country"]
            region.state = cd["state"]
            region.save()
            return HttpResponseRedirect("/location/region")

    return render(request, 'general_form/edit_form.html', {"form": form, "title": 'Region',
                                                           "cancel_url": reverse('region_list')})


def region_add(request):
    if request.method == 'POST':
        form = RegionForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect("/location/region")
    else:
        form = RegionForm()

    return render(request, 'general_form/add_form.html', {"form": form, "title": 'Region',
                                                          "cancel_url": reverse('region_list')})


# **************************************** city views *************************************************************

# ------------ data_source method ---------------------
def data_source(request):
    start = int(request.GET.get("start"))
    length = int(request.GET.get("length"))
    to = start + length
    total = City.objects.count()

    search_value = request.GET.get("search[value]")

    if search_value != "":
        cities = City.objects.filter(name__contains=search_value)
        total = cities.count()
    else:
        cities = City.objects.all()[start:to]

    response_data = {
        "recordsTotal": total,
        "recordsFiltered": total
    }

    response = []

    for obj in cities:
        links = "<a href = '/location/city/edit/" + str(obj.pk) + \
                "'>Edit</a> | <a href = '/location/city/show/" + str(obj.pk) + "'>Show</a>"
        city = [
            obj.pk,
            obj.name,
            obj.region.name,
            obj.state,
            links
        ]

        response.append(city)

    response_data["data"] = response

    return HttpResponse(json.dumps(response_data), content_type='application/json')


# ------------ index method ---------------------
def city_list(request):
    return render(request, 'location/city/index.html')


# --------------- show method ------------------
def city_show(request, city_id):
    city = get_object_or_404(City, pk=city_id)
    return render(request, 'location/city/show.html', {'city': city})


# --------------- edit --------------------------
def city_edit(request, city_id):
    city = get_object_or_404(City, pk=city_id)
    form = CityForm(instance=city)
    if request.method == 'POST':
        form = CityForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            city.name = cd["name"]
            city.region = cd["region"]
            city.state = cd["state"]
            city.save()
            return HttpResponseRedirect("/location/city")

    return render(request, 'general_form/edit_form.html', {"form": form, "title": 'City',
                                                           "cancel_url": reverse('city_list')})


def city_add(request):
    if request.method == 'POST':
        form = CityForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect("/location/city")
    else:
        form = CityForm()

    return render(request, 'general_form/add_form.html', {"form": form, "title": 'City',
                                                          "cancel_url": reverse('city_list')})


# **************************************** address views *************************************************************
def address_list(request):
    return render(request, 'location/address/index.html',
                  {'addresses': Address.objects.all()})


def address_show(request, address_id):
    address = get_object_or_404(Address, pk=address_id)
    return render(request, 'location/address/index.html', {'address': address})


def address_edit(request, address_id):
    address = get_object_or_404(Address, pk=address_id)
    form = AddressForm(instance=address)
    if request.method == 'POST':
        form = AddressForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            address.address = cd["address"]
            address.city = cd["city"]
            address.state = cd["state"]
            address.save()
            return HttpResponseRedirect("/location/address")

    return render(request, 'general_form/edit_form.html', {"form": form, "title": 'Address',
                                                           "cancel_url": reverse('address_list')})


def address_add(request):
    if request.method == 'POST':
        form = AddressForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect("/location/address")
    else:
        form = AddressForm()

    return render(request, 'general_form/add_form.html', {"form": form, "title": 'Address',
                                                          "cancel_url": reverse('address_list')})
