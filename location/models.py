# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Country(models.Model):
    """Страны"""
    name = models.CharField(_("Name"), max_length=100, null=False, blank=False)
    state = models.BooleanField(_("State"), default=True)

    def __unicode__(self):
        return unicode(self.name)

    class Meta:
        verbose_name = _("Country")
        verbose_name_plural = _("Countries")


class Region(models.Model):
    """Области(Штаты, Регионы)"""
    name = models.CharField(_("Name"), max_length=100, null=False, blank=False)
    state = models.BooleanField(_("State"), default=True)
    country = models.ForeignKey(Country)

    def __unicode__(self):
        return "%s - %s " % (unicode(self.name), unicode(self.country))

    class Meta:
        verbose_name = _("Region")
        verbose_name_plural = _("Regions")


class City(models.Model):
    """Города"""
    name = models.CharField(_("Name"), max_length=100, null=False, blank=False)
    state = models.BooleanField(_("State"), default=True)
    region = models.ForeignKey(Region)

    def __unicode__(self):
        return "%s - %s " % (unicode(self.name), unicode(self.region))

    class Meta:
        verbose_name = _("City")
        verbose_name_plural = _("Cities")


class Address(models.Model):
    """Адрес"""
    address = models.CharField(_("Address"), max_length=100, null=False, blank=False)
    city = models.ForeignKey(City)
    state = models.BooleanField(_("State"))

    def __unicode__(self):
        return '%s - %s' % (unicode(self.city.name), unicode(self.address))

    class Meta:
        verbose_name = _("Address")
        verbose_name_plural = _("Addresses")
