# -*- coding: utf-8 -*-
import autocomplete_light.shortcuts as al
from models import City, Region, Address
from django.utils.translation import ugettext_lazy as _
from location.models import Country

min_chars = 2

al.register(Region,
            choices=Region.objects.all(),
            search_fields=['name'],
            attrs={
                'placeholder': _("Start typing ?"),
                'data-autocomplete-minimum-characters': min_chars,
            },
            widget_attrs={
                'data-widget-maximum-values': 4,
                'class': 'modern-style',
            },
            )

al.register(Address,
            choices=Address.objects.all(),
            search_fields=['address'],
            attrs={
                'placeholder': _("Start typing ?"),
                'data-autocomplete-minimum-characters': min_chars,
            },
            widget_attrs={
                'data-widget-maximum-values': 4,
                'class': 'modern-style',
            },
            )

al.register(City,
            choices=City.objects.all(),
            search_fields=['name'],
            attrs={
                'placeholder': _("Start typing ?"),
                'data-autocomplete-minimum-characters': min_chars,
            },
            widget_attrs={
                'data-widget-maximum-values': 4,
                'class': 'modern-style',
            },
            )

al.register(Country, choices=Country.objects.all(), attrs = {
                                                            'placeholder': _("Country"),
                                                            'data-autocomplete-minimum-characters': min_chars,
                                                        },
                                                        widget_attrs={
                                                            'data-widget-maximum-values': 4,
                                                            'class': 'modern-style',
                                                        })
