/**
 * Created by jamshed on 5/12/15.
 */
$(document).ready(function() {
        var serverSide = $("#table").attr("data-serverSide");
        var dataSource = $("#table").attr("data-source");
        var autoInit = $("#table").attr("data-init");

    if(dataSource != undefined && serverSide == undefined){
        $('#table').dataTable({
            "processing": true,
            "serverSide": true,
            "ajax": dataSource
        });
    }else if(autoInit == undefined){
        $('#table').dataTable();
    }

});
// для того чтобы запрашивать данные с сервера при помощи ajax в первую очередь задайте  data-source = "путь до JSON файла или API".
// Если Вы не хотите получать запросы при помощи ajax просте не надо добавлять никаких элементов
// Если хотите задать настройку для таблици индивидуально то вам надо будет добавить аттрибут data-init = 'false' или  data-init и настроить dataTables {% block head %} <script> $("#table").dataTable({..Ваши настройки ..}); </script>