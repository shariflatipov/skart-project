# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-03-29 14:11
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0007_semester_order'),
        ('people', '0007_auto_20160216_2219'),
    ]

    operations = [
        migrations.CreateModel(
            name='StudentStatistics',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('total_absents', models.FloatField(default=0)),
                ('total_weeks', models.FloatField(default=0)),
                ('total_score', models.FloatField(default=0)),
                ('absents_with_good_cause', models.FloatField(default=0)),
                ('semester', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Semester', verbose_name='Semester')),
            ],
            options={
                'verbose_name': 'Student statistics',
                'verbose_name_plural': 'Students statistics',
            },
        ),
        migrations.RemoveField(
            model_name='student',
            name='total_absents',
        ),
        migrations.RemoveField(
            model_name='student',
            name='total_score',
        ),
        migrations.RemoveField(
            model_name='student',
            name='total_weeks',
        ),
        migrations.AddField(
            model_name='studentstatistics',
            name='student',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='people.Student', verbose_name='Student'),
        ),
    ]
