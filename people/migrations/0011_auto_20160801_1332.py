# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-08-01 08:32
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('location', '0001_initial'),
        ('learning_process', '0008_auto_20160203_1712'),
        ('people', '0010_auto_20160412_1639'),
    ]

    operations = [
        migrations.AddField(
            model_name='student',
            name='address',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='location.Address', verbose_name=b'Address'),
        ),
        migrations.AddField(
            model_name='student',
            name='applicante',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name='Applicante'),
        ),
        migrations.AddField(
            model_name='student',
            name='attestat',
            field=models.CharField(blank=True, max_length=65, null=True, verbose_name='Attestat'),
        ),
        migrations.AddField(
            model_name='student',
            name='characteristic',
            field=models.BooleanField(default=False, verbose_name='active'),
        ),
        migrations.AddField(
            model_name='student',
            name='date_get_passport',
            field=models.DateField(blank=True, null=True, verbose_name='date get passport'),
        ),
        migrations.AddField(
            model_name='student',
            name='department_type',
            field=models.CharField(choices=[(b'FULLTIME', b'\xd0\x9e\xd1\x87\xd0\xbd\xd1\x8b\xd0\xb9'), (b'INABSENTIA', b'\xd0\x97\xd0\xb0\xd0\xbe\xd1\x87\xd0\xbd\xd1\x8b\xd0\xb9')], default=b'FULLTIME', max_length=15),
        ),
        migrations.AddField(
            model_name='student',
            name='diplom',
            field=models.BooleanField(default=False, verbose_name='active'),
        ),
        migrations.AddField(
            model_name='student',
            name='employment',
            field=models.CharField(blank=True, max_length=65, null=True, verbose_name='Employment history'),
        ),
        migrations.AddField(
            model_name='student',
            name='employment_history',
            field=models.BooleanField(default=False, verbose_name='active'),
        ),
        migrations.AddField(
            model_name='student',
            name='expiration_date_school',
            field=models.DateField(blank=True, null=True, verbose_name='Expiration date'),
        ),
        migrations.AddField(
            model_name='student',
            name='faculty',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='learning_process.Faculty', verbose_name=b'Faculty'),
        ),
        migrations.AddField(
            model_name='student',
            name='father_name',
            field=models.CharField(blank=True, max_length=30, null=True, verbose_name='father name'),
        ),
        migrations.AddField(
            model_name='student',
            name='father_phone_number',
            field=models.CharField(blank=True, max_length=65, null=True, verbose_name='number father'),
        ),
        migrations.AddField(
            model_name='student',
            name='foreign_language',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='Foreign language'),
        ),
        migrations.AddField(
            model_name='student',
            name='image',
            field=models.BooleanField(default=False, verbose_name='active'),
        ),
        migrations.AddField(
            model_name='student',
            name='institution',
            field=models.CharField(choices=[(b'SCHOOL', b'\xd0\xa1\xd1\x80\xd0\xb5\xd0\xb4\xd0\xbd\xd1\x8f\xd1\x8f \xd1\x88\xd0\xba\xd0\xbe\xd0\xbb\xd0\xb0'), (b'HIGHTSCHOOL', b'\xd0\x92\xd1\x8b\xd1\x81\xd1\x88\xd0\xb0\xd1\x8f \xd1\x88\xd0\xba\xd0\xbe\xd0\xbb\xd0\xb0'), (b'LITZY', b'\xd0\x9b\xd0\xb8\xd1\x86\xd0\xb5\xd0\xb9'), (b'GYMNASIUM', b'\xd0\x93\xd0\xb8\xd0\xbc\xd0\xbd\xd0\xb0\xd0\xb7\xd0\xb8\xd1\x8f'), (b'COLLEGE', b'\xd0\x9a\xd0\xbe\xd0\xbb\xd0\xbb\xd0\xb5\xd0\xb4\xd0\xb6'), (b'TECHNICAL COLLEGE', b'\xd0\xa2\xd0\xb5\xd1\x85\xd0\xbd\xd0\xb8\xd0\xba\xd1\x83\xd0\xbc')], default=b'SCHOOL', max_length=25),
        ),
        migrations.AddField(
            model_name='student',
            name='language',
            field=models.CharField(choices=[(b'TAJIK', 'Tajik'), (b'RUSSIAN', 'Russian'), (b'ENGLISH', 'English')], default=b'TAJIK', max_length=55),
        ),
        migrations.AddField(
            model_name='student',
            name='mather_name',
            field=models.CharField(blank=True, max_length=30, null=True, verbose_name='mather name'),
        ),
        migrations.AddField(
            model_name='student',
            name='mather_phone_number',
            field=models.CharField(blank=True, max_length=65, null=True, verbose_name='number mather'),
        ),
        migrations.AddField(
            model_name='student',
            name='medal',
            field=models.CharField(blank=True, max_length=65, null=True, verbose_name='Medal'),
        ),
        migrations.AddField(
            model_name='student',
            name='medal_count',
            field=models.CharField(blank=True, max_length=65, null=True, verbose_name='Medal count'),
        ),
        migrations.AddField(
            model_name='student',
            name='medic_info',
            field=models.BooleanField(default=False, verbose_name='active'),
        ),
        migrations.AddField(
            model_name='student',
            name='region',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='location.Region', verbose_name=b'Region'),
        ),
        migrations.AddField(
            model_name='student',
            name='shahodatnoma',
            field=models.BooleanField(default=False, verbose_name='active'),
        ),
        migrations.AddField(
            model_name='student',
            name='shool_language',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='shool_language', to='learning_process.Language', verbose_name=b'shool language'),
        ),
        migrations.AddField(
            model_name='student',
            name='speciality',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='learning_process.Speciality', verbose_name='Speciality'),
        ),
        migrations.AddField(
            model_name='student',
            name='statement',
            field=models.BooleanField(default=False, verbose_name='active'),
        ),
        migrations.AddField(
            model_name='student',
            name='type_of_training',
            field=models.CharField(choices=[(b'CONTRACTUAL', b'\xd0\x9a\xd0\xbe\xd0\xbd\xd1\x82\xd1\x80\xd0\xb0\xd0\xba\xd1\x82\xd0\xbd\xd1\x8b\xd0\xb9'), (b'BUDGETARY', b'\xd0\x91\xd1\x8e\xd0\xb4\xd0\xb6\xd0\xb5\xd1\x82\xd0\xbd\xd1\x8b\xd0\xb9')], default=b'CONTRACTUAL', max_length=15),
        ),
        migrations.AddField(
            model_name='student',
            name='what_city_school',
            field=models.CharField(blank=True, max_length=65, null=True, verbose_name='What city school'),
        ),
        migrations.AddField(
            model_name='student',
            name='workstation',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name='Workstation'),
        ),
        migrations.AlterField(
            model_name='student',
            name='entrance_date',
            field=models.DateField(blank=True, default=django.utils.timezone.now, null=True, verbose_name='entrance date'),
        ),
        migrations.AlterField(
            model_name='student',
            name='learning_group',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='learning_process.LearningGroup', verbose_name='Learning group'),
        ),
    ]
