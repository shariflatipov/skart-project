# -*- coding: utf-8 -*-

from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from .models import Position, Student, Rank, User, Teacher, StudentOperationDocument


class UserCreationForm(forms.ModelForm):
    """A form for creating new users. Includes all the required
    fields, plus a repeated password."""
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)

    class Meta:
        model = User
        exclude = []

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = User
        exclude = []

    def clean_password(self):
        return self.initial["password"]


class UserAdmin(UserAdmin):
    # The forms to add and change user instances
    form = UserChangeForm
    add_form = UserCreationForm

    list_display = ('first_name', 'last_name', 'username', 'email', 'is_superuser', 'is_active')
    list_filter = ('is_superuser', 'is_active')
    search_fields = ('email', 'username',)
    ordering = ('email', 'username',)
    filter_horizontal = ()


class StudentAdmin(admin.ModelAdmin):
    list_display = ('username', 'first_name', 'last_name', 'is_active', 'learning_group')
    list_filter = ('is_active', 'learning_group__speciality__code')
    search_fields = ['learning_group__speciality__code', 'username', 'first_name', 'last_name']


class StudentOperationDocumentAdmin(admin.ModelAdmin):
    raw_id_fields = ("students",)


class TeacherAdmin(admin.ModelAdmin):
    search_fields = ('first_name', 'last_name',)


admin.site.register(User, UserAdmin)
admin.site.register(Teacher, TeacherAdmin)
admin.site.register(Rank)
admin.site.register(Position)
admin.site.register(Student, StudentAdmin)
admin.site.register(StudentOperationDocument, StudentOperationDocumentAdmin)
