# -*- coding: utf-8 -*-
import autocomplete_light.shortcuts as al
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic.edit import UpdateView
from django.http import Http404
from people.forms import UserForm, TeacherForm, PositionForm, StudentForm, ChangePassForAllForm
from people.models import Teacher, Position, Student
al.autodiscover()


def people_add(request):
    if request.method == 'POST':
        form = UserForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('place_address_list')
    else:
        form = UserForm()

    return render(request, 'people/add.html', {"form": form})


def teacher_list(request):
    return render(request, 'people/teacher/list.html',
                  {'teachers': Teacher.objects.all().order_by('pk')})


def teacher_add(request):
    if request.method == 'POST':
        form = TeacherForm(request.POST)
        if form.is_valid():
            t = form.save()
            t.set_password('123')
            t.save(update_fields=['password'])

            return redirect('teachers_list')
    else:
        form = TeacherForm()

    return render(request, 'general_form/add_form.html',
                  {"form": form, "title": 'Teachers',
                   "cancel_url": reverse('teachers_list')})


class UpdateTeacher(UpdateView):
    model = Teacher
    form_class = TeacherForm
    template_name = 'people/teacher/update_form.html'
    success_url = '/people/teacher/list'


def teacher_detail(request, teacher_id):
    teacher = get_object_or_404(Teacher, pk=teacher_id)
    return render(request, 'people/teacher/detail.html', {
        'teacher': teacher
    })


def student_list(request):
    return render(request, 'people/student/list2.html',
                  {'students': Student.objects.all()})


def student_add(request):
    if request.method == 'POST':
        form = StudentForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            s = form.save()
            s.set_password(cd['password'])
            s.save(update_fields=['password'])

            return redirect('student_list')
    else:
        form = StudentForm()

    return render(request, 'general_form/add_form.html',
                  {"form": form, "title": 'Student', "cancel_url": reverse('student_list')})


def position_add(request):
    if request.method == 'POST':
        form = PositionForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('positions_list')
    else:
        form = PositionForm()

    return render(request, 'general_form/add_form.html',
                  {"form": form, "title": 'Positions', "cancel_url": reverse('positions_list')})


def positions_list(request):
    return render(request, 'people/positions_list.html',
                  {'positions': Position.objects.all()})


def block_people(request):

    students = Student.objects.all()

    for student in students:
        pass

    return render(request, 'people/block_student.html', {'students': students})


def change_pass(request):
    """Changes password of all students
    """
    form = None
    if request.user.is_superuser:
        if request.method == 'POST':
            form = ChangePassForAllForm(request.POST)
            if form.is_valid():
                cd = form.cleaned_data
                pass_word = cd['password']
                students = Student.objects.all()
                for student in students:
                    student.set_password(pass_word)
                    student.save()
                return render(request, 'people/student/change_pass.html', {'form': form})
        else:
            form = ChangePassForAllForm()
            return render(request, 'people/student/change_pass.html', {'form': form})
    else:
        raise Http404
