# -*- coding: utf-8 -*-
import xlrd
from datetime import datetime
from django.db.transaction import atomic
from core.enums import ATTENDANT, DISTANT, TRADITIONAL
from learning_process.models import LearningGroup, Speciality, Specialization, Language
from people.models import Student
from testing.models import STANDARD, TestVariantShuffled, TestVariantBase, StudentTestResult

__author__ = 'Sharif'


def compare_student_and_test(answers, **kwargs):
    """
    Gets as parameters student answers as dict, TestVariantShuffled
    and returns score of the student depending on what scores are
    prepended for question types
    """

    try:
        student = Student.objects.get(pk=kwargs['student'])
        test_variant_base = TestVariantBase.objects.get(test_case__pk=kwargs['test'], code=kwargs['variant'])

        correct_answers = TestVariantShuffled.objects.filter(test_variant_base=test_variant_base)

        standard = correct_answers.filter(question__test_type=STANDARD).order_by('question_position')
        # TODO check students fitness answers 
        # fitness = correct_answers.filter(question__test_type=FITNESS).order_by('question_position')
        # mark = 25 because we didn't checked fitness questions 
        # instead we just said that all fitness questions were answered correctly 
        mark = 25
        question_counter = 1

        for answer in standard:
            if 'c-pos' in answer.answers_positions:
                if answer.answers_positions['c-pos'] == answers['S'+str(question_counter)]:
                    # print "question {} is answered correctly".format(question_counter)
                    mark += 3
                # else:
                #     pass
                    # print "question right answer position {}: answered position {}".format(
                    #     answer.answers_positions['c-pos'], answers['S' + str(question_counter)])
            else:
                raise KeyError('Correct answer not exists')
            question_counter += 1

        print "{}, {}, {}".format(
            test_variant_base.test_case.subject,
            student,
            mark
        )

        results = StudentTestResult()
        results.answers = answers
        results.student = student
        results.test_variant_base = test_variant_base
        results.total_score = mark
        results.save()
    except Exception, e:
        print e


class Excel:
    def __init__(self):
        self.specialities = Speciality.objects.all()

    @atomic
    def fill_speciality(self, file_name):
        document = xlrd.open_workbook(file_name)
        sheet = document.sheet_by_index(0)

        # Stub for specialization
        if Specialization.objects.count() <= 0:
            specialization = Specialization()
            specialization.name = 'empty'
            specialization.code = 'empty'
            specialization.save()

        for row_num in range(sheet.nrows):
            row = sheet.row_values(row_num)
            spec_spec = row[0].split('-')

            speciality = Speciality.objects.get_or_create(code=spec_spec[0])
            speciality[0].name = row[1]
            speciality[0].save()

            if len(spec_spec) > 1:

                specialization = Specialization()
                specialization.name = spec_spec[1]
                specialization.code = spec_spec[1]
                specialization.speciality = speciality[0]
                specialization.save()

        self.specialities = Speciality.objects.all()

    @atomic
    def fill_groups(self, file_name):
        document = xlrd.open_workbook(file_name)
        sheet = document.sheet_by_index(0)
        for row_num in range(sheet.nrows):
            row = sheet.row_values(row_num)

            learning_group = LearningGroup()
            learning_group.pk = int(row[0])

            speciality = row[3].split('-')

            learning_group.speciality = Speciality.objects.get(code=speciality[0])

            learning_group.language = Language.objects.get(pk=int(row[8]))

            study_type = ATTENDANT
            letter = row[2][-1]
            if letter == u'ғ':
                letter = row[2][-2:-1]
                study_type = DISTANT

            learning_group.subgroup_letter = letter
            learning_group.study_type = study_type

            learning_group.study_model = TRADITIONAL
            learning_group.entrance_year = datetime(year=2015 - int(row[4]), month=1, day=1)
            learning_group.course = int(row[1])
            learning_group.state = 1
            learning_group.save()

    @atomic
    def fill_users(self, file_name):
        document = xlrd.open_workbook(file_name)
        sheet = document.sheet_by_index(0)
        for row_num in range(sheet.nrows):
            row = sheet.row_values(row_num)
            student = Student()
            student.learning_group = LearningGroup.objects.get(pk=int(row[16]))
            student.pk = int(row[0])
            student.first_name = unicode(row[3])

            student.middle_name = unicode(row[4])
            student.last_name = unicode(row[5])
            student.gender = bool(row[6])
            student.birth_date = '2015-01-01'
            student.specialization = Specialization.objects.get(pk=1)
            student.username = row[1]
            student.set_password("123")
            student.save()


def fill_all():

    l = Language()
    l.pk = 1
    l.code = 'ru'
    l.name = 'ru'
    l.short_name = u'р'
    l.save()

    l = Language()
    l.pk = 2
    l.code = 'tj'
    l.name = 'tj'
    l.short_name = u'т'
    l.save()

    l = Language()
    l.pk = 3
    l.code = 'en'
    l.name = 'en'
    l.short_name = u'а'
    l.save()

    # ex = Excel()
    # ex.fill_speciality("/home/sharif/Desktop/specialty.xlsx")
    # ex.fill_groups("/home/sharif/Desktop/groups.xlsx")
    # ex.fill_users("/home/sharif/Desktop/peoples.xlsx")


if __name__ == '__main__':
    pass
