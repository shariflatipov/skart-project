# -*- coding: utf-8 -*-
import autocomplete_light.shortcuts as al
from django.utils.translation import ugettext_lazy as _
from people.models import Teacher, Position

al.register(Teacher,
            search_fields=['first_name', 'last_name', 'middle_name'],
            choices=Teacher.objects.all(),
            attrs={
                'placeholder': _('Teacher'),
                'data-autocomplete-minimum-characters': 0,
            },
            widget_attrs={
                'data-widget-maximum-values': 4,
                'class': 'modern-style',
            },
            )

al.register(Position,
            search_fields=['code', 'name'],
            choices=Position.objects.all(),
            attrs={
                'placeholder': _('Position'),
                'data-autocomplete-minimum-characters': 0,
            },
            widget_attrs={
                'data-widget-maximum-values': 4,
                'class': 'modern-style',
            },
            )
