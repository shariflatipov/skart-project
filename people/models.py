# -*- coding: utf-8 -*-
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.core.mail import send_mail
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.core import validators
from core.models import Semester
from core.enums import MARRIAGE_STATUS_CHOICES, SINGLE
from core.enums import TAJIK, EDU_LANGUAGE_CHOICE, LESSON_CHOICES, FULLTIME, INSTITUTION_CHOICES, SCHOOL, TRAINING_CHOICES, CONTRACTUAL
from refs.models import PlacesAddress, PASSPORT_OFFICE, Nationality
from location.models import City


class UserOpsStates:
    MOVE = 0
    EXPEL = 1
    REINSTATE = 2
    ACADEMIC_LEAVE = 3

    OPERATIONS = ((MOVE, _('Move')),
                  (EXPEL, _('Expel')),
                  (REINSTATE, _('Reinstate')),
                  (ACADEMIC_LEAVE, _('Academic leave')))

    ACTIVE = 0
    DISABLED = 1
    BLOCKED = 2

    STATES = ((ACTIVE, _('Active')), (DISABLED, _('Disabled')), (BLOCKED, _('Blocked')))


class AuthManager(BaseUserManager):
    def create_user(self, username, email, password=None):
        if not email:
            raise ValueError(_("User must have an email address"))
        if not username:
            raise ValueError(_("User must have an username"))
        user = self.model(username=username, email=self.normalize_email(email))
        user.is_active = True
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, email, password):
        user = self.model(username=username, email=self.normalize_email(email))
        user.is_active = True
        user.set_password(password)
        user.is_staff = True
        user.is_superuser = True

        user.save(using=self._db)

        return user


def update_filename(instance, filename):
    return 'people/pics/{0}.jpg'.format(instance.id)


class User(AbstractBaseUser, PermissionsMixin):
    """
    An abstract base class implementing a fully featured User model with
    admin-compliant permissions.

    Username, password and email are required. Other fields are optional.
    """

    FEMALE = 0
    MALE = 1
    GENDER_CHOICE = ((FEMALE, _("female")), (MALE, _("male")))

    username = models.CharField(_('username'), max_length=30, unique=True,
                                help_text=_('Required. 30 characters or fewer. Letters, digits and '
                                            '@/./+/-/_ only.'),
                                validators=[validators.RegexValidator(r'^[\w.@+-]+$',
                                            _('Enter a valid username. '
                                              'This value may contain only letters, numbers '
                                              'and @/./+/-/_ characters.'), 'invalid')],
                                error_messages={
                                    'unique': _("A user with that username already exists.")})

    first_name = models.CharField(_('first name'), max_length=30, blank=True, null=True)
    middle_name = models.CharField(_('middle name'), max_length=30, blank=True, null=True)
    last_name = models.CharField(_('last name'), max_length=30, blank=True, null=True)
    phone = models.CharField(_('phone'), max_length=40, blank=True, null=True)
    phone_additional = models.CharField(_('Additional phone'), max_length=40, blank=True, null=True)
    gender = models.BooleanField(_('gender'), choices=GENDER_CHOICE, default=MALE)
    birth_date = models.DateField(_('Birth date'), blank=True, null=True)

    email = models.EmailField(_('email address'), blank=True)
    is_staff = models.BooleanField(_('staff status'), default=False,
                                   help_text=_('Designates whether the user can log into this admin '
                                               'site.'))
    is_active = models.BooleanField(_('active'), default=True,
                                    help_text=_('Designates whether this user should be treated as '
                                                'active. Unselect this instead of deleting accounts.'))
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)
    passport_series = models.CharField(_('Passport series'), max_length=3, blank=True, null=True)
    passport_number = models.CharField(_('Passport number'), max_length=8, blank=True, null=True)
    passport_office = models.ForeignKey(PlacesAddress, verbose_name=_("Passport office"),
                                        related_name='passport_office', limit_choices_to={'type': PASSPORT_OFFICE},
                                        blank=True, null=True)

    birth_place = models.ForeignKey(City, verbose_name=_('Birth place'), null=True, blank=True)
    registration = models.CharField(max_length=255, null=True, blank=True)
    actual_living_address = models.CharField(max_length=255, null=True, blank=True)

    nationality = models.ForeignKey(Nationality, verbose_name=_('Nationality'), related_name='nationality',
                                    blank=True, null=True)
    picture = models.ImageField(verbose_name="Picture", blank=True, null=True, upload_to=update_filename)
    state = models.PositiveSmallIntegerField(_('State'), choices=UserOpsStates.STATES, default=UserOpsStates.ACTIVE)

    objects = AuthManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    def get_full_name(self):
        """
        Returns the middle_name plus first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s %s' % (self.last_name, self.first_name, self.middle_name)
        return full_name.strip()

    def get_short_name(self):
        # Returns the short name for the user.
        return '%s %s. %s.' % ((self.last_name or ""),
                               (self.first_name[0] if len(self.first_name or "") > 0 else ""),
                               (self.middle_name[0] if len(self.middle_name or "") > 0 else ""))

    def email_user(self, subject, message, from_email=None, **kwargs):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email], **kwargs)

    def is_teacher(self):
        return hasattr(self, 'teacher')

    def is_student(self):
        return hasattr(self, 'student')

#    def save(self, *args, **kwargs):
#        picture = Image.open(self.picture)
#        size = (256, 256)
#        picture.thumbnail(size, Image.ANTIALIAS)
#        self.picture = picture
#        super(User, self).save(*args, **kwargs)


class Position(models.Model):
    code = models.CharField(max_length=100, unique=True)
    name = models.CharField(max_length=100)

    def __unicode__(self):
        return unicode(_(self.name))

    class Meta:
        verbose_name = _('Position')
        verbose_name_plural = _('Positions')


class Rank(models.Model):
    """Every people have rank as Ph'd or Magister"""
    code = models.CharField(max_length=200, unique=True)
    name = models.CharField(max_length=300, unique=True)

    def __unicode__(self):
        return unicode(self.name)

    class Meta:
        verbose_name = _("Rank")
        verbose_name_plural = _("Ranks")


class Teacher(User):
    from learning_process.models import Faculty

    faculty = models.ForeignKey(Faculty, verbose_name=_("Faculty"), null=True, blank=True)
    rank = models.ForeignKey(Rank, verbose_name=_("Rank"), null=True, blank=True)
    position = models.ForeignKey(Position, verbose_name=_("Position"))
    marriage_status = models.PositiveSmallIntegerField(_("Marriage status"),
                                                       choices=MARRIAGE_STATUS_CHOICES,
                                                       default=SINGLE)
    objective_information = models.TextField(verbose_name=_("Objective information"))

    def __unicode__(self):
        return self.get_full_name()

    class Meta:
        verbose_name = _('Teacher')
        verbose_name_plural = _('Teachers')


class Student(User):
    from location.models import Region, Address
    from learning_process.models import Specialization, Speciality, LearningGroup, Faculty, Language
    learning_group = models.ForeignKey(LearningGroup, verbose_name=_("Learning group"), blank=True, null=True)
    specialization = models.ForeignKey(Specialization, verbose_name=_("Specialization"),
                                       blank=True, null=True)
    speciality = models.ForeignKey(Speciality, verbose_name=("Speciality"), blank=True, null=True)
    faculty = models.ForeignKey(Faculty, verbose_name=("Faculty"), blank=True, null=True)
    address = models.ForeignKey(Address, verbose_name=("Address"), blank=True, null=True)
    region = models.ForeignKey(Region, verbose_name=("Region"), blank=True, null=True)
    entrance_date = models.DateField(_('entrance date'), default=timezone.now, blank=True, null=True)
    mather_name = models.CharField(_('mather name'), max_length=30, blank=True, null=True)
    father_name = models.CharField(_('father name'), max_length=30, blank=True, null=True)
    foreign_language = models.CharField(_("Foreign language"), max_length=100, blank=True, null=True)
    # school_number=models.CharField(_("School number"),max_length=50,blank=True,null=True)
    shool_language = models.ForeignKey(Language, verbose_name=("shool language"), related_name='shool_language', blank=True, null=True)
    applicante = models.CharField(_("Applicante"), max_length=50, blank=True, null=True)
    workstation = models.CharField(_("Workstation"), max_length=50, blank=True, null=True)
    department_type = models.CharField(max_length=15, choices=LESSON_CHOICES, default=FULLTIME)
    type_of_training = models.CharField(max_length=15, choices=TRAINING_CHOICES, default=CONTRACTUAL)
    institution = models.CharField(max_length=25, choices=INSTITUTION_CHOICES, default=SCHOOL)
    language = models.CharField(max_length=55, choices=EDU_LANGUAGE_CHOICE, default=TAJIK)

    statement = models.BooleanField(_('active'), default=False)
    image = models.BooleanField(_('active'), default=False)
    medic_info = models.BooleanField(_('active'), default=False)
    characteristic = models.BooleanField(_('active'), default=False)
    diplom = models.BooleanField(_('active'), default=False)
    shahodatnoma = models.BooleanField(_('active'), default=False)
    employment_history = models.BooleanField(_('active'), default=False)
    employment = models.CharField(_("Employment history"), max_length=65, blank=True, null=True)
    what_city_school = models.CharField(_("What city school"), max_length=65, blank=True, null=True)
    expiration_date_school = models.DateField(_('Expiration date'), blank=True, null=True)
    attestat = models.CharField(_("Attestat"), max_length=65, blank=True, null=True)
    medal = models.CharField(_("Medal"), max_length=65, blank=True, null=True)
    medal_count = models.CharField(_("Medal count"), max_length=65, blank=True, null=True)
    mather_phone_number = models.CharField(_("number mather"), max_length=65, blank=True, null=True)
    father_phone_number = models.CharField(_("number father"), max_length=65, blank=True, null=True)
    date_get_passport = models.DateField(_('date get passport'), blank=True, null=True)

    def __unicode__(self):
        return self.get_full_name()

    class Meta:
        verbose_name = _('Student')
        verbose_name_plural = _('Students')
        permissions = (('operate_student', 'Operations on students'),)


class StudentStatistics(models.Model):
    student = models.ForeignKey(Student, verbose_name=_("Student"))
    semester = models.ForeignKey(Semester, verbose_name=_("Semester"))
    total_absents = models.FloatField(default=0)
    total_weeks = models.FloatField(default=0)
    total_score = models.FloatField(default=0)
    absents_with_good_cause = models.FloatField(default=0)

    def __unicode__(self):
        return "{} - {}".format(self.student, self.semester)

    class Meta:
        verbose_name = _('Student statistics')
        verbose_name_plural = _('Students statistics')


class StudentOperationDocument(models.Model):
    operation_document_number = models.PositiveIntegerField(_('Order'))
    order_file = models.FileField()
    operation = models.PositiveIntegerField(choices=UserOpsStates.OPERATIONS)
    students = models.ManyToManyField(Student, verbose_name=_('Students'))
    document_sign_date = models.DateField()
    created_date = models.DateField(auto_now=True)
    created_by = models.ForeignKey(User, verbose_name=_("User"), related_name='history_creator')
