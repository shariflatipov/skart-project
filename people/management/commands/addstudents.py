# -*- coding: utf-8 -*-
__author__ = 'sharif'

from openpyxl import load_workbook
from django.core.management.base import BaseCommand, CommandError
from people.utils import compare_student_and_test

class Command(BaseCommand):
    help = 'Adds students from excel sheet'

    def add_arguments(self, parser):
        parser.add_argument('--file', type=str)

    def handle(self, *args, **options):

        f = options['file']

        try:
            wb = load_workbook(filename=f, read_only=True)
            sheet = wb[u'Лист1']
            first_row = True
            for row in sheet.rows:
                if first_row:
                    first_row = False
                    continue

                test_id = row[0].value
                student_id = row[1].value
                variant_id = row[6].value

                kwargs = {'student': student_id, 'test': test_id, 'variant': variant_id}

                fields = {'S1': row[7].value, 'S2': row[8].value, 'S3': row[9].value, 'S4': row[10].value,
                          'S5': row[11].value, 'S6': row[12].value, 'S7': row[13].value, 'S8': row[14].value,
                          'S9': row[15].value, 'S10': row[16].value, 'S11': row[17].value, 'S12': row[18].value,
                          'S13': row[19].value, 'S14': row[20].value, 'S15': row[21].value, 'S16': row[22].value,
                          'S17': row[23].value, 'S18': row[24].value, 'S19': row[25].value, 'S20': row[26].value,
                          'S21': row[27].value, 'S22': row[28].value, 'S23': row[29].value, 'S24': row[30].value,
                          'S25': row[31].value, 'F1': row[32].value, 'F2': row[33].value, 'F3': row[34].value,
                          'F4': row[35].value, 'F5': row[36].value, 'F6': row[37].value, 'F7': row[38].value,
                          'F8': row[39].value, 'F9': row[40].value, 'F10': row[41].value, 'F11': row[42].value,
                          'F12': row[43].value, 'F13': row[44].value, 'F14': row[45].value, 'F15': row[46].value,
                          'F16': row[47].value, 'F17': row[48].value, 'F18': row[49].value, 'F19': row[50].value,
                          'F20': row[51].value}

                compare_student_and_test(fields, **kwargs)
                # print fields
                # raise CommandError("Value error in row {} and cell {}".format(row_counter, cell_counter))
                # print "There are {} cells at row {}".format(cell_counter, row_counter)
        except Exception, e:
            raise CommandError('Exception %s' % e)
