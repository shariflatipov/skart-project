# -*- coding: utf-8 -*-
from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^index$', views.people_add, name='people_add'),
    url(r'^teacher/list$', views.teacher_list, name='teachers_list'),
    url(r'^teacher/add$', views.teacher_add, name='teacher_add'),
    url(r'^teacher/update/(?P<pk>\d+)/$', views.UpdateTeacher.as_view(), name='teacher_update'),
    url(r'^teacher/detail/(?P<teacher_id>\d+)/$', views.teacher_detail, name='teacher_detail'),
    url(r'^student/list$', views.student_list, name='student_list'),
    url(r'^student/add$', views.student_add, name='student_add'),
    url(r'^student/pass/change/$', views.change_pass, name='student_change_pass'),
    url(r'^position/add$', views.position_add, name='position_add'),
    url(r'^position/list$', views.positions_list, name='positions_list'),
]
