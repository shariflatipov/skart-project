# -*- coding: utf-8 -*-
import autocomplete_light.shortcuts as al

from people.models import User, Position, Teacher, Student
from django import forms
from django.utils.translation import ugettext_lazy as _


class UserForm(al.ModelForm):

    nationality = al.GenericModelChoiceField('NationalityAutocomplete', )
    passport_office = al.GenericModelChoiceField('PlacesAddressAutocomplete')

    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)
        self.fields['birth_date'].widget.attrs['class'] = 'datepicker'
        self.fields['nationality'].widget.attrs['class'] = 'form-control'

    class Meta:
        model = User
        autocomplete_fields = ('passport_office',)
        exclude = ['date_joined', 'is_superuser', 'is_staff', 'last_login', 'user_permissions', 'groups']


class TeacherForm(al.ModelForm):

    def __init__(self, *args, **kwargs):
        super(TeacherForm, self).__init__(*args, **kwargs)
        self.fields['birth_date'].widget.attrs['class'] = 'datepicker'
        # self.fields['objective_information'].widget.attrs['class'] = 'tiny_keyboard'

    class Meta:
        model = Teacher
        autocomplete_fields = ('position', 'faculty', 'birth_place')
        exclude = ['password', 'last_login', 'is_superuser', 'groups', 'user_permissions', 'is_staff', 'date_joined']


class StudentForm(al.ModelForm):

    password = forms.CharField(widget=forms.PasswordInput, label=_('Password'))

    def __init__(self, *args, **kwargs):
        super(StudentForm, self).__init__(*args, **kwargs)
        self.fields['entrance_date'].widget.attrs['class'] = 'datepicker'

    class Meta:
        model = Student
        autocomplete_fields = ('position', 'faculty', 'learning_group')
        exclude = ['password', 'last_login', 'is_superuser', 'groups', 'user_permissions', 'is_staff', 'date_joined']


class PositionForm(forms.ModelForm):
    class Meta:
        model = Position
        exclude = []


class ChangePassForAllForm(forms.Form):
    password = forms.CharField(label=_('New password'), widget=forms.PasswordInput)
