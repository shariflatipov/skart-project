# -*- coding: utf-8 -*-
import autocomplete_light.shortcuts as al
from refs.models import PlacesAddress, Nationality


class PlacesAddressForm(al.ModelForm):
    address = al.ModelChoiceField('PlacesAddressAutocomplete')

    class Meta:
        model = PlacesAddress
        exclude = []


class NationalityForm(al.ModelForm):

    class Meta:
        model = Nationality
        exclude = []
