# -*- coding: utf-8 -*-

from django.db import models
from location.models import Address
from django.utils.translation import gettext as _

PASSPORT_OFFICE = 0
BIRTH_PLACE = 1
REGISTRATION = 2
ACTUAL_LIVING_ADDRESS = 3

PLACES_CHOICES = ((PASSPORT_OFFICE, _('Passport office')),
                  (BIRTH_PLACE, _('Birth place')),
                  (REGISTRATION, _('Registration')),
                  (ACTUAL_LIVING_ADDRESS, _("Actual living address")))


class PlacesAddress(models.Model):
    """Адрес паспортных столов"""
    name = models.CharField(_("Name"), max_length=255)
    address = models.ForeignKey(Address, verbose_name=_("Address"))
    type = models.PositiveSmallIntegerField(_('Type'), choices=PLACES_CHOICES)

    def __unicode__(self):
        return unicode(self.name)

    class Meta:
        verbose_name = _("Place address")
        verbose_name_plural = _("Places address")


class Nationality(models.Model):
    """Национальность"""
    code = models.CharField(_('Code'), max_length=20)
    name = models.CharField(_('Name'), max_length=50)

    def __unicode__(self):
        return unicode(self.name)

    class Meta:
        verbose_name_plural = _('Nationality')
        verbose_name = _('Nationality')
