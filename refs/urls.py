# -*- coding: utf-8 -*-
from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'places_address/$', views.place_address_list, name='place_address_list'),
    url(r'places_address/add/$', views.places_address_add, name='places_address_add'),
    url(r'places_address/edit/(?P<places_address_id>[0-9]+)/$', views.places_address_edit, name='places_address_edit'),
    url(r'places_address/show/(?P<places_address_id>[0-9]+)/$', views.places_address_show, name='places_address_show'),

    # ********************************* nationality **********************************
    url(r'nationality/$', views.nationality_list, name='nationality_list'),
    url(r'nationality/add', views.nationality_add, name='nationality_add'),
    url(r'nationality/edit/(?P<nationality_id>[0-9]+)/$', views.nationality_edit, name='nationality_edit'),
    url(r'nationality/show/(?P<nationality_id>[0-9]+)/$', views.nationality_show, name='nationality_show'),

]
