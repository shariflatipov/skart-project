# -*- coding: utf-8 -*-
from location.models import Address
from refs.models import PlacesAddress, Nationality
import autocomplete_light.shortcuts as al
from django.utils.translation import ugettext_lazy as _

al.register(PlacesAddress,
            search_fields=['address'],
            choices=Address.objects.all(),
            attrs={
                'placeholder': _('Start typing address ?'),
                'data-autocomplete-minimum-characters': 2,
            },
            widget_attrs={
                'data-widget-maximum-values': 4,
                'class': 'modern-style',
            },
            )

al.register(Nationality,
            search_fields=['name', 'code'],
            choices=Nationality.objects.all(),
            attrs={
                'placeholder': _('Nationality ?'),
                'data-autocomplete-minimum-characters': 3,
            },
            widget_attrs={
                'data-widget-maximum-values': 4,
                'class': 'modern-style',
            },
            )
