# -*- coding: utf-8 -*-
# Generated by Django 1.9.1 on 2016-08-01 08:11
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('refs', '0005_auto_20160420_1110'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='nationality',
            options={'verbose_name': 'Nationality', 'verbose_name_plural': 'Nationality'},
        ),
        migrations.AlterModelOptions(
            name='placesaddress',
            options={'verbose_name': 'Place address', 'verbose_name_plural': 'Places address'},
        ),
        migrations.AlterField(
            model_name='nationality',
            name='code',
            field=models.CharField(max_length=20, verbose_name=b'Code'),
        ),
        migrations.AlterField(
            model_name='nationality',
            name='name',
            field=models.CharField(max_length=50, verbose_name=b'Name'),
        ),
        migrations.AlterField(
            model_name='placesaddress',
            name='address',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='location.Address', verbose_name=b'Address'),
        ),
        migrations.AlterField(
            model_name='placesaddress',
            name='name',
            field=models.CharField(max_length=255, verbose_name=b'Name'),
        ),
        migrations.AlterField(
            model_name='placesaddress',
            name='type',
            field=models.PositiveSmallIntegerField(choices=[(0, b'Passport office'), (1, b'Birth place'), (2, b'Registration'), (3, b'Actual living address')], verbose_name=b'Type'),
        ),
    ]
