import autocomplete_light.shortcuts as al
from django.core.urlresolvers import reverse
from refs.forms import PlacesAddressForm, NationalityForm
from refs.models import PlacesAddress, Nationality
from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponseRedirect

al.autodiscover()


def place_address_list(request):
    return render(request, 'refs/places_address/index.html',
                  {'places_addresses': PlacesAddress.objects.all()})


def places_address_show(request, places_address_id):
    places_address = get_object_or_404(PlacesAddress, pk=places_address_id)
    return render(request, 'refs/places_address/show.html',
                  {'places_address': places_address})


def places_address_edit(request, places_address_id):
    places_address = get_object_or_404(PlacesAddress, pk=places_address_id)
    if request.method == 'GET':
        form = PlacesAddressForm(instance=places_address)
    if request.method == 'POST':
        form = PlacesAddressForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            places_address.name = cd["name"]
            places_address.address = cd["address"]
            places_address.type = cd["type"]
            places_address.save()
            return redirect('place_address_list')

    return render(request, 'general_form/add_form.html', {"form": form, "title": 'Place address',
                                                          "cancel_url": reverse('place_address_list')})


def places_address_add(request):
    if request.method == 'POST':
        form = PlacesAddressForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("place_address_list")
    else:
        form = PlacesAddressForm()

    return render(request, 'general_form/add_form.html', {"form": form, "title": 'Place address',
                                                          "cancel_url": reverse('place_address_list')})


def nationality_list(request):
    return render(request, 'refs/nationality/index.html',
                  {'nationalities': Nationality.objects.all()})


def nationality_show(request, nationality_id):
    nationality = get_object_or_404(Nationality, pk=nationality_id)
    return render(request, 'refs/nationality/show.html',
                  {'nationality': nationality})


def nationality_edit(request, nationality_id):
    nationality = get_object_or_404(Nationality, pk=nationality_id)
    if request.method == 'GET':
        form = NationalityForm(instance=nationality)
    if request.method == 'POST':
        form = NationalityForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            nationality.name = cd["name"]
            nationality.address = cd["code"]
            nationality.save()
            return redirect('nationality_list')

    return render(request, 'general_form/edit_form.html', {"form": form, "title": 'Nationality',
                                                           "cancel_url": reverse('nationality_list')})


def nationality_add(request):
    if request.method == 'POST':
        form = NationalityForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("nationality_list")
    else:
        form = NationalityForm()

    return render(request, 'general_form/add_form.html', {"form": form, "title": 'Nationality',
                                                          "cancel_url": reverse('nationality_list')})
