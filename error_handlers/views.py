# -*- coding: utf-8 -*-

from django.shortcuts import render


# Страница не найдена
def page_not_found(request):
    return render(request, 'error_handlers/404.html')


# Произошла ошибка на сервере
def server_error(request):
    return render(request, 'error_handlers/500.html')


# Доступ запрещен
def permission_denied(request):
    return render(request, 'error_handlers/403.html')


# Негодный запрос
def bad_request(request):
    return render(request, 'error_handlers/400.html')
