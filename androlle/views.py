# -*- coding: utf-8 -*-
import autocomplete_light.shortcuts as al
from django.shortcuts import render
import autocomplete_light.shortcuts as al
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect, get_object_or_404
from django.views.generic.edit import UpdateView
from django.http import Http404,HttpResponse
from django.forms import ModelForm
from django.utils import timezone
from datetime import datetime,date,time
from people.models import Teacher, Position, Student,User
from refs.models import Nationality
from django.db.models.query_utils import Q
from operator import itemgetter
from core.enums import INABSENTIA, FULLTIME,TAJIK,RUSSIAN
from androlle.forms import RegionForm,ReportForm,SchoolForm,GenRegionForm,WelcomeStudentForm,ChangeUserPassword
from django.contrib.auth.hashers import check_password
import random
al.autodiscover()

# Create your views here.

def welcome_student(request):
    foo=['A','B','C','D','E','F','G','I','J','K','L','M','O','P','R','S','T','V','W','Y','Z','0','1','2','3','4','5','6','7','8','9']
    ran=''
    i=0
    while i<5:
        ran+=random.choice(foo)
        i+=1
    t=Student.objects.all().count()
    student=Student.objects.all()
    if t<=0:
        t=1
        l=1
    else:
        l=student[t-1].id
    today=Student.objects.filter(entrance_date=date.today()).count()
    if request.method == 'POST':
        form = WelcomeStudentForm(request.POST)
        if form.is_valid():
            s = form.save()         
            return redirect('result_students')
    else:

        form = WelcomeStudentForm() 
    return render(request, 'general_form/add_students_form.html', {"form": form,'l':l,'t':t,'ran':ran+str(l),'today':today})


def result_students(request):
    t=Student.objects.all().count()
  
    student=Student.objects.all()
    l=student[t-1].id
    return render(request, 'people/student/list.html',
                                             {"students":Student.objects.all(),'l':l})

def general_information(request,student_id=1):
    student=Student.objects.get(id=student_id)
    return render(request, 'general_form/general_information.html', {'student':student})



def region_report(request):
    if request.method=='POST':
        form=RegionForm(request.POST)
        if form.is_valid():
            cd=form.cleaned_data
            region=cd['region']
            students=Student.objects.filter(region=region)
    else:
       
        students=Student.objects.all()
        form=RegionForm()

    return render(request, 'people/student/report.html', {'students':students,'form':form})   

def report_gen_reg(request):
    if request.method=='POST':
        form=GenRegionForm(request.POST)
        if form.is_valid():
            cd=form.cleaned_data
            gender=cd['gender']
            region=cd['region']
            students=Student.objects.filter(gender=gender,region=region)
    else:
        students=Student.objects.all()
        form=GenRegionForm()
    return render(request, 'people/student/report.html', {'students':students,'form':form})

def report_list(request):
    
    return render(request, 'people/student/report.html', {'students':Student.objects.all()})    


def change_user_password(request):
    user_id=request.user.id
    user=User.objects.get(id=user_id)
    form=None
    if request.method=='POST':
        form=ChangeUserPassword(request.POST)
        print form
        if form.is_valid():
            cd=form.cleaned_data
            old_pass=cd['old_password']
            pass_word=cd['password']
            if check_password(old_pass,user.password):
                print pass_word
                user.set_password(pass_word)
                user.save()
                succes="New password saved successfully"
                return render(request,'people/student/change_pass2.html',{'form':form,'succes':succes})
            else:
                succes="Error is old password"
                return render(request,'people/student/change_pass2.html',{'form':form,'succes':succes})
        else:
            succes=""
            return render(request,'people/student/change_pass.html',{'form':form,'succes':succes})
    else:
        succes=""
        form=ChangeUserPassword()
        return render(request,'people/student/change_pass.html',{'form':form,'succes':succes})