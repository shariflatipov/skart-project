# -*- coding: utf-8 -*-
import autocomplete_light.shortcuts as al

from people.models import User, Position, Teacher, Student
from django import forms
from django.forms import ModelForm
from django.utils.translation import ugettext_lazy as _

class WelcomeStudentForm(al.ModelForm):
 
    #password = forms.CharField(widget=forms.PasswordInput, label=_('Password'))
    
   
    def __init__(self, *args, **kwargs):
        super(WelcomeStudentForm, self).__init__(*args, **kwargs)
        #self.fields['entrance_date'].widget.attrs['class'] = 'datepicker'

    class Meta:
        model = Student
        autocomplete_fields = ('position', 'Faculty', 'Learning_group')
        fields = ['date_get_passport','medal_count','medal','attestat','father_phone_number','mather_phone_number','expiration_date_school','what_city_school','employment','employment_history','shahodatnoma','diplom','characteristic','medic_info','image','statement','father_name','mather_name','type_of_training','learning_group','department_type','speciality','language','first_name','last_name','middle_name','birth_date','username','gender','nationality','email','phone','passport_series','passport_number','passport_office','region','institution','foreign_language','shool_language','applicante','workstation']
        

class ReportForm(ModelForm):
    class Meta:
        model=Student
       
        fields=['entrance_date']

class RegionForm(ModelForm):
    class Meta:
        model=Student
        fields=['region']

class GenRegionForm(ModelForm):
    class Meta:
        model=Student
        fields=['region','gender']

class SchoolForm(ModelForm):
    class Meta:
        model=Student
        fields=['faculty','institution']

class ChangeUserPassword(forms.Form):
    old_password=forms.CharField(label=('Old password'),widget=forms.PasswordInput)
    password=forms.CharField(label=('New password'),widget=forms.PasswordInput)