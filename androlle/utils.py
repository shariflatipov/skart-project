# -*- coding: utf-8 -*-

from datetime import datetime,date
from docx import Document
from django.db.transaction import atomic
from django.shortcuts import render, redirect, get_object_or_404
from androlle.forms import RegionForm,ReportForm,SchoolForm,GenRegionForm,WelcomeStudentForm
from core.enums import ATTENDANT, DISTANT, TRADITIONAL,FULLTIME,RUSSIAN,TAJIK,INABSENTIA
from learning_process.models import LearningGroup, Speciality, Specialization, Language
from people.models import Student
from testing.models import STANDARD, TestVariantShuffled, TestVariantBase, StudentTestResult
from docx.shared import Cm, Pt, Inches
from people.models import Student,User
from learning_process.models import Faculty,Speciality,Language
from refs.models import Nationality 
from django.http.response import HttpResponse


def agreement_file(request,student_id=1):
    student=Student.objects.get(id=student_id)
    document=Document()
    font=document.styles['Normal'].font
    font.name='Times New Roman'
    font.size=Pt(10)
    section=document.sections
    section[0].left_margin = Cm(3)
    section[0].right_margin = Cm(1.7)
    section[0].top_margin = Cm(0.2)
    section[0].bottom_margin = Cm(2)
    response = HttpResponse(
        content_type='application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            )
    response['Content-Disposition'] = "attachment; filename=" + unicode() + ".docx"
    text1=(u'                                                                                                                   №'+student.username )  
    document.add_paragraph(text1)
    
    date_today=(u'                          '+unicode(date.today()))
    document.add_paragraph(date_today)
   
    text001=(u'')
    document.add_paragraph(text001)
    name_student=(u'                                                     '+student.last_name+u' '+student.first_name+u' '+student.middle_name)
    document.add_paragraph(name_student)
    
    text002=(u'')
    document.add_paragraph(text002)
    text003=(u'')
    document.add_paragraph(text003)

    departament=student.department_type
    if departament=='FULLTIME':
        departament=u'рузона'
    else:
        departament=u'гоибона'
    lang=student.language
    if lang=='RUSSIAN':
        lang=u'руси'
    elif lang=='TAJIK':
        lang=u'точики'
    else:
        lang=u'англиси'

    faculty=(u'                                     '+student.speciality.code+u' '+student.speciality.name+u', '+departament+u','+lang)
    document.add_paragraph(faculty)

    text004=(u'')
    document.add_paragraph(text004)
    i=0
    while i<42:
        text005=(u'')
        document.add_paragraph(text005)
        i+=1

    student_name=(u'                                                                         '+student.last_name+u' '+student.first_name+u' '+student.middle_name)
    document.add_paragraph(student_name)
    i=0
    while i<5:
        text005=(u'')
        document.add_paragraph(text005)
        i+=1
    
    name=(u'                                                                                                                                     '+student.last_name+u' '+student.first_name)
    document.add_paragraph(name)

    middle=(u'                                                                                                                                       '+student.middle_name)
    document.add_paragraph(middle)
    

    region=(u'                                                                                                                                    '+unicode(student.region))   
    document.add_paragraph(region)  

    pasport=(u'                                                                                                                                    Шиноснома: '+student.passport_series+student.passport_number)
    document.add_paragraph(pasport)
    date_get_passport=(u'                                                                                                                                                з '+unicode(student.date_get_passport))                                                                                                                                                                                      
    document.add_paragraph(date_get_passport)
    document.save(response)
    return response


def agreement_file2(request,student_id=1):
    student=Student.objects.get(id=student_id)
    document=Document()
    font=document.styles['Normal'].font
    font.name='Times New Roman'
    font.size=Pt(11)
    section=document.sections
    section[0].left_margin = Cm(2.5)
    section[0].right_margin = Cm(1.5)
    section[0].top_margin = Cm(1.2)
    section[0].bottom_margin = Cm(1.2)
    response = HttpResponse(
        content_type='application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            )
    response['Content-Disposition'] = "attachment; filename=" + unicode() + ".docx"
    
    
    title=(u'                                            Донишгоҳи давлатии ҳуқуқ, бизнес ва сиёсати Тоҷикистон')
    document.add_paragraph(title)

    zabonhat=(u'                                                                                Забонхати № '+student.username)
    document.add_paragraph(zabonhat)

    soder=(u'                                                               Оид ба қабули ҳуҷҷатҳои довталаб ')
    document.add_paragraph(soder)

    fullname=(u'Ному насаб:  '+student.last_name+u' '+student.first_name+u' '+student.middle_name)
    document.add_paragraph(fullname)

    faculty=(u'Факультет:   '+unicode(student.speciality.faculty))
    document.add_paragraph(faculty)

    departament=student.department_type
    if departament=='FULLTIME':
        departament=u'рузона'
    else:
        departament=u'гоибона'
    lang=student.language
    if lang=='RUSSIAN':
        lang=u'руси'
    elif lang=='TAJIK':
        lang=u'точики'
    else:
        lang=u'англиси'

    spets=(u'Ихтисос: '+student.speciality.code+u'  шӯъба: '+departament+u'  гуруҳ: '+lang)
    document.add_paragraph(spets)


    
    table = document.add_table(rows=0, cols=3)
    table.autofit = False
    table.style = 'TableGrid'
    table.columns[0].width = Cm(1)
    table.columns[1].width = Cm(14)
    table.columns[2].width = Cm(2.5)
    

    row_cells = table.add_row().cells
    row_cells[0].text = u'№'
    row_cells[1].text = u'                                                      Номгуи ҳуҷҷатҳо'
    row_cells[2].text = u'Кайдҳо оид ба қабул'
    
    row_cells = table.add_row().cells
    row_cells[0].text = u'1'
    row_cells[1].text = u'Ариза'
    if student.statement==True:
        sta=u'√'
    else:
        sta=u''
    row_cells[2].text = sta

    row_cells = table.add_row().cells
    row_cells[0].text = u'2'
    row_cells[1].text = u'6 адад сурат (3х4)'
    if student.image==True:
        image=u'√'
    else:
        image=u''
    row_cells[2].text = image
    row_cells = table.add_row().cells
    row_cells[0].text = u'3'
    row_cells[1].text = u'Маълумотномаи тиббии шакли 086-у'
    if student.medic_info==True:
        med=u'√'
    else:
        med=u''
    row_cells[2].text = med
    row_cells = table.add_row().cells
    row_cells[0].text = u'4'
    row_cells[1].text = u'Тавсифнома (Характеристика)'
    if student.characteristic==True:
        cha=u'√'
    else:
        cha=u''
    row_cells[2].text = cha
    
    row_cells = table.add_row().cells
    row_cells[0].text = u'5'
    if student.diplom==True:
        dip=u'√'
        dip2= (u'Аттестаь:'+student.institution.lower()+u' '+student.what_city_school+u' '+unicode(student.expiration_date_school))
    else:
        dip=u''
        dip2= u'Аттестать:'
    row_cells[2].text = dip
    row_cells[1].text=dip2

    row_cells = table.add_row().cells
    row_cells[0].text = u'6'
    if student.shahodatnoma==True:
        sha=u'√'
        sha2= (u'Шаҳодатномаи даъватшаванда: '+student.what_city_school+u' '+unicode(student.expiration_date_school))
    else:
        sha2=u'Шаҳодатномаи даъватшаванда: '
        sha=u''
    row_cells[1].text=sha2
    row_cells[2].text = sha



    row_cells = table.add_row().cells
    row_cells[0].text = u'7'
    if student.employment_history==True:
        emp=u'√'
        emp2=u'Дафтарчаи меҳнати (нусха): '+student.employment
    else:
        emp=u''
        emp2=u'Дафтарчаи меҳнати (нусха): '
    row_cells[2].text = emp
    row_cells[1].text = emp2

    row_cells = table.add_row().cells
    row_cells[0].text = u'8'
    row_cells[1].text = u''
    row_cells[2].text = u''
    

    info=(u'Дар сурати гум кардани забонхат довталаб бояд фавран ба комиссияи қабул муроҷиат намояд.')
    document.add_paragraph(info)

    shartnoma=(u'Шартномаи № '+student_id+u' аз '+unicode(date.today())+u'   Хуҷҷатхо санаи '+unicode(date.today())+u' қабул карда шудаанд.')
    document.add_paragraph(shartnoma)

    imzo=(u'Котиби техникй: __________________________________________________(имзо)')
    document.add_paragraph(imzo)
    text005=(u'')
    document.add_paragraph(text005)


    title=(u'                                            Донишгоҳи давлатии ҳуқуқ, бизнес ва сиёсати Тоҷикистон')
    document.add_paragraph(title)

    zabonhat=(u'                                                                                Парвандаи шахсии № '+student.username)
    document.add_paragraph(zabonhat)

    soder=(u'                                                               Оид ба қабули ҳуҷҷатҳои довталаб ')
    document.add_paragraph(soder)

    fullname=(u'Ному насаб:  '+student.last_name+u' '+student.first_name+u' '+student.middle_name)
    document.add_paragraph(fullname)

    faculty=(u'Факультет:   '+unicode(student.speciality.faculty))
    document.add_paragraph(faculty)

    departament=student.department_type
    if departament=='FULLTIME':
        departament=u'рузона'
    else:
        departament=u'гоибона'
    lang=student.language
    if lang=='RUSSIAN':
        lang=u'руси'
    elif lang=='TAJIK':
        lang=u'точики'
    else:
        lang=u'англиси'

    spets=(u'Ихтисос: '+student.speciality.code+u'  шӯъба: '+departament+u'  гуруҳ: '+lang)
    document.add_paragraph(spets)


    
    table = document.add_table(rows=0, cols=3)
    table.autofit = False
    table.style = 'TableGrid'
    table.columns[0].width = Cm(1)
    table.columns[1].width = Cm(14)
    table.columns[2].width = Cm(2.5)
    

    row_cells = table.add_row().cells
    row_cells[0].text = u'№'
    row_cells[1].text = u'                                                      Номгуи ҳуҷҷатҳо'
    row_cells[2].text = u'Кайдҳои оид ба қабул'
    
    row_cells = table.add_row().cells
    row_cells[0].text = u'1'
    row_cells[1].text = u'Ариза'
    if student.statement==True:
        sta=u'√'
    else:
        sta=u''
    row_cells[2].text = sta

    row_cells = table.add_row().cells
    row_cells[0].text = u'2'
    row_cells[1].text = u'6 адад сурат (3х4)'
    if student.image==True:
        image=u'√'
    else:
        image=u''
    row_cells[2].text = image

    row_cells = table.add_row().cells
    row_cells[0].text = u'3'
    row_cells[1].text = u'Маълумотномаи тиббии шакли 086-у'
    if student.medic_info==True:
        med=u'√'
    else:
        med=u''
    row_cells[2].text = med

    row_cells = table.add_row().cells
    row_cells[0].text = u'4'
    row_cells[1].text = u'Тавсифнома (Характеристика)'
    if student.characteristic==True:
        cha=u'√'
    else:
        cha=u''
    row_cells[2].text = cha

    row_cells = table.add_row().cells
    row_cells[0].text = u'5'
    if student.diplom==True:
        dip=u'√'
        dip2= (u'Аттестать:'+student.institution+u' '+student.what_city_school+u' '+unicode(student.expiration_date_school))
    else:
        dip=u''
        dip2= u'Аттестать:'
    row_cells[2].text = dip
    row_cells[1].text=dip2

    row_cells = table.add_row().cells
    row_cells[0].text = u'6'
    if student.shahodatnoma==True:
        sha=u'√'
        sha2= (u'Шаҳодатномаи даъватшаванда: '+student.what_city_school+u' '+unicode(student.expiration_date_school))
    else:
        sha2=u'Шаҳодатномаи даъватшаванда: '
        sha=u''
    row_cells[1].text=sha2
    row_cells[2].text = sha

    row_cells = table.add_row().cells
    row_cells[0].text = u'7'
    if student.employment_history==True:
        emp=u'√'
        emp2=u'Дафтарчаи меҳнати (нусха): '+student.employment
    else:
        emp=u''
        emp2=u'Дафтарчаи меҳнати (нусха): '
    row_cells[2].text = emp
    row_cells[1].text = emp2

    row_cells = table.add_row().cells
    row_cells[0].text = u'8'
    row_cells[1].text = u''
    row_cells[2].text = u''
   

    info=(u'Дар сурати гум кардани забонхат довталаб бояд фавран ба комиссияи қабул муроҷиат намояд.')
    document.add_paragraph(info)

    shartnoma=(u'Шартномаи № '+student_id+u' аз '+unicode(date.today())+u'   Хуҷҷатхо санаи '+unicode(date.today())+u' қабул карда шудаанд.')
    document.add_paragraph(shartnoma)

    imzo=(u'Котиби техникй: __________________________________________________(имзо)')
    document.add_paragraph(imzo)

    document.save(response)
    return response


def time_report(request):
    if request.method=='POST':
        form=ReportForm(request.POST)
        if form.is_valid():
            cd=form.cleaned_data
            date=cd['entrance_date']
            students=Student.objects.filter(entrance_date=date)

            document=Document()
            font=document.styles['Normal'].font
            font.name='Palatino'
            font.size=Pt(11)

            
            section=document.sections
            section[0].left_margin = Cm(2)
            section[0].right_margin = Cm(1)
            section[0].top_margin = Cm(1)
            section[0].bottom_margin = Cm(1)
            response = HttpResponse(
                content_type='application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                    )
            response['Content-Disposition'] = "attachment; filename=" + unicode() + ".docx"
            text=u'                                    Натичаи кабули хуччатхои довталабон    сана:  '+unicode(date)
            
            
            document.add_paragraph(text)
            table = document.add_table(rows=0, cols=7)
            table.autofit = False
            table.style = 'TableGrid'

            table.columns[0].width = Cm(1)
            table.columns[1].width = Cm(3)
            table.columns[2].width = Cm(4)
            table.columns[3].width = Cm(2.2)
            table.columns[4].width = Cm(2.2)
            table.columns[5].width = Cm(2.2)
            table.columns[6].width = Cm(2.2)
          

            row_cells = table.add_row().cells

            merged_cell=row_cells[0].merge(row_cells[2]).text = u'                         Шубахо '
            
            merged_cell = row_cells[3].merge(row_cells[4]).text=u'        Шубаи рузона'
            merged_cell = row_cells[5].merge(row_cells[6]).text=u'        Шубаи гоибона'
          

            row_cells = table.add_row().cells
            row_cells[0].text = u'№'
            row_cells[1].text = u'Рамз (шифр)'

            row_cells[2].text = u'Ихтисосхо'
            row_cells[3].text = u'руси'
            row_cells[4].text = u'точики'
            row_cells[5].text = u'руси'
            row_cells[6].text = u'точики'
            
            specialization=Speciality.objects.all()
            students=Student.objects.all()
            nationality=Nationality.objects.all()
            i = 1
            for gss in specialization:

                row_cells = table.add_row().cells
                row_cells[0].text = str(i)
                row_cells[1].text = str(gss.code)
                row_cells[2].text = unicode(gss.name)
                row_cells[3].text = unicode(students.filter(speciality=gss,department_type=FULLTIME,language=RUSSIAN,entrance_date=date).count())
                row_cells[4].text = unicode(students.filter(speciality=gss,department_type=FULLTIME,language=TAJIK,entrance_date=date).count())
                row_cells[5].text = unicode(students.filter(speciality=gss,department_type=INABSENTIA,language=RUSSIAN,entrance_date=date).count())
                row_cells[6].text = unicode(students.filter(speciality=gss,department_type=INABSENTIA,language=TAJIK,entrance_date=date).count())
               
                i += 1
            row_cells = table.add_row().cells
            
            merged_cell=row_cells[0].merge(row_cells[2]).text=u'              Натичаи хамаги'
            row_cells[3].text = unicode(students.filter(department_type=FULLTIME,language=RUSSIAN,entrance_date=date).count())
            row_cells[4].text = unicode(students.filter(department_type=FULLTIME,language=TAJIK,entrance_date=date).count())
            row_cells[5].text = unicode(students.filter(department_type=INABSENTIA,language=RUSSIAN,entrance_date=date).count())
            row_cells[6].text = unicode(students.filter(department_type=INABSENTIA,language=TAJIK,entrance_date=date).count())
           
            document.save(response)
            return response
    else:
       
        students=Student.objects.filter(entrance_date=datetime.now())
        form=ReportForm()

    return render(request, 'people/student/report.html', {'students':students,'form':form})   


def create_doc(request):
    document=Document()
    font=document.styles['Normal'].font
    font.name='Palatino'
    font.size=Pt(9)
    section=document.sections
    section[0].left_margin = Cm(2)
    section[0].right_margin = Cm(0.8)
    section[0].top_margin = Cm(0.8)
    section[0].bottom_margin = Cm(0.8)
    response = HttpResponse(
        content_type='application/vnd.openxmlformats-officedocument.wordprocessingml.document',
            )
    response['Content-Disposition'] = "attachment; filename=" + unicode() + ".docx"
    text=u'                                              Маълумот оиди љинс ва миллати довталабон (барои соли хониши 2015-2016)'
    
    document.add_paragraph(text)
    table = document.add_table(rows=0, cols=13)
    table.autofit = False
    table.style = 'TableGrid'
    table.columns[0].width = Cm(1)
    table.columns[1].width = Cm(2)
    table.columns[2].width = Cm(2.5)
    table.columns[3].width = Cm(1.2)
    table.columns[4].width = Cm(1.2)
    table.columns[5].width = Cm(1.2)
    table.columns[6].width = Cm(1.2)
    table.columns[7].width = Cm(1.2)
    table.columns[8].width = Cm(1.2)
    table.columns[9].width = Cm(1.2)
    table.columns[10].width = Cm(1.2)
    table.columns[11].width = Cm(1.2)
    table.columns[12].width = Cm(1.2)


    row_cells = table.add_row().cells
    row_cells[0].text = u'№'
    row_cells[1].text = u'Рамз (шифр)'
    row_cells[2].text = u'Ихтисосхо'
    row_cells[3].text = u'Теьдод'
    row_cells[4].text = u'Писарон'
    row_cells[5].text = u'Духтарон'
    row_cells[6].text = u'Тожик'
    row_cells[7].text = u'Узбек'
    row_cells[8].text = u'Рус'
    row_cells[9].text = u'Туркмен'
    row_cells[10].text = u'Киргиз'
    row_cells[11].text = u'Карес'
    row_cells[12].text = u'Дигар миллатхо'
    faculty=Faculty.objects.all()
    students=Student.objects.all()
    nationality=Nationality.objects.all()
    i = 1
    for gss in faculty:

        row_cells = table.add_row().cells
        row_cells[0].text = str(i)
        row_cells[1].text = str("shifr")
        row_cells[2].text = unicode(gss.name)
        row_cells[3].text = unicode(students.filter(faculty=gss,entrance_date=datetime.now()).count())
        row_cells[4].text = unicode(students.filter(faculty=gss,gender=True,entrance_date=datetime.now()).count())
        row_cells[5].text = unicode(students.filter(faculty=gss,gender=False,entrance_date=datetime.now()).count())
        row_cells[6].text = unicode(students.filter(faculty=gss,nationality=nationality[1],entrance_date=datetime.now()).count())
        row_cells[7].text = unicode(students.filter(faculty=gss,nationality=nationality[2],entrance_date=datetime.now()).count())
        row_cells[8].text = unicode(students.filter(faculty=gss,nationality=nationality[0],entrance_date=datetime.now()).count())
        row_cells[9].text = unicode(students.filter(faculty=gss,nationality=nationality[3],entrance_date=datetime.now()).count())
        row_cells[10].text = unicode(students.filter(faculty=gss,nationality=nationality[4],entrance_date=datetime.now()).count())
        row_cells[11].text = unicode(students.filter(faculty=gss,nationality=nationality[5],entrance_date=datetime.now()).count())
        row_cells[12].text = unicode(students.filter(faculty=gss,nationality=nationality[6],entrance_date=datetime.now()).count())
        i += 1
    row_cells = table.add_row().cells
    row_cells[0].text = str()
    row_cells[1].text = str("Natijai hamagi")
    row_cells[3].text = unicode(students.filter(entrance_date=datetime.now()).count())
    row_cells[4].text = unicode(students.filter(gender=True,entrance_date=datetime.now()).count())
    row_cells[5].text = unicode(students.filter(gender=False,entrance_date=datetime.now()).count())
    row_cells[6].text = unicode(students.filter(nationality=nationality[1],entrance_date=datetime.now()).count())
    row_cells[7].text = unicode(students.filter(nationality=nationality[2],entrance_date=datetime.now()).count())
    row_cells[8].text = unicode(students.filter(nationality=nationality[0],entrance_date=datetime.now()).count())
    row_cells[9].text = unicode(students.filter(nationality=nationality[3],entrance_date=datetime.now()).count())
    row_cells[10].text = unicode(students.filter(nationality=nationality[4],entrance_date=datetime.now()).count())
    row_cells[11].text = unicode(students.filter(nationality=nationality[5],entrance_date=datetime.now()).count())
    row_cells[12].text = unicode(students.filter(nationality=nationality[6],entrance_date=datetime.now()).count())
    document.save(response)
    return respons


def create_main_report(request):
    if request.method=='POST':
        form=ReportForm(request.POST)
        if form.is_valid():
            cd=form.cleaned_data
            date=cd['entrance_date']
            students=Student.objects.filter(entrance_date=date)

            document=Document()
            font=document.styles['Normal'].font
            font.name='Palatino'
            font.size=Pt(11)
            section=document.sections
            section[0].left_margin = Cm(1.5)
            section[0].right_margin = Cm(1)
            section[0].top_margin = Cm(1)
            section[0].bottom_margin = Cm(1)
            response = HttpResponse(
                content_type='application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                    )
            response['Content-Disposition'] = "attachment; filename=" + unicode() + ".docx"
            text=u'                                              Натичаи кабули хуччатхои довталабон'
            
            document.add_paragraph(text)
            table = document.add_table(rows=0, cols=7)
            table.autofit = False
            table.style = 'TableGrid'
            table.columns[0].width = Cm(1)
            table.columns[1].width = Cm(3)
            table.columns[2].width = Cm(2.5)
            table.columns[3].width = Cm(2.2)
            table.columns[4].width = Cm(2.2)
            table.columns[5].width = Cm(2.2)
            table.columns[6].width = Cm(2.2)
          

            row_cells = table.add_row().cells
            merged_cell=row_cells[0].merge(row_cells[1]).text = u'Дата '
            row_cells[2].text = unicode(datetime.today())
            merged_cell = row_cells[3].merge(row_cells[4]).text=u'Шубаи рузона'
            merged_cell = row_cells[5].merge(row_cells[6]).text=u'Шубаи гоибона'
          

            row_cells = table.add_row().cells
            row_cells[0].text = u'№'
            row_cells[1].text = u'Рамз (шифр)'

            row_cells[2].text = u'Ихтисосхо'
            row_cells[3].text = u'Руси'
            row_cells[4].text = u'Точики'
            row_cells[5].text = u'Руси'
            row_cells[6].text = u'Точики'
            
            specialization=Speciality.objects.all()
            students=Student.objects.all()
            nationality=Nationality.objects.all()
            language=Language.objects.all()
            i = 1
            for gss in specialization:

                row_cells = table.add_row().cells
                row_cells[0].text = str(i)
                row_cells[1].text = str(gss.speciality.code)
                row_cells[2].text = unicode(gss.name)
                row_cells[3].text = unicode(students.filter(specialization=gss,department_type=FULLTIME,language=language,entrance_date=date).count())
                row_cells[4].text = unicode(students.filter(specialization=gss,department_type=FULLTIME,language=language,entrance_date=date).count())
                row_cells[5].text = unicode(students.filter(specialization=gss,department_type=INABSENTIA,language=language,entrance_date=date).count())
                row_cells[6].text = unicode(students.filter(specialization=gss,department_type=INABSENTIA,language=language,entrance_date=date).count())
               
                i += 1
            row_cells = table.add_row().cells
            
            merged_cell=row_cells[0].merge(row_cells[2]).text=u'Натичаи хамаги'
            row_cells[3].text = unicode(students.filter(department_type=FULLTIME,entrance_date=date).count())
            row_cells[4].text = unicode(students.filter(department_type=FULLTIME,entrance_date=date).count())
            row_cells[5].text = unicode(students.filter(department_type=INABSENTIA,entrance_date=date).count())
            row_cells[6].text = unicode(students.filter(department_type=INABSENTIA,entrance_date=date).count())
           
            document.save(response)
            return response
    else:
       
        students=Student.objects.filter(entrance_date=datetime.now())
        form=ReportForm()

    return render(request, 'people/student/report.html', {'students':students,'form':form})   