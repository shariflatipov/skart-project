from django.conf.urls import url
from . import views,utils


urlpatterns = [
    
    
    url(r'^result/students$', views.result_students, name='result_students'),
    url(r'^time/report$', utils.time_report, name='time_report'),
    url(r'^report/list$', views.report_list, name='report_list'),
    url(r'^region/report$', views.region_report, name='region_report'),
    url(r'^report/gen/reg$', views.report_gen_reg, name='report_gen_reg'),
   
    url(r'^welcome/student$', views.welcome_student, name='welcome_student'),
    url(r'^general/information/get/(?P<student_id>\d+)/$',views.general_information, name='general_information'),
    url(r'^change/user/password/$',views.change_user_password,name='change_user_password'),


    url(r'^agreement/file/get/(?P<student_id>\d+)/$',utils.agreement_file,name='agreement_file'),
    url(r'^agreement/file2/get/(?P<student_id>\d+)/$',utils.agreement_file2,name='agreement_file2'),
    

    url(r'^create/doc$', utils.create_doc, name='create_doc'),
    url(r'^create/main/report$', utils.create_main_report, name='create_main_report'),
   
   
]
