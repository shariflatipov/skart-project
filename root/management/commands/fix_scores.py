# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand, CommandError

from testing.models import GroupSubjectStudent
from testing.views import compare_fitness, compare_standard


class Command(BaseCommand):
    help = u'Fix scores of tests in students'

    def add_arguments(self, parser):
        parser.add_argument('--file', type=str)

    def handle(self, *args, **options):
        gss = GroupSubjectStudent.objects.all()
        i = 0

        for g in gss:
            std_count = g.group_subject.standard_question_count
            std_score = g.group_subject.standard_question_score
            ftn_score = g.group_subject.fitness_question_score
            total_score = 0

            if len(g.shuffled_questions) > 0:
                for k, v in g.shuffled_questions.items():
                    try:
                        if int(k) > std_count:
                            total_score += compare_fitness(g.answers[k], v, ftn_score)
                        else:
                            total_score += compare_standard(g.answers[k], v, std_score)
                    except KeyError, e:
                        continue

            if g.total_score != total_score:
                print "Total score of student {} changed from {} to {}".format(g, g.total_score, total_score)
            g.total_score = total_score
            g.save()
            i += 1
        print "Total records processed {}".format(str(i))

