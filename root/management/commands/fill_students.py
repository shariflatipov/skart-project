# -*- coding: utf-8 -*-
from openpyxl import load_workbook
from learning_process.models import LearningGroup, Speciality, Faculty, Language
from core.enums import DISTANT, ATTENDANT, CREDIT, ACTIVE
from people.models import Teacher, Position, Student
from django.contrib.auth.models import Group
from django.core.management.base import BaseCommand, CommandError
from django.db import transaction


class Command(BaseCommand):
    
    u"""
        --------------    Fields   ---------------------
        0 - Number, format= 1,2,3 ...  
        1 - Course, format = 3 
        2 - Specialization, format = 1-25.01.11
        3 - Language, format = тоҷикӣ 
        4 - Group letter, format = а
        5 - Educational type, format = рӯзона
        6 - Student last_name, name, middle_name
        7 - Student login, pass
    """
    
    
    help = u'Заполнение студентов из файла Excel'

    def add_arguments(self, parser):
        parser.add_argument('--file', type=str)

    def handle(self, *args, **options):
        with transaction.atomic():
            g = Group.objects.get(name='student')
            f = options['file']
            i = 1
            try:
                wb = load_workbook(filename=f, read_only=True)
                sheet = wb[u'Лист1']
                first_row = True
                for row in sheet.rows:
                    if first_row:
                        first_row = False
                        continue
                    i+=1

                    speciality = Speciality.objects.get(code=row[2].value)
                        
                    print "Speciality {} retrieved".format(speciality)
                    
                    # Languages should be stored at database
                    lang = Language.objects.get(name=row[3].value)
                    print "Language: {} retrieved".format(lang)
                    
                    if row[5].value == u'ғоибона':
                        study_type = DISTANT
                    else:
                        study_type = ATTENDANT
                    
                    print "Study type: {}".format(study_type) 
                    print "Lang: {}".format(lang)
                    print "Course: {}".format(row[1].value)
                    print "Speciality: {}".format(speciality)
                    learning_group = LearningGroup.objects.get(speciality=speciality, course=row[1].value,
                                                               language=lang, subgroup_letter=row[4].value,
                                                               state=ACTIVE, study_type=study_type)
                    print "Learning group: {} retrieved".format(learning_group)
                        

                    try:
                        student = Student()
                        student.username = row[7].value
                        student.set_password(row[7].value)
                        student_name = row[6].value.split(" ")
                        student.first_name = student_name[1]
                        student.last_name = student_name[0]
                        student.learning_group = learning_group
                        if len(student_name) >= 3:
                            student.middle_name = student_name[2]
                        else:
                            student.middle_name = ""
                            
                        student.save()
                        print "{} {}".format(student, "saved")
                        g.user_set.add(student)
                        print 'Student permissions added'
                    except Exception, e:
                        print e

            except Exception, e:
                raise CommandError('Exception %s' % e)
            g.save()
