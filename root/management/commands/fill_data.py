# -*- coding: utf-8 -*-
from openpyxl import load_workbook
from learning_process.models import LearningGroup, Speciality, Faculty, Language
from core.enums import DISTANT, ATTENDANT, CREDIT, ACTIVE
from datetime import datetime
from educational_plan.models import MarkBook
from core.models import Subject, Semester, Setting
from people.models import Teacher, Position
from django.contrib.auth.models import Group
from testing.models import TestCase, GroupSubject
from django.core.management.base import BaseCommand, CommandError


class Command(BaseCommand):
    u"""
        --------------    Fields   --------------------- 
        0 - Course, format = 3 
        1 - Specialization, format = 1-25.01.11
        2 - Specialization name, format = Аудит ва ревизия
        3 - Language, format = тоҷикӣ 
        4 - Group letter, format = а
        5 - Educational type, format = рӯзона
        6 - Subject name, format = Асосњои нархгузори
        7 - TestCase code, format = AXJE_2
        8 - Department code, format = AX
        9 - Department name, format = Системаҳои иттилоотии автоматикунонидашуда
        10 - Teacher login(password), format = MASK3
        11 - Teacher surname, format = Махмудов
        12 - Teacher first name, format = Абдурахмон
        13 - Teacher last_name, format = Субхониддинович
        14 - Faculty name, format = Инноватсия ва телекоммуникатсия
    """
    
    help = u'Работает как учебный план. Заполняется данными из файла Excel'

    def add_arguments(self, parser):
        parser.add_argument('--file', type=str)

    def handle(self, *args, **options):
        sub_code = 1
        g = Group.objects.get(name='teacher')
        f = options['file']
        i = 0
        try:
            wb = load_workbook(filename=f, read_only=True)
            sheet = wb[u'Лист1']
            first_row = True
            for row in sheet.rows:
                if first_row:
                    first_row = False
                    continue
                i+=1
                print "starting loop on row number {}".format(i)
                fac = row[14].value.encode('utf8')
                print "faculty to create {}".format(fac)
                
                faculty, created = Faculty.objects.get_or_create(name=fac)  # @UnusedVariable
                
                print "Faculty created: {}".format(created)
                
                try:
                    speciality, created = Speciality.objects.get_or_create(
                                                                           faculty=faculty, 
                                                                           code=row[1].value, 
                                                                           name=row[2].value)
                    
                    print "Speciality: {} created: {}".format(speciality, created)
                except CommandError, e:
                    print "{} Exception during retrieving speciality".format(e)
                    speciality = Speciality.objects.get(code=row[1].value)
                except Exception, ex:
                    print "{} Exception during retrieving speciality".format(ex)
                    speciality = Speciality.objects.get(code=row[1].value)
                    
                print "Speciality {} retrieved".format(speciality)
                
                try:
                    subject = Subject.objects.get(name=row[6].value)
                except Exception, e:
                    print str(e)
                    subject, created = Subject.objects.get_or_create(code=sub_code, name=row[6].value)
                    
                sub_code += 1                
                
                print "Subject: {} created: {}".format(subject, created)
                
                position, created = Position.objects.get_or_create(code='Teacher', name='Teacher')
                
                print "Position: {} created: {}".format(position, created)
                
                try:
                    print "Trying to retrieve teacher"
                    teacher = Teacher.objects.get(username=row[10].value)
                    print "{} {}".format(teacher, "retrieved")
                except Exception, e:
                    print "{} exception occurred while trying to retrieve teacher".format(e)
                    teacher = Teacher()
                    teacher.username = row[10].value
                    teacher.set_password(row[10].value)
                    teacher.first_name = row[12].value
                    teacher.last_name = row[11].value
                    teacher.middle_name = row[13].value or ""
                    teacher.faculty = faculty
                    teacher.position = position
                    teacher.save()
                    print "{} {}".format(teacher, "saved")
                    g.user_set.add(teacher)
                    print 'Teacher permissions added'
                    
                try:
                    # Languages should be stored at database
                    lang = Language.objects.get(name=row[3].value)
                    print "Language: {} retrieved".format(lang)
                    learning_group_study_type = DISTANT if row[5].value == u'ғоибона' else ATTENDANT
                    
                    learning_group = LearningGroup.objects.get(speciality=speciality, course=row[0].value,
                                                               language=lang, subgroup_letter=row[4].value,
                                                               state=ACTIVE, study_type=learning_group_study_type)
                    print "Learning group: {} retrieved".format(learning_group)
                except LearningGroup.DoesNotExist, e:
                    learning_group = LearningGroup()
                    learning_group.course = row[0].value
                    learning_group.speciality = speciality
                    learning_group.language = lang
                    learning_group.subgroup_letter = row[4].value
                    learning_group.study_type = learning_group_study_type
                    learning_group.study_model = CREDIT
                    learning_group.entrance_year = datetime.now()
                    learning_group.state = ACTIVE
                    learning_group.save()
                    
                # Create or get TestCase 
                
                try:
                    print "Trying to get existing Test case with code {}".format(row[7].value)
                    test_case = TestCase.objects.get(code=row[7].value)
                except:
                    print "Test case not found creating"
                    test_case = TestCase()
                    test_case.code = row[7].value
                    test_case.subject = subject
                    test_case.teacher = teacher
                    test_case.description = learning_group
                    test_case.state = TestCase.NEW
                    test_case.save()
                    print "Test case created"
                    
                # Creating MarkBook that should be as glue 
                mark_book = MarkBook()
                mark_book.marks = {}
                mark_book.learning_group = learning_group
                mark_book.state = ACTIVE
                mark_book.semester = Semester.objects.get(code='s1')
                mark_book.subject = subject
                mark_book.teacher = teacher
                mark_book.created_by = teacher
                mark_book.test_case = test_case
                mark_book.save()
                
                group_subject = GroupSubject()
                group_subject.test_case = test_case
                group_subject.learning_group = learning_group
                group_subject.time_limit = 40
                group_subject.state = GroupSubject.NEW
                group_subject.standard_question_count = Setting.objects.get(
                        variable='STANDARD_TEST_COUNT').value
                group_subject.fitness_question_count = Setting.objects.get(
                        variable='FITNESS_TEST_COUNT').value
                group_subject.standard_question_score = Setting.objects.get(
                        variable='STANDARD_TEST_SCORE').value
                group_subject.fitness_question_score = Setting.objects.get(
                        variable='FITNESS_TEST_SCORE').value
                group_subject.save()
    
        except Exception, e:
            raise CommandError('Exception %s' % e)
        g.save()
