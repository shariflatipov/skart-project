from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth import views as auth_views
from root import views


handler404 = 'error_handlers.views.page_not_found'
handler500 = 'error_handlers.views.server_error'
handler403 = 'error_handlers.views.permission_denied'
handler400 = 'error_handlers.views.bad_request'

urlpatterns = [
    url(r'^autocomplete/', include('autocomplete_light.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^login/', auth_views.login, name='login'),
    url(r'^logout/', auth_views.logout, {'next_page': "/"}, name='logout'),
    url(r'^change-password/', auth_views.password_change,
        {'template_name': 'registration/change_password.html'}, name='change_password'),
    url(r'^change-password-done/', auth_views.password_change_done,
        {'extra_context': {'some_key': 'some_val'},
         'template_name': 'registration/change_password_done.html'}, name='password_change_done'),
    url(r'^index$', views.index_view, name='index'),
    url(r'^$', views.index_view, name='index'),
    url(r'^core/', include('core.urls')),
    url(r'^refs/', include('refs.urls')),
    url(r'^people/', include('people.urls')),
    url(r'^androlle/', include('androlle.urls')),
    url(r'^location/', include('location.urls')),
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^learning_process/', include('learning_process.urls')),
    url(r'^documents/', include('documents.urls')),
    url(r'^educational_plan/', include('educational_plan.urls')),
    url(r'^testing/', include('testing.urls')),
    url(r'^statistics/', include('statistics.urls')),

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + \
    static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
