#!/usr/bin/python

import os
import sys

from django.core.wsgi import get_wsgi_application

sys.path.append('/var/www/skart.tj/skart')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "skart.settings")

application = get_wsgi_application()
